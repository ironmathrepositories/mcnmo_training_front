# Обработка запросов #

В этом документе описан процесс работы с запросами со стороны фронта системы.

## Общая логика ##

В общем, логика обработки запросов со стороны фронта, не отличается от привычной нам.

Формируются saga-обработчики, которые "просыпаются" при возникновении action того типа, на который они подписаны. Нюанс заключается в том, что в системе есть обобщенный обработчик `run_request_process`, который берет на себя основную логику обработки запросов. При этом его работу можно модифицировать, передав ему генератор, в котором мы определяем дополнительную логику обработки успешного или ошибочного ответа бэка.

Общая логика состоит в том, что для каждого запроса должны быть сформированы три action, имя типа которых подчиняется простому правилу:

* имя первого action, запускающего запрос, может быть произвольным (например, `fetchData`).
* В случае успешного ответа бэка, `run_request_process` бросает в redux action, имя которого совпадает с первым, но имеет суффикс `Success` (например, `fetchDataSuccess`). Первым параметром action-creator будет объект `AxiosResponse` с телом ответа и прочими параметрами ответа. Вторым параметром будет null - так как объекта ошибки нет. Третьим параметром является ссылка на первый action (в данном случае `fetchData`).
* В случае ошибки запроса со стороны бэка, обработчик отправляет в redux action, имя которого совпадает с первым, но имеет суффикс `Failure` (например, `fetchDataFailure`). Первым параметром action-creator будет null - так как объекта успешного ответа нет. Вторым параметром - объект ошибки запроса `AxiosError`, третьим - action, который инициировал запрос.

Таков процесс обработки ответа бэка по-умолчанию.

Однако эту обработку можно модифицировать если в `run_request_process` передать вторым параметром некоторый генератор, который обработает ответ бэка прежде чем он будет отправлен в redux. Генератор первым параметром ожидает объект успешного ответа, вторым параметром объект ошибки ответа, третьим - объект самого первого action. Эта дополнительная обработка может быть связана как с модификацией ответа бэка перед передачей в redux, так и с вызовом дополнительных action, или выводом уведомлений.

Если переданный нами дополнительный генератор-обработчик выбросит ошибку `BreakRequestProcess` - то вызов `Success` и `Failure` action-ов не произойдет.

## Интерфейсы ##

Обработчик запросов находится в [`src/utils/run_request_process.ts`](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/src/utils/run_request_process.ts). Это generic. Первым параметром интерфейса является интерфейс ответа бэка (`R`), вторым - интерфейс payload инициирующего action (`P`).

```typescript
function runRequestProcess<R, P> (type: string, responseProcessor?: ResponseProcessGenerator<R, P>) => Generator;
```

Первым параметром (`type`) обработчика является строка - это имя action, на который он должен среагировать. Вторым (`responseProcessor`) - дополнительный генератор-обработчик, определяемый разработчиком. Дополнительный обработчик имеет интерфейс:

```typescript
export type ResponseProcessGenerator<R = unknown, P = unknown> = (
    data: AxiosResponse<R> | null,
    error: AxiosError<ErrorResponse> | null,
    action: Action<P> | PromisifiedAction<P, AxiosResponse<R>, AxiosResponse<ErrorResponse>>
) => Generator<unknown, ResponsePreprocessorReturn<R>, unknown>

export interface ResponsePreprocessorReturn<R = unknown> {
    data: AxiosResponse<R> | null,
    error: AxiosError<ErrorResponse>
}
```

Это тоже generic, где `R` - это интерфейс ответа бэка, `P` - интерфейс payload инициирующего action. Параметр `data` - объект успешного ответа бэка, параметр `error` - объект ответа бэка с ошибкой, `action` - объект action инициировавшего запрос.

## Пример использования ##

```typescript
const redirectOnSuccess = function* (data: AxiosResponse<LoginResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        Cookies.set('JWT-Cookie', data.data.token);
        history.push('/');
        throw new BreakRequestProcess();
    }
    if (error) {
        toastError(error, 'Ошибка аутентификации.');
    }

    return { data, error };
}

export const watchFetchLogin = runRequestProcess<LoginResponse, Login>(fetchLogin.type, redirectOnSuccess);
```

Обявляется saga-обработчик `watchFetchLogin`, который потом подключается к `root_saga`. В качестве ответа бэка ожидается объект с интерфейсом `LoginResponse`, а объект payload инициирующего action `fetchLogin` имеет интерфейс `Login`.

В `runRequestProcess` передается обработчик `redirectOnSuccess`. Этот оработчик, в случае успешного ответа бэка, записывает полученный токен в cookies, выполняет редирект на главную страницу и выбрасывает ошибку `BreakRequestProcess`. В случае ошибки в ответе бэка, вызывает уведомление с сообщением `Ошибка аутентификации.`. После чего возвращает управление обратно в `runRequestProcess`. Последний попытается вызвать `fetchLoginFailure` action.