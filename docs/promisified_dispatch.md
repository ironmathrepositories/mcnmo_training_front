# Промисифицированный dispatch #

## Общее описание ##

Это инструмент, доступный в [`utils/promisified_dispatch.ts`](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/src/utils/promisified_dispatch.ts). Экземпляр промисифицированного метода dispatch, замкнутого на объект `store` экспортируется из `@store`.

Суть этого dispatch в том, что он возвращает Promise объект, который резолвится при успешном завершении запроса к бэку, который инициируется отправленным action, и реджектится - в случае ошибки со стороны бэка. И в том, и в другом случае Promise объект разрешается с объектом, который мы имеем после работы `responseProcessor` передаваемого в `runRequestProcess`.

Инструмент используемый редко, но бывают случаи когда он удобен. В основном это наследие системы, в которой UI строится на базе схем.

## Пример использования ##

```typescript
import { promisifiedDispatch } from '@store';

promisifiedDispatch(fetchLogin(login, password))
    .then((v: AxiosResponse<LoginResponse>) => { /* обработка "на месте" */ })
    .catch((v: AxiosResponse<ErrorReponse>) => { /* обработка "на месте" */ })
```

Action `fetchLogin` - это обычный action.

Логика работы `runRequestProcess` такова, что он гарантирует передачу в Promise того объекта, который был отправлен в redux.