const { ModuleFederationPlugin } = require('webpack').container;

module.exports = (options = {}) => {
  console.log('process.env.MODULES_TARGET:', process.env.MODULES_TARGET);

  return new ModuleFederationPlugin({
    name: 'training',
    shared: {
      react: {
        // import: 'react', // the "react" package will be used a provided and fallback module
        // shareKey: 'react', // under this name the shared module will be placed in the share scope
        // shareScope: 'default', // share scope with this name will be used
        singleton: true,
        eager: true,
        requiredVersion: false, // pkg.dependencies.react,
      },
      'react-dom': {
        singleton: true,
        eager: true,
        requiredVersion: false, // pkg.dependencies['react-dom'],
      },
    },
  });
};
