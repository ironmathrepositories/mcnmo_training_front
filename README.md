# Описание системы "Тренинги"

## Разработка

Установка зависимостей:

```sh
npm install
```

Запуск проекта:

```sh
npm start
```

При запуске проекта через `npm start` будет использованы версии МПП и WYSIWYG, расположенные на тестовом сервере (https://altnpm.mathem.ru)

### Запуск с конкретными версиями МПП и WYSIWYG

При помощи переменных окружения можно задать URL'ы, по которым будут загружены определённые версии МПП и WYSIWYG:
`WYSIWYG_URL` - путь до remoteEntry.js c WYSIWYG.
`WYSIWYG_BASE_URL` - базовый путь до репозитория с модулями, который будет использован для построения пути модуля с версией (http://altnpm.mathem.ru -> http://altnpm.mathem.ru/wysiwyg/v1/remoteEntry.js).
`WEBPREVIEW_URL_V1` - путь до remoteEntry.js c WebPreview версии 1.
`WEBPREVIEW_URL_V2` - путь до remoteEntry.js c WebPreview версии 2.
`WEBPREVIEW_URL` - путь до remoteEntry.js c WebPreview. Будет использован для всех версий, если не указаны `WEBPREVIEW_URL_V1` и `WEBPREVIEW_URL_V2`.
`WEBPREVIEW_BASE_URL` - базовый путь до репозитория с модулями, который будет использован для построения пути модуля с версией (http://altnpm.mathem.ru -> http://altnpm.mathem.ru/mpp/v1/remoteEntry.js).

`WYSIWYG_BASE_URL` и `WEBPREVIEW_BASE_URL` выставляются автоматически (но могут быть переопределены при необходиомости) в зависимости от `MODULES_TARGET` при сборке:
для `test` - `http://altnpm.mathem.ru`, для `production` - `http://npm.mathem.ru`

Например:

```sh
WEBPREVIEW_URL_V2=http://localhost:5000/remoteEntry.js npm run build:test
```

Для всех модулей, кроме WebPreview версии 2 будет использованы url `http://altnpm.mathem.ru/***`. Для WebPreview версии 2 будет использована локальная сборка МПП.

````

### Сборка проекта

Сборка проекта для тестового сервера:

```sh
npm run build:test
````

Сборка проекта для боевого сервера:

```sh
npm run build:prod
```

Боевая и тестовая сборки различаются endpoint'ами, к которым они обращаются для подключения модулей средствами module federation.

### Локальный запуск production-сборки

```sh
npm run build:prod
# или `npm run build:test` для тестового сервера

docker-compose build
docker-compose up -d

start chrome http://localhost:8083
```

## Структура проекта

- `docs/`

  Содержит основные [документы](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/docs/) связанные с текущей системой.

- `src/api/`

  Инициализирует библиотеку axios и содержит методы обращения к api.

- `src/app/`

  Содержит основные маршруты и базовый каркас всех страниц системы.

- `src/components/`

  - `general_components/`

    Содержит коллекцию используемых в системе компонентов, чаще всего здесь располагаются многократно переиспользуемые компоненты.

  - `screens/`

    Содержит экраны основных страниц системы. Эти экраны подключаются к роутеру.

- `src/json`

  Здесь можно разместить любые json-файлы. В частности, в папке `mock_data/` можно разместить данные-заглушки, если это необходимо.

- `src/redux`

  Директория с настройками redux, action-методами, saga-обработчиками и селекторами. Redux работает на базе [ReduxToolkit](https://redux-toolkit.js.org/) библиотеки.

  - `<sub-module-dir-name>/`

    Содержит основные элементы настройки и управления состоянием данных подсистемы. Для имен файлов принято следующее соглашение:

    - `actions.ts` - файл с action-методами,

    - `reducer.ts` - определение редьюсера и начального состояния данных,

    - `selectors.ts` - велекторы данных,

    - `sagas.ts` - обработчики saga. На этот счет, следует изучить логику обработки запросов со стороны фронта: [run_request_process](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/docs/request_processing.md).

  - `actions.ts`

    В этот файл необходимо импортировать все создаваемые action-ы. Доступны для импорта по ссылке `@actions`.

  - `history.ts`

    Определение объекта history. Доступно по ссылке `@history`.

  - `root_reducer.ts`

    В этот файл необходимо подключать все reducer-ы системы.

  - `root_saga.ts`

    В этот файл необходимо подключать все saga-обработчики.

  - `selectors.ts`

    В этот файл необходимо импортировать все создаваемые селекторы. Становятся доступны для импортирования по ссылке `@selectors`.

  - `store.ts`

    Конфигурирует store redux, подключает middlewares. Экспортирует:

    - ссылку на `promisifiedDispatch` коллбэк ([описание](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/docs/promisified_dispatch.md)),

    - базовый тип, описывающий интерфейс `RootState`,

    - и, собственно, ссылку на объект `store`.

    Доступен для импорта по ссылке `@store`.

- `src/scss`

  Директория с общими для всей системы стилями.

- `src/types`

  [Директория](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/src/types/) с общими для всей системы типами и интерфейсами данных. Именно сюда следует размещать описания аннотаций typescript. Однако если описываемый тип или интерфей используется локально только в одном файла и никуда более не экспортируется - то можно его определение разместить в файле, где он используется. Как только возникает необходимость аннотацию экспортировать для использования в других файлах, её нужно перенести в папку `src/types`.

- `src/utils`

  Содержит общие для всей системы инструменты, логика которых абстрагирована от каких-либо подсистем фронта и может быть широко использована в разных частях системы. [Здесь](https://bitbucket.org/ironmathrepositories/mcnmo_training_front/src/master/src/utils/) уже содержатся некоторые TypeGuards для typescript и полезные утилиты.

## Соглашение по именованию файлов

Все файлы и директории в проекте мы именуем в `snake_case` формате.

## Ветка разработки

Основная ветка разработки `develop`. Необходимо бранчеваться от неё и в неё же делать pull-request.

## Запуск development сборки

Необходимо выполнить следующие команды:

- npm install
- npm run start

Работающая dev-сборка становится доступна на [localhost:9005](http://localhost:9005/).

## Настройка mock-данных в ответе сервера

Все запросы со стороны фронта проксируются на удаленный backend. Настройки адреса проксирования в `webpack.dev.js` в блоке `devServer`.

Если по каким-то причинам нет возможности со стороны бэка получить корректный формат ответа, или еще не реализован метод api, то можно в файле `webpack.dev.js` в корне проекта, определить endpoint метода, который не будет проксироваться на alt.mathem.ru, а обрабатываться локальным dev-сервером.

Необходимые методы локального сервера можно определить в блоке `devServer.before` в настройках `webpack.dev.js`.

## Сборка production версии

Пока не реализована.
