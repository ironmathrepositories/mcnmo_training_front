FROM nginx:latest as build
WORKDIR /
COPY ./dist /usr/share/nginx/html
COPY ./infra/nginx/nginx.conf /etc/nginx/conf.d/default.conf
