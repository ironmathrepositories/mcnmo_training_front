const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

module.exports = (env, options = {}) => 
    merge(common(env, { ...options, mode: 'production' }), {
        mode: 'production',
        devtool: false,
        plugins: [
            new webpack.DefinePlugin({
                PRODUCTION: JSON.stringify('true'),
                DEVELOPMENT: JSON.stringify('false')
            })
        ],
        output: {
            filename: 'app.js',
            publicPath: '/',
            path: path.resolve(__dirname, 'dist'),
        },
    });
