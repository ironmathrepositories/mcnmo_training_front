const { merge } = require('webpack-merge');
const webpack = require('webpack');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = (env, options = {}) =>
    merge(common(env, { ...options, mode: 'development' }), {
        mode: 'development',
        devtool: 'eval-source-map',
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            contentBasePublicPath: '/',
            publicPath: '/',
            historyApiFallback: true,
            watchContentBase: true,
            compress: true,
            host: '0.0.0.0',
            port: 9005,
            hot: true,
            stats: {
                logging: 'warn'
            },
            // before: (app, server, compiler) => {
            //     // здесь можно настроить локальные заглушки api, на случай, если нужно протестировать функционал, который на бэке еще не реализован.
            //     app.get('/api/example/data', (req, res) => {
            //         if (Math.random() < 0.5) {
            //             res.status(500);
            //             res.send({
            //                 error: 'sql error.....',
            //                 errorCode: 'SERVER_ERROR',
            //                 displayErrorMessage: 'Случайным образом отдаваемая с сервера ошибка 500.'
            //              });
            //             return;
            //         }
            //         res.json({
            //             example: {
            //                 key: 'value',
            //                 array: [ 'one', 'two', 'three' ]
            //             }
            //         });
            //     });
            // }
            proxy: [
                {
                    context: ['/api', '/static'],

                    target: 'https://alttraining.mathem.ru',
                    changeOrigin: true,
                    headers: {
                        'host': 'alttraining.mathem.ru',
                        'origin': 'https://alttraining.mathem.ru',
                    }
                },
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                DEVELOPMENT: JSON.stringify('true')
            })
        ],
        output: {
            filename: 'app.js',
            publicPath: '/',
            path: path.resolve(__dirname, 'dist')
        },
    });
