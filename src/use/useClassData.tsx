import React, { useEffect } from 'react';
import { UserTypes } from '@/interfaces/login';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@store';
import * as actions from '@actions';

const useClassData = () => {
    const user = useSelector((state: RootState) => state.auth.token?.dataAccess.user);
    const classData = useSelector((state: RootState) => state.accountTeacher.classData);
    const dispatch = useDispatch();
    const isTeacher = user?.type === UserTypes.TEACHER;

    useEffect(() => {
        if (!classData && isTeacher) {
            dispatch(actions.fetchClassData());
        }
    }, [ isTeacher, classData ]);

    return classData;
};

export default useClassData;
