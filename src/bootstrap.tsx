import React, { FC } from 'react';
import ReactDOM from 'react-dom';
import App from '@/app';
import './scss/normalize.scss';
import './scss/main.scss';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.version = '1.1.0';

const render = (Component: FC): void => {
    ReactDOM.render(
        <Component />,
        document.querySelector('#app')
    );
}

render(App);

if (DEVELOPMENT && module.hot) {
    module.hot.accept('@/app', () => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const newApp = require('@/app').default;
        render(newApp);
    });
}
