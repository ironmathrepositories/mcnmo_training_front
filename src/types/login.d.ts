import { IUser } from '@redux/auth/interfaces';
export interface Login {
    login: string;
    password: string;
}

export interface LoginResponse {
    token: string;
    refresh: string;
}

export interface ITokenPayload {
    token: {
        id: string;
        expired: number;
    },
    user: IUser
}

export interface IRefreshTokenPayload {
    token: {
        id: string;
        expired: number;
    }
}
