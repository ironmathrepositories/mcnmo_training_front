import { AxiosError, AxiosResponse } from 'axios';
import { ErrorResponse } from './error_response';

export interface PromiseCallbacks<T, E> {
    resolve: (v: T) => Promise<T>,
    reject: (v: E) => Promise<E>
}

export interface PreparedPayloadWithPromise<P, R, E> { // for redux-toolkit action preparing
    payload: P,
    meta: {
        promiseActions: PromiseCallbacks<R, E>
    }
}

export interface Action<P> {
    type: string;
    payload: P;
    meta?: Record<string, unknown>;
}

export interface PromisifiedMetaAction<R, E> {
    meta: {
        promiseActions: PromiseCallbacks<R, E>
    }
}
export interface PromisifiedAction<P, R = AxiosResponse, E = AxiosError<ErrorResponse>> extends Action<P>, PromisifiedMetaAction<R, E> {}
