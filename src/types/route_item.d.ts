import { FC } from 'react';

export interface RouteItem {
    path: string,
    component: FC,
    exact?: boolean,
    isLogin?: boolean
}

export interface RouteItem<T> {
    path: string,
    component: FC,
    exact?: boolean,
    isLogin?: boolean,
    props?: T;
}

export type Routes = Array<RouteItem>;
