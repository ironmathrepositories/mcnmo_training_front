declare module 'WebPreview' {
    export type WebPreviewRootData = {
        version?: number;
        tag: 'root',
        elem: WebPreviewData;
    }

    export type WebPreviewData = {
        tag: string;
        elem: string | string[] | WebPreviewData | WebPreviewData[];
    };
    export type WebPreviewExternalStyles = Record<string, string>;
    export type WebPreviewSettings = Record<string, any>;
    export type WebPreviewCheckAnswerHandler = (answer: any) => any;

    export interface WebPreviewProps {
        data: WebPreviewRootData;
        checkAnswerHandler?: WebPreviewCheckAnswerHandler;
        externalStyles?: WebPreviewExternalStyles;
        settings?: WebPreviewSettings;
    }

    function WebPreview(props: WebPreviewProps): JSX.Element;

    export = WebPreview;
}

declare module 'web_preview/WebPreview' {
    function WebPreview(props: WebPreviewProps): JSX.Element;

    export = WebPreview;
}
