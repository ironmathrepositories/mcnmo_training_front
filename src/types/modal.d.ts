export interface IModalState {
    modalName: string;
    title: string,
    isOpen?: boolean;
}