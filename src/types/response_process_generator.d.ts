import { Action, PromisifiedAction } from './action';
import { ErrorResponse } from './error_response';

export type ResponseProcessGenerator<R = unknown, P = unknown> = (data: AxiosResponse<R> | null, error: AxiosError<ErrorResponse> | null, action: Action<P> | PromisifiedAction<P, AxiosResponse<R>, AxiosResponse<ErrorResponse>>) => Generator<unknown, ResponsePreprocessorReturn<R>, unknown>
export interface ResponsePreprocessorReturn<R = unknown> {
    data: AxiosResponse<R> | null,
    error: AxiosError<ErrorResponse>
}
