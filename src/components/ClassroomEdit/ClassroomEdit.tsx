import React from 'react';
import { connect, ConnectedProps } from 'react-redux';

import StudentListEdit from '../StudentListEdit/StudentListEdit';
import { IClassData } from '@redux/accountTeacher/interfaces';
import { RootState } from '@store';
import * as actions from '@actions';
import * as accountTeacherActions from '@redux/accountTeacher/slice';
import AddStudent from './AddStudent/AddStudent';
import styles from './СlassroomEdit.module.scss';

const mapState = (state: RootState) => ({
    scores: state.accountTeacher.scores
});

const mapDispatch = {
    setReadableClass: accountTeacherActions.setReadableClass,
    fetchEditClass: actions.fetchEditClass,
    fetchDeleteTeacherClass: actions.fetchDeleteTeacherClass
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    data: IClassData
}

const ClassroomEdit: React.FC<IProps> = (
    {
        data,
        setReadableClass,
        fetchEditClass,
        fetchDeleteTeacherClass
    }
) => {
    const cancel = () => setReadableClass(data.id);
    const remove = () => {
        if (confirm('Вы уверенны что хотите удалить класс?')) {
            fetchDeleteTeacherClass({ classId: data.id });
        }
    };

    const blur = (e: React.FocusEvent<HTMLDivElement>) => {
        const name = e.target.innerText;
        fetchEditClass({ id: data.id, data: { name, class: data.class } });
    };

    return (
        <div className={styles.wrap}>
            <div className={styles.head}>
                <div
                    className={styles.titleEdit}
                    contentEditable={true}
                    suppressContentEditableWarning={true}
                    onBlur={blur}
                >
                    {data.name}
                </div>
                <div className={styles.hButtons}>
                    <button className={styles.hButton} onClick={remove}>Удалить класс</button>
                    <button className={styles.hButton} onClick={cancel}>Назад к статистике класса</button>
                </div>
            </div>

            <h3 className={styles.desc}>
                Ученики
            </h3>

            <div className={styles.buttons}>
                <AddStudent data={data} />
            </div>

            <StudentListEdit data={data.students} classId={data.id} />
        </div>
    );
};

export default connector(ClassroomEdit);
