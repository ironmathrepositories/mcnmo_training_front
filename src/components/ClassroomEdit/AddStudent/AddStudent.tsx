import React, { useState } from 'react';
import { ModalComponent } from 'mcnmo_components-collection';
import { Field, Form } from 'react-final-form';
import classNames from 'classnames';

import ApiOutlined from '@ant-design/icons/ApiOutlined';
import Input from '@/components/form/Input/Input';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import CheckOutlined from '@ant-design/icons/CheckOutlined';
import { required } from '@/validation';

import modalStyles from './modal.module.scss';
import styles from './AddStudent.module.scss';
import { RootState } from '@store';
import * as actions from '@actions';
import { connect, ConnectedProps } from 'react-redux';
import { getFieldErrors } from '@utils/getFieldErrors';
import { IClassData } from '@redux/accountTeacher/interfaces';

const nameValidate = required();

const mapState = (state: RootState) => ({});

const mapDispatch = {
    fetchAddStudent: actions.fetchAddStudent
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
interface IProps extends PropsFromRedux {
    data: IClassData
}

const AddStudent: React.FC<IProps> = ({ data, fetchAddStudent }) => {
    const [ isDialogVisible, setDialogVisible ] = useState(false);
    const close = () => setDialogVisible(false);

    const send = (values) => {
        return new Promise((resolve) => {
            fetchAddStudent(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values) => {
        const errors = await send({
            studentId: values.id,
            classId: data.id
        });

        if (!errors) {
            close();
        }

        return errors;
    };

    return (
        <>
            <button className={styles.buttonAdd} onClick={() => setDialogVisible(true)}>
                <ApiOutlined />
                <span className={styles.buttonText}>Добавить ученика по ID</span>
            </button>

            {isDialogVisible && (
                <ModalComponent close={close} title={'Добавить ученка'} customStyles={modalStyles}>
                    <Form
                        onSubmit={onSubmit}
                        render={({ handleSubmit, valid }) => (
                            <form onSubmit={handleSubmit}>
                                <div className={styles.content}>
                                    <Field
                                        name="id"
                                        label="id:"
                                        validate={nameValidate}
                                        component={Input}
                                    />

                                    <div className={styles.buttonWrapper}>
                                        <span role="button" tabIndex={0} className={classNames(styles.button, styles.cancel)} onClick={close}>
                                            <CloseOutlined className={styles.buttonIcon} />
                                            Отмена
                                        </span>
                                        <button typeof="submit" className={classNames(styles.button, styles.ok)}>
                                            <CheckOutlined className={styles.buttonIcon} />
                                            Добавить
                                        </button>
                                    </div>
                                </div>
                            </form>
                        )}
                    />
                </ModalComponent>
            )}
        </>
    )
};

export default connector(AddStudent);
