import React from 'react';
import { Loader } from 'mcnmo_components-collection';
import { RemoteComponent, RemoteComponentProps } from 'mcnmo_remote-module';

import type { WebPreviewRootData, WebPreviewProps } from 'WebPreview';

export type { WebPreviewRootData, WebPreviewProps };

export interface WebPreviewComponentProps extends RemoteComponentProps, WebPreviewProps {}

function getWebPreviewUrl(version: number) {
    let url: string | null = null;

    // Support for custom URL for specific version
    if (version === 1) {
        if (process.env.WEBPREVIEW_URL_V1) {
            url = process.env.WEBPREVIEW_URL_V1;
        }
    } else if (version === 2) {
        if (process.env.WEBPREVIEW_URL_V2) {
            url = process.env.WEBPREVIEW_URL_V2;
        }
    }

    if (!url) {
        url =
        process.env.WEBPREVIEW_URL ??
        `${process.env.WEBPREVIEW_BASE_URL}/mpp/v${version}/remoteEntry.js`;
    }

    // console.log('[DEBUG] WebPreview module url: ', url);

    return url;
}

const DEFAULT_VERSION = 1;

export const WebPreview = React.forwardRef<HTMLElement, WebPreviewComponentProps>(
    ({ data, ...props }, ref): JSX.Element => {
        const version = Number(data.version ?? DEFAULT_VERSION);
        const url = getWebPreviewUrl(version);
        const scope = `web_preview${version > 1 ? `_v${version}` : ''}`;

        return (
            <RemoteComponent
                {...props}
                data={data}
                ref={ref}
                url={url}
                module="./WebPreview"
                scope={scope}
                error={`Failed to load URL ${url}.`}
                fallback={<Loader />}
                loader={<Loader />}
            />
        );
    }
);

WebPreview.displayName = 'WebPreview';
