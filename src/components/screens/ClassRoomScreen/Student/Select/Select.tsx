import React, { useState, useEffect, useCallback } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import * as accountStudentI from '@redux/accountStudent/interfaces';
import CaretDownOutlined from '@ant-design/icons/CaretDownOutlined';
import { RootState } from '@store';
import * as actions from '@actions';
import styles from './Select.module.scss';

const accessData = [
    {
        value: 0,
        label: 'Доступ ЗАПРЕЩЕН'
    },
    {
        value: 1,
        label: 'Доступ РАЗРЕШЕН',
    }
];

const mapState = (state: RootState) => ({});

const mapDispatch = {
    fetchStudentSetAccessResolution: actions.fetchStudentSetAccessResolution
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    teacher: accountStudentI.ITeacherData
}

const Select: React.FC<IProps> = ({ teacher, fetchStudentSetAccessResolution }) => {
    const [ isOpen, setOpen ] = useState(false);
    const close = useCallback(() => setOpen(false), [ setOpen ]);

    const setAccess = () => {
        fetchStudentSetAccessResolution({ teacherId: teacher.id, accessResolution: !teacher.accessResolution });
    };

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('click', close);
        }

        return () => {
            window.removeEventListener('click', close);
        }
    }, [ isOpen, close ]);

    const currentValue = accessData.find(item => Boolean(teacher.accessResolution) === Boolean(item.value));

    return (
        <div className={styles.selectWrap}>
            <div className={styles.select} onClick={() => setOpen(!isOpen)}>
                {currentValue?.label}
                <CaretDownOutlined />
            </div>
            {isOpen && (
                <ul className={styles.selectList}>
                    {accessData.filter(item => item.value !== currentValue?.value).map(item => (
                        <li className={styles.selectItem} key={item.value} onClick={() => setAccess()}>
                            {item.label}
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default connector(Select);
