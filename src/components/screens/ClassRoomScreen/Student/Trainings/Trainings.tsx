import React from 'react';
import styles from '@/components/screens/TopicListScreen/topicList/topic.module.scss';
import { RootState } from '@store';
import { connect, ConnectedProps } from 'react-redux';
import TaskLearner from '@/components/screens/TopicListScreen/topicList/TaskLearner';
import { IStudentTrainingBlock } from '@redux/accountStudent/interfaces';

const mapState = (state: RootState) => ({
    user: state.auth.token?.dataAccess.user

});
const mapDispatch = {};
const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    trainings: Array<IStudentTrainingBlock>
}

const Trainings: React.FC<IProps> = ({ trainings }) => {
    return (
        <div className={styles.body}>
            <div className={styles.list}>
                {trainings.map(block => <TaskLearner
                    key={block.id}
                    directionId={block.examDirectionId}
                    collectionId={block.trainingCollectionId}
                    data={block}
                />)}
            </div>
        </div>
    );
};

export default connector(Trainings);
