import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import * as actions from '@actions';
import { RootState } from '@store';

import Title from '../Title/Title';
import Back from '@/components/Back/Back';
import { getUserName } from '@utils/user';
import TitleId from '../TitleId/TitleId';
import Select from './Select/Select';
import Trainings from './Trainings/Trainings';
import styles from './Student.module.scss';

const mapState = (state: RootState) => ({
    teachers: state.accountStudent.teachers,
    studentTrainings: state.accountStudent.studentTrainings
});

const mapDispatch = {
    fetchStudentTeachers: actions.fetchStudentTeachers,
    fetchStudentTrainings: actions.fetchStudentTrainings
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const ClassRoomStudentScreen: React.FC<IProps> = (
    {
        teachers,
        studentTrainings,
        fetchStudentTeachers,
        fetchStudentTrainings
    }
) => {
    useEffect(() => {
        fetchStudentTeachers();
        fetchStudentTrainings({ page: 1, chunk: 50 });
    }, [ fetchStudentTeachers, fetchStudentTrainings ]);

    const trainings = studentTrainings?.list || [];

    return (
        <div className="container">
            <Back />

            <div className="mainTitle">
                Личный кабинет
            </div>

            <TitleId />

            {teachers && (
                <div className={styles.teachers}>
                    <div className={styles.teachersTitle}>
                        Мои учителя:
                        {teachers.length === 0 && (
                            <span> пока ещё ни один учитель не добавил тебя в свой класс.</span>
                        )}
                    </div>
                    <div className={styles.teachersList}>
                        {teachers.map(teacher => (
                            <div key={teacher.id} className={styles.teacher}>
                                {/* @ts-ignore */}
                                <div className={styles.teacherName} tooltip={teacher.id} flow="down">{teacher.name || teacher.comment}</div>
                                <div className={styles.teacherSpace} />
                                <div className={styles.teacherAccess}>
                                    <Select teacher={teacher} />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            )}

            {trainings.length === 0 && (
                <div className={styles.empty}>
                    <p>Здесь появятся рекомендации от твоих учителей и твои личные тренинги.</p>
                    <p>Ты сможешь их также видеть непосредственно в интерфейсе.</p>
                </div>
            )}

            {trainings.length > 0 && (
                <div className={styles.trainings}>
                    <Trainings trainings={trainings} />
                </div>
            )}
        </div>
    );
};

export default connector(ClassRoomStudentScreen);
