import React, { useState, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import EditOutlined from '@ant-design/icons/EditOutlined';
import CopyOutlined from '@ant-design/icons/CopyOutlined';

import { getUserName } from '@utils/user';
import { RootState } from '@store';
import * as actions from '@actions';
import Back from '@/components/Back/Back';
import Classroom from '@/components/Classroom/Classroom';
import AddClass from '../AddClass/AddClass';
import Title from '../Title/Title';
import styles from './Teacher.module.scss';

const mapState = (state: RootState) => ({
    classData: state.accountTeacher.classData
});

const mapDispatch = {
    fetchClassData: actions.fetchClassData
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const ClassRoomTeacherScreen: React.FC<IProps> = ({ classData, fetchClassData }) => {
    useEffect(() => {
        fetchClassData();
    }, [ fetchClassData ]);

    return (
        <div className="container">
            <Back />

            <Title />
            <AddClass />

            {classData && (
                <>
                    {classData.map(classItem => (
                        <Classroom key={classItem.id} data={classItem} />
                    ))}

                    {classData.length === 0 && (
                        <div className={styles.empty}>
                            Здесь появятся классы после того, как вы их добавите
                        </div>
                    )}
                </>
            )}
        </div>
    );
};

export default connector(ClassRoomTeacherScreen);
