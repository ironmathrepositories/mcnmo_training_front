import React, { useEffect, useRef } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import ClipboardJS from 'clipboard';
import CopyOutlined from '@ant-design/icons/CopyOutlined';
import { RootState } from '@store';
import styles from './TitleId.module.scss';

const mapState = (state: RootState) => ({
    user: state.account.accountData
});

const mapDispatch = {};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const TitleId: React.FC<IProps> = ({ user }) => {
    const btnClipboard = useRef<HTMLSpanElement>(null);

    const id = user?.id || '';

    useEffect(() => {
        if (!btnClipboard?.current) {
            return;
        }

        new ClipboardJS(btnClipboard.current as Element, { text: () => id });
    }, [ btnClipboard?.current ]);

    return (
        <div className={styles.wrapId}>
            <span>Ваш ID:</span>
            <span className={styles.wrapIdText}>
                <span className={styles.id}>{id}</span>
                <span ref={btnClipboard}>
                    <CopyOutlined className={styles.icon} />
                </span>
            </span>
        </div>
    );
};

export default connector(TitleId);
