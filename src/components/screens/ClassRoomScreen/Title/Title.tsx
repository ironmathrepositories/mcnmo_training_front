import React, { useEffect, useRef, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Field, Form } from 'react-final-form';
import classNames from 'classnames';
import EditOutlined from '@ant-design/icons/EditOutlined';
import CopyOutlined from '@ant-design/icons/CopyOutlined';
import ClipboardJS from 'clipboard';

import { RootState } from '@store';
import * as actions from '@actions';
import * as IAccount from '@redux/account/interfaces';
import { getUserName } from '@utils/user';
import { required } from '@/validation';
import TitleId from '../TitleId/TitleId';
import styles from './Title.module.scss';

const mapState = (state: RootState) => ({
    user: state.account.accountData
});

const mapDispatch = {
    fetchAccountSetFio: actions.fetchAccountSetFio
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const Title: React.FC<IProps> = ({ user, fetchAccountSetFio }) => {
    const [ isEdit, setEdit ] = useState(false);

    // const name = getUserName(user);
    const name = user?.name || '';
    const id = user?.id || '';

    const onSubmit = (values: IAccount.IFetchAccountSetFioPayload) => {
        fetchAccountSetFio(values);
        setEdit(false);
    };

    return (
        <>
            {!isEdit && (
                <div className="mainTitle">
                    <span className={styles.titleWrap}>
                        <span className={styles.title}>
                            { name ? name : `Представьтесь: ${id}`}
                        </span>
                        <EditOutlined
                            className={styles.icon}
                            onClick={() => setEdit(true)}
                        />
                    </span>
                </div>
            )}

            {isEdit && (
                <Form
                    onSubmit={onSubmit}
                    render={({ handleSubmit, valid }) => (
                        <form className={styles.form} onSubmit={handleSubmit}>
                            <Field
                                name="name"
                                validate={required()}
                                className={styles.input}
                                defaultValue={name}
                                component="input"
                            />

                            <div className={styles.buttons}>
                                <button
                                    typeof="button"
                                    className={classNames(styles.button, styles.buttonCancel)}
                                    onClick={() => setEdit(false)}
                                >
                                    Отменить
                                </button>
                                <button typeof="submit" className={styles.button}>Сохранить</button>
                            </div>
                        </form>
                    )}
                />
            )}

            {!name && (
                <div className={styles.descTitle}>
                    Обратите внимание! Ваше представление будет отображаться в кабинетах ваших учеников.
                </div>
            )}

            <TitleId />
        </>
    )
};

export default connector(Title);
