import React, { useState } from 'react';
import { ModalComponent } from 'mcnmo_components-collection';
import { Field, Form } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';
import classNames from 'classnames';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import CheckOutlined from '@ant-design/icons/CheckOutlined';

import Input from '@/components/form/Input/Input';
import { required } from '@/validation';
import { getFieldErrors } from '@utils/getFieldErrors';
import * as IAccountTeacher from '@redux/accountTeacher/interfaces';
import { RootState } from '@store';
import * as actions from '@actions';
import SelectInput from '@/components/form/SelectInput/SelectInput';

import modalStyles from './modal.module.scss';
import styles from './AddClass.module.scss';
import { IOption } from '@/lib/Select';
import FormStyleContext, { brForm } from '@/context/FormStyleContext';


const options: Array<IOption> = [ {
    label: 'Выберите класс...',
    value: ''
} ];

for (let i = 11; i > 0; i--) {
    options.push({
        label: `${i} класс`,
        value: i.toString()
    });
}

const nameValidate = required();
const classValidate = required();

const mapState = (state: RootState) => ({
    classData: state.accountTeacher.classData || []
});

const mapDispatch = {
    fetchAddClass: actions.fetchAddClass
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const AddClass: React.FC<IProps> = ({ fetchAddClass }) => {
    const [ isDialogVisible, setDialogVisible ] = useState(false);
    const close = () => setDialogVisible(false);

    const send = (values: IAccountTeacher.IAddClassPayload) => {
        return new Promise((resolve) => {
            fetchAddClass(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAccountTeacher.IAddClassPayload) => {
        values.class = 9;
        const errors = await send({
            name: values.name,
            class: Number(values.class)
        });
        close();
        return errors;
    };

    return (
        <>
            <div className={styles.buttons}>
                <button className={styles.buttonAdd} onClick={() => setDialogVisible(true)}>
                    Добавить класс
                </button>
            </div>

            {isDialogVisible && (
                <ModalComponent close={close} title={'Добавить класс'} customStyles={modalStyles}>
                    <FormStyleContext.Provider value={brForm}>
                        <Form
                            onSubmit={onSubmit}
                            render={({ handleSubmit, valid }) => (
                                <form onSubmit={handleSubmit}>
                                    <div className={styles.content}>
                                        <Field
                                            name="name"
                                            label="Название:"
                                            validate={nameValidate}
                                            component={Input}
                                            placeholder="Впишите любое название, которое будет отображаться в интерфейсе..."
                                        />

                                        {/*<Field*/}
                                        {/*    name="class"*/}
                                        {/*    label="Номер класса:"*/}
                                        {/*    type="number"*/}
                                        {/*    validate={classValidate}*/}
                                        {/*    options={options}*/}
                                        {/*    component={SelectInput}*/}
                                        {/*/>*/}

                                        <div className={styles.buttonWrapper}>
                                            <span typeof="button" className={classNames(styles.button, styles.cancel)} onClick={close}>
                                                <CloseOutlined className={styles.buttonIcon} />
                                                Отмена
                                            </span>
                                            <button typeof="submit" className={classNames(styles.button, styles.ok)}>
                                                <CheckOutlined className={styles.buttonIcon} />
                                                Добавить
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            )}
                        />
                    </FormStyleContext.Provider>
                </ModalComponent>
            )}
        </>
    )
};

export default connector(AddClass);
