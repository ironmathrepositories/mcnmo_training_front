import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '@store';
import * as actions from '@actions';
import { UserTypes } from '@/interfaces/login';
import ClassRoomTeacherScreen from './Teacher/Teacher';
import ClassRoomStudentScreen from './Student/Student';

const mapState = (state: RootState) => ({
    user: state.auth.token?.dataAccess.user
});

const mapDispatch = {
    fetchAccount: actions.fetchAccount
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;

const ClassRoomScreen: React.FC<IProps> = ({ user, fetchAccount }) => {
    useEffect(() => {
        fetchAccount();
    }, [ fetchAccount ]);

    if (user && user.type === UserTypes.STUDENT) {
        return <ClassRoomStudentScreen />;
    }

    return <ClassRoomTeacherScreen />;
};

export default connector(ClassRoomScreen);
