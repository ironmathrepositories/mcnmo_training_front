import React from 'react';
import styles from './PersonalDataProcessingScreen.module.scss';

const PersonalDataProcessingScreen: React.FC = () => {
    return (
        <div className={styles.content}>
            Страница в разработке.
            <br />
            Скоро здесь появится полезный материал.
            <br />
            Приносим свои извинения за неудобство.
        </div>
    );
};

export default PersonalDataProcessingScreen;
