import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import { RootState } from '@store';
import logo from '../../../img/logo-large.svg';
import styles from './EnterScreen.module.scss';

const mapState = (state: RootState) => ({
    directionList: state.training.directionList
});

const mapDispatch = {};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {}

const EnterScreen: React.FC<IProps> = ({ directionList }) => {
    return (
        <div className="container">
            <div className={styles.content}>
                <div className={styles.title}>
                    <img src={logo} alt="МАТР" className={styles.logo} />
                    — Тренинги по математике
                </div>
                <div className={styles.subtitle}>
                    Тренируйся, решай, сдавай на 5!
                </div>
                <div className={styles.subtitle}>
                    Выбери раздел:
                </div>
            </div>

            <ul className={styles.list}>
                {directionList && directionList.map(direction => (
                    <li key={direction.id} className={styles.itemLink}>
                        <Link to={`/topics/${direction.id}`} className={styles.link} style={direction.style}>
                            {direction.name}
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default connector(EnterScreen);
