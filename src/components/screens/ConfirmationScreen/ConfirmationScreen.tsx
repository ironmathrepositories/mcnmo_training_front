import React from 'react';
import { useParams } from 'react-router-dom';
import Confirmation from '@/components/auth/Confirmation/Confirmation';

interface IUrlParams {
    confirmCode: string;
}

const ConfirmationScreen:React.FC = () => {
    const { confirmCode } = useParams<IUrlParams>();

    return (
        <Confirmation confirmCode={confirmCode} />
    )
};

export default ConfirmationScreen;
