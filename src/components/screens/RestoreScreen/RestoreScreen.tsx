import React from 'react';
import { useParams } from 'react-router-dom';
import Restore from '@/components/auth/Restore/Restore';

interface IUrlParams {
    singleToken: string;
}

const RestoreScreen:React.FC = () => {
    const { singleToken } = useParams<IUrlParams>();

    return (
        <Restore singleToken={singleToken} />
    )
};

export default RestoreScreen;
