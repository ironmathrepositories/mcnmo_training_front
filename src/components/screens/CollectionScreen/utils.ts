import { ITrainingBlock } from '@redux/training/interfaces';

export interface ICurrentTraining {
    current: ITrainingBlock,
    prev?: ITrainingBlock,
    next?: ITrainingBlock,
    number: number
}

export const getCurrentTraining = (trainings: Array<ITrainingBlock>, id: string): ICurrentTraining | undefined => {
    const currentIndex = trainings.findIndex(item => String(item.id) === String(id));

    if (currentIndex === -1) {
        return undefined;
    }

    return {
        current: trainings[currentIndex],
        prev: currentIndex >= 0 ? trainings[currentIndex - 1]: undefined,
        next: currentIndex < trainings.length - 1 ? trainings[currentIndex + 1]: undefined,
        number: currentIndex + 1
    }
};
