import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import LeftOutlined from '@ant-design/icons/LeftOutlined';
import RightOutlined from '@ant-design/icons/RightOutlined';

import { RootState } from '@store'
import * as actions from '@actions';
import * as trainingActions from '@redux/training/slice';
import Task from '@/components/Task/Task';
import { UserTypes } from '@/interfaces/login';
import TeacherSelectClasses from '@/components/TeacherSelectClasses/TeacherSelectClasses';
import useClassData from '@/use/useClassData';
import Back from '../../Back/Back';
import CircleFill from '../../svg/CircleFill';
import { getCurrentTraining, ICurrentTraining } from './utils';
import { Position } from '@/components/PopoverSelect/PopoverSelect';
import styles from './CollectionScreen.module.scss';

const mapState = (state: RootState) => ({
    collection: state.training.collection,
    user: state.auth.token?.dataAccess.user
});

const mapDispatch = {
    fetchTrainingsCollection: actions.fetchTrainingsCollection,
    clearCollection: trainingActions.clearCollection
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
type IProps = PropsFromRedux;
interface IUrlParams {
    directionId: string;
    collectionId: string;
    trainingId: string;
}

const CollectionScreen: React.FC<IProps> = ({ collection, user, fetchTrainingsCollection, clearCollection }) => {
    const { directionId, collectionId, trainingId } = useParams<IUrlParams>();
    const [ training, setTraining ] = useState<ICurrentTraining>();
    const score = training?.current.score || 0;
    useClassData();

    useEffect(() => {
        if (collectionId) {
            fetchTrainingsCollection(collectionId);
        }

        return () => {
            clearCollection();
        };
    }, [ collectionId ]);

    useEffect(() => {
        if (!collection) {
            return;
        }

        setTraining(getCurrentTraining(collection.trainings, trainingId));
    }, [ collection, trainingId ]);

    const addButton = (
        <div className={styles.addButton} />
    );

    const getInfo = () => {
        if (user?.type !== UserTypes.STUDENT) {
            return (
                <div className={styles.info}>
                    {training?.current && (
                        <TeacherSelectClasses data={training?.current} position={Position.Right}>
                            {addButton}
                        </TeacherSelectClasses>
                    )}
                </div>
            );
        }

        // @ts-ignore
        return <div className={styles.info} tooltip={`${score}%`} flow="up">
            <CircleFill percent={score} />
        </div>;
    };

    return (
        <div className="container">
            <Back url={`/topics/${directionId}`} />

            {collection && training?.current && (
                <>
                    <div className="mainTitle">
                        {collection.name}
                    </div>

                    <div className={styles.nav}>
                        {training.prev ? (
                            <div className={styles.navLeft}>
                                <Link to={`/training/${directionId}/${collectionId}/${training.prev.id}`} className={styles.arrLeft}>
                                    <LeftOutlined />
                                </Link>
                                <span className={styles.navText}>
                                    {training.number - 1}. {training.prev.name}
                                </span>
                            </div>
                        ): <div />}

                        {training.next ? (
                            <div className={styles.navRight}>
                                <span className={styles.navText}>
                                    {training.number + 1}. {training.next.name}
                                </span>
                                <Link to={`/training/${directionId}/${collectionId}/${training.next.id}`} className={styles.arrRight}>
                                    <RightOutlined />
                                </Link>
                            </div>
                        ): <div />}
                    </div>

                    <div className={styles.titleWrap}>
                        <h2 className={styles.title}>{training.number}. {training.current.name}</h2>
                        {getInfo()}
                    </div>

                    <Task trainingId={trainingId} />
                </>
            )}
        </div>
    );
};

export default connector(CollectionScreen);
