import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import TexWebPreview from '../../WebPreview';
import type { WebPreviewRootData } from '../../WebPreview/WebPreview';

import classNames from 'classnames';

import { RootState } from '@store';
import * as actions from '@actions';
import * as trainingActions from '@redux/training/slice';
import { IOGEVariant } from '@redux/training/interfaces';
import Select from '../../SelectTitle/Select';
import * as ISelect from '../../SelectTitle/interfaces';
import styles from './TestVersionScreen.module.scss';
import { ErrorBoundary } from '@components';

const options: Array<ISelect.IOption> = [ {
    value: 'oge',
    label: 'ОГЭ'
}, {
    value: 'ege',
    label: 'ЕГЭ'
} ];

const mapState = (state: RootState) => ({
    variants: state.training.variants,
    user: state.auth.token?.dataAccess.user
});

const mapDispatch = {
    fetchVariants: actions.fetchVariants,
    fetchCheckAnswer: actions.fetchCheckAnswer
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface IProps extends PropsFromRedux {
    type: string;
}

const TestVersionScreen: React.FC<IProps> = (
    {
        type,
        variants,
        fetchVariants,
        fetchCheckAnswer
    }
) => {
    const [ variantActive, setVariantActive ] = useState<IOGEVariant>();
    const [ countCheck, setCountCheck ] = useState<number | null>(null);

    useEffect(() => {
        fetchVariants(type);
    }, []);

    useEffect(() => {
        if (variants && variants.length && !variantActive) {
            setVariantActive(variants[0]);
        }
    }, [ variants ]);

    useEffect(() => {
        setCountCheck(null);
    }, [ variantActive ]);

    if (!variants) {
        return null;
    }

    const send = (data) => {
        return new Promise((resolve) => {
            fetchCheckAnswer(data, {
                promiseActions: {
                    resolve: (d) => {
                        const { payload } = d.data;
                        const checkResult = payload?.checkResult;
                        if (checkResult && typeof checkResult === 'object') {
                            const count = Object
                                .values(checkResult)
                                .reduce<number>((res, value) => value ? res + 1 : res, 0);

                            setCountCheck(count);
                        }

                        resolve(payload)
                    },
                    reject: () => resolve(false)
                }
            })
        });
    };

    const checkAnswerHandler = async (data) => {
        return  await send({ variantId: variantActive?.id, data });
    };

    return (
        <div className="container">
            <div className={styles.header}>
                <span className={styles.headerSpacer} />
                <h1 className={styles.title}>Тренировочные варианты</h1>
                <div className={styles.selectTitle}>
                    <Select
                        disabled
                        list={options}
                        placeholder="Выберите тип экзамена"
                        defaultValue={type}
                    />
                </div>
            </div>

            <ul className={styles.variants}>
                {variants.map(variant => {
                    const className = variantActive && variant.id === variantActive.id
                        ? classNames(styles.variant, styles.variantActive)
                        : styles.variant;

                    return (
                        // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
                        <li
                            className={className}
                            onClick={() => setVariantActive(variant)}
                            key={variant.id}
                        >
                            {variant.title}
                        </li>
                    );
                })}
            </ul>

            <div className={styles.info}>
                <div className={styles.titleWrap}>
                    <b className={styles.title}>{variantActive?.title}</b>
                    {/*<span className={styles.description}>Возможно дополнительное название</span>*/}
                </div>
                <div className={styles.statisticWrap}>
                    <span className={styles.statistic}>
                        <b>{countCheck === null ? '?' : countCheck}</b>/{variantActive?.tasksCount}
                    </span>
                    <span className={styles.statisticDescription}>
                        решенных
                        <br />
                        верно задач
                    </span>
                </div>
            </div>

            {variantActive && (
                <ErrorBoundary>
                    <div className={styles.content}>
                        {variantActive.tasks && <TexWebPreview key={variantActive.id} data={variantActive.tasks as WebPreviewRootData} checkAnswerHandler={checkAnswerHandler} />}
                    </div>
                </ErrorBoundary>
            )}
        </div>
    );
};

export default connector(TestVersionScreen);
