import AboutScreen from './InfoSreens/AboutScreen';
import ClassRoomScreen from './ClassRoomScreen/ClassRoomScreen';
import CollectionScreen from './CollectionScreen/CollectionScreen';
import ConfirmationScreen from './ConfirmationScreen/ConfirmationScreen';
import EnterScreen from './EnterScreen/EnterScreen';
import ErrorScreen from './error';
import LoginScreen from './login';
import NoAccessScreen from './NoAccessScreen/NoAccessScreen';
import PersonalDataProcessingScreen from './PersonalDataProcessingScreen/PersonalDataProcessingScreen';
import RestoreScreen from './RestoreScreen/RestoreScreen';
import TeacherInfoScreen from './InfoSreens/TeacherInfoScreen';
import TermsOfUseScreen from './InfoSreens/TermsOfUseScreen';
import TestVersionScreen from './TestVersionScreen/TestVersionScreen';
import TopicsScreen from './TopicListScreen/TopicListScreen';

export {
    AboutScreen,
    ClassRoomScreen,
    CollectionScreen,
    ConfirmationScreen,
    EnterScreen,
    ErrorScreen,
    LoginScreen,
    NoAccessScreen,
    PersonalDataProcessingScreen,
    RestoreScreen,
    TeacherInfoScreen,
    TermsOfUseScreen,
    TestVersionScreen,
    TopicsScreen
};
