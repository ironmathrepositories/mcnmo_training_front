/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import classnames from 'classnames';
import styles from './InfoScreens.module.scss';
import { Matr } from '@/constants';
import logo from '@/img/logo-large.svg';
import { ITermOfUse, TermOfUseContent } from './term_of_use';
import { isArray } from '@/utils/is_array';

interface ITermOfUseProps extends ITermOfUse {
    level?: number;
    itemNumber?: number;
    path?: string
}

type IContainerProps = Omit<Required<ITermOfUseProps>, 'title' | 'intro' | 'itemNumber'>;

const TermOfUseItemsContainer = ({ nested, level, path }: IContainerProps) => {
    return <table className={styles.TermOfUseItemsContainer}>
        {nested.map((props, index) => <TermOfUseItem
            {...props}
            level={level}
            path={path}
            itemNumber={index + 1}
            key={index}
        />)}
    </table>
}

const TermOfUseItem = ({ title, intro, nested, level = 1, itemNumber = 1, path = '' }: ITermOfUseProps) => {
    if (level === 1) {
        return <article>
            <h3>{`${path}${itemNumber}.`} {title}</h3>
            {intro && <p>{intro}</p>}
            {nested && isArray(nested) && nested.length
                ? <TermOfUseItemsContainer
                    nested={nested}
                    level={level + 1}
                    path={`${path}${itemNumber}.`}
                />
                : null}
        </article>
    }

    return <tr className={styles.TermOfUseItem}>
        <td>
            <p>{`${path}${itemNumber}`}</p>
        </td>
        <td>
            <p>{title}</p>
            {intro && <p>{intro}</p>}
            {nested && isArray(nested) && nested.length
                ? <TermOfUseItemsContainer
                    nested={nested}
                    level={level + 1}
                    path={`${path}${itemNumber}.`}
                />
                : null}
        </td>
    </tr>;
};

const TermsOfUseScreen: React.FC = () => {
    return (
        <div className={classnames(styles.InfoContainer, 'container')}>
            <h1>Пользовательское соглашение сервиса «<img src={logo} alt={Matr} className={styles.inlineLogo} />»</h1>
            {TermOfUseContent.map((props, index) => <TermOfUseItem {...props} itemNumber={index + 1} key={index} />)}
        </div>
    );
};

export default TermsOfUseScreen;
