import React from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { RootState } from '@store';
import * as authSliceActions from '@redux/auth/slice';
import { Dialog } from '@redux/auth/constants';
import styles from './styles.scss';

const mapState = (state: RootState) => ({
    isGuest: !state.auth.token
});

const mapDispatch = {
    singDialog: authSliceActions.singDialog
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux

const NoAccessScreen: React.FC<IProps> = ({ singDialog }) => {
    return (
        <div className={styles.container}>
            <h1 className={styles.title}>Сожалеем, но у вас нет прав доступа</h1>
            <span className={styles.link} onClick={() => singDialog(Dialog.SingIn)}>Войдите</span>
            или
            <span className={styles.link} onClick={() => singDialog(Dialog.SingUp)}>Зарегистрируйтесь</span>
        </div>
    );
};

export default connector(NoAccessScreen);
