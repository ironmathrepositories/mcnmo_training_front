import React from 'react';
import SingIn from '@/components/auth/SingIn/SingIn';
import styles from './styles.scss';

const LoginScreen = (): JSX.Element => {
    return (
        <div className={styles.container}>
            <SingIn />
        </div>
    )
};

export default LoginScreen;
