import React, { useEffect } from 'react';
import axios from 'axios';

import styles from '@screens/styles.scss';

const ErrorScreen = (): JSX.Element => {
    useEffect(() => {
        axios.get('/api/');
    }, []);
    return <div className={styles.ErrorScreen}>
        <h2>Error Screen</h2>
    </div>
};

export default ErrorScreen;
