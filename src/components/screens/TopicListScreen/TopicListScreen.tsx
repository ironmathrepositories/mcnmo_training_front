import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { useParams } from 'react-router-dom';

import { RootState } from '@store';
import * as actions from '@actions';
import * as trainingActions from '@redux/training/slice';
import * as trainingI from '@redux/training/interfaces';
import Back from '@/components/Back/Back';
import Topic from './topicList/Topic';
import SelectTitle from '../../SelectTitle/SelectTitle';
import styles from './TopicListScreen.module.scss';

const mapState = (state: RootState) => ({
    directionList: state.training.directionList,
    collectionList: state.training.collectionList
});

const mapDispatch = {
    fetchTrainingsCollections: actions.fetchTrainingsCollections,
    clearCollections: trainingActions.clearCollections
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;
interface IUrlParams {
    trainingId: string;
}

const TopicListPage: React.FC<IProps> = (
    {
        directionList,
        collectionList,
        fetchTrainingsCollections,
        clearCollections
    }
) => {
    const [ direction, setDirection ] = useState<trainingI.ITrainingDirection>();
    const { trainingId } = useParams<IUrlParams>();

    useEffect(() => {
        if (trainingId) {
            fetchTrainingsCollections(trainingId);

            if (directionList) {
                const activeDirection = directionList.find(item => String(item.id) === String(trainingId));
                setDirection(activeDirection);
            }
        }

        return () => {
            clearCollections();
        }
    }, [ directionList, trainingId ]);

    return (
        <div className="container">
            <Back url="/" />

            <div className={styles.header}>
                <span className={styles.headerSpacer} />
                {direction && (
                    <h1 className={styles.title}>{direction.name}</h1>
                )}
                {directionList && (
                    <div className={styles.selectTitle}>
                        <SelectTitle list={directionList} />
                    </div>
                )}
            </div>

            <ul className={styles.list}>
                {collectionList && collectionList.map(collection => <Topic
                    key={collection.id}
                    directionId={trainingId}
                    data={collection}
                />)}
            </ul>
        </div>
    );
};

export default connector(TopicListPage);
