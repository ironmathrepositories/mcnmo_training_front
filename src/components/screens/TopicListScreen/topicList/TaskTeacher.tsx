import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import * as trainingI from '@redux/training/interfaces';
import { RootState } from '@store';
import * as actions from '@actions';
import TeacherSelectClasses from '@/components/TeacherSelectClasses/TeacherSelectClasses';
import styles from './task.module.scss';

interface IPropsOwn {
    data: trainingI.ITrainingBlock
    collectionId: string;
    directionId: string;
    number: number;
}

const mapState = (state: RootState) => ({
    // user: state.auth.token?.dataAccess.user
});

const mapDispatch = {};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux, IPropsOwn {}

const TaskTeacher: React.FC<IProps> = (
    {
        collectionId,
        directionId,
        data
    }
) => {

    const trainingId = data.id;

    return (
        <div className={styles.task}>
            <div className={styles.taskBody}>
                <div className={styles.info}>
                    <TeacherSelectClasses data={data}>
                        <div className={styles.addButton} />
                    </TeacherSelectClasses>
                </div>
                <Link to={`/training/${directionId}/${collectionId}/${trainingId}`} className={styles.content}>
                    <div className={styles.title}>
                        {data.name}
                    </div>
                </Link>
            </div>

            <div className={styles.desc}>
                {data.icon && <img src={data.icon} />}
            </div>
        </div>
    );
};

export default connector(TaskTeacher);
