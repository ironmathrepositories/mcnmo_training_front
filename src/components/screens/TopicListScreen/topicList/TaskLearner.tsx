import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import * as trainingI from '@redux/training/interfaces';
import Circle from '../../../svg/Circle';
import styles from './task.module.scss';

interface IProps {
    data: trainingI.ITrainingBlock
    collectionId: string;
    directionId: string;
    number?: number;
}

const TaskLearner: React.FC<IProps> = ({ collectionId, directionId, data, number }) => {
    const isOpened = !!data.recommendedBy?.length;

    return (
        <Link to={`/training/${directionId}/${collectionId}/${data.id}`} className={classNames(styles.task, isOpened ? styles.taskOpened : {})}>
            {isOpened && <i className={styles.recommended} />}
            <div className={styles.taskBody}>
                <div className={styles.info}>
                    {number && <div className={styles.number}>{number}</div>}
                    <Circle percent={data.score || 0} />
                </div>
                <div className={styles.content}>
                    <div className={styles.title}>
                        {data.name}
                    </div>
                </div>
            </div>

            <div className={styles.desc}>
                {data.icon && <img src={data.icon} />}
            </div>
        </Link>
    );
};

export default TaskLearner;
