import React, { useState } from 'react';
import classNames from 'classnames';
import { connect, ConnectedProps } from 'react-redux';
import DownOutlined from '@ant-design/icons/DownOutlined';

import * as trainingI from '@redux/training/interfaces';
import { RootState } from '@store';
import * as actions from '@actions';
import TaskLearner from './TaskLearner';
import TaskTeacher from './TaskTeacher';

import styles from './topic.module.scss';
import { UserTypes } from '@/interfaces/login';
import useClassData from '@/use/useClassData';

const mapState = (state: RootState) => ({
    user: state.auth.token?.dataAccess.user

});

const mapDispatch = {};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    data: trainingI.ITrainingsCollection
    directionId: string;
}

const Topic: React.FC<IProps> = ({ data, directionId, user }) => {
    const [ isOpen, setOpen ] = useState(false);
    const { trainings } = data;

    const isTeacher = user?.type === UserTypes.TEACHER;
    const Task = isTeacher ? TaskTeacher : TaskLearner;

    useClassData();

    return (
    // <li className={classNames(styles.topic)}>
        <li className={
            classNames(
                styles.topic,
                isOpen ? styles.topicOpen: {}
            )
        }>
            <div className={styles.header} onClick={() => setOpen(!isOpen)}>
                <span>{data.name}</span>
                <div className={styles.topicChk}>
                    <DownOutlined />
                </div>
            </div>
            <div className={styles.body}>
                <div className={styles.list}>
                    {trainings.map((block, i) => <Task
                        key={block.id}
                        directionId={directionId}
                        collectionId={data.id}
                        data={block}
                        number={i + 1}
                    />)}
                </div>
            </div>
        </li>
    );
};

export default connector(Topic);
