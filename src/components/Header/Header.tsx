import React from 'react';
import { Link } from 'react-router-dom';
import UserOutlined from '@ant-design/icons/UserOutlined';
import LogoutOutlined from '@ant-design/icons/LogoutOutlined';
import { ModalComponent } from 'mcnmo_components-collection';
import classNames from 'classnames';

import * as actions from '@actions';
import logo from '../../img/logo.svg';
import SingIn from '@/components/auth/SingIn/SingIn';
import SingUp from '@/components/auth/SingUp/SingUp';
import Restoration from '@/components/auth/Restoration/Restoration';
import { RootState } from '@store';
import { connect, ConnectedProps } from 'react-redux';
import { Dialog } from '@redux/auth/constants';
import * as authSliceActions from '@redux/auth/slice';

import modalStyles from './modal.module.scss';
import styles from './Header.module.scss';

const mapState = (state: RootState) => ({
    authDialog: state.auth.authDialog,
    isGuest: !state.auth.token,
    stat: state.accountStudent.stat
});

const mapDispatch = {
    singDialog: authSliceActions.singDialog,
    logout: actions.fetchLogout
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;

const Header: React.FC<IProps> = (
    {
        authDialog,
        singDialog,
        logout,
        isGuest,
        stat
    }
) => {
    const close = () => singDialog(Dialog.Close);
    const logoutHandler = () => {
        if (confirm('Вы уверены, что хотите выйти?')) {
            logout();
        }
    };

    return (
        <div className={styles.header}>
            <div className="containerEx">
                <div className={styles.headerContent}>
                    <div className={styles.left}>
                        <Link to="/">
                            <img className={styles.logo} src={logo} alt="logo" />
                        </Link>
                        {stat && (
                            <div className={styles.statistic}>
                                <div className={styles.statisticCountAll}>{stat.total}</div>
                                <div className={styles.statisticTextAll}>
                                    всего задач
                                    <br />
                                    решено
                                </div>
                                <div className={styles.statisticCountResolved}>{stat.lastWeek}</div>
                                <div className={styles.statisticTextResolved}>
                                    задач решено
                                    <br />
                                    на этой неделе
                                </div>
                            </div>
                        )}
                    </div>
                    <div className={styles.right}>
                        <div className={styles.links}>
                            <Link to="/" className={classNames(styles.link, styles.linkMain)}>Главная</Link>
                            <Link to="/oge" className={styles.link}>Тренировочные варианты</Link>
                            <Link to="/about" className={styles.link}>О нас</Link>
                            <Link to="/teacherInfo" className={styles.link}>Учителю</Link>
                        </div>
                        {/*<Search />*/}
                        {isGuest ? (
                            <button type="button" className={styles.enter} onClick={() => singDialog(Dialog.SingIn)}>Вход</button>
                        ) : (
                            <>
                                <span className={styles.account}>
                                    { /* @ts-ignore */ }
                                    <Link to="/classroom" tooltip="Личный кабинет" flow="down">
                                        <UserOutlined />
                                    </Link>
                                </span>

                                { /* @ts-ignore */ }
                                <div className={styles.logout} tooltip="Выйти из системы" flow="down">
                                    <LogoutOutlined onClick={() => logoutHandler()} />
                                </div>
                            </>
                        )}
                    </div>
                </div>
            </div>

            {!!authDialog && (
                <ModalComponent close={close} noHeader footer={null} title={''} customStyles={modalStyles}>
                    {authDialog === Dialog.SingUp && <SingUp />}
                    {authDialog === Dialog.SingIn && <SingIn />}
                    {authDialog === Dialog.Restoration && <Restoration />}
                </ModalComponent>
            )}
        </div>
    )
};

export default connector(Header);
