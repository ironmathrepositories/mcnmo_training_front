import React, { useCallback, useEffect, useState } from 'react';
import classNames from 'classnames';

import styles from './PopoverSelect.module.scss';

export interface IOption {
    label: string;
    value: string;
    selected?: boolean;
}

export enum Position {
    Left,
    Right
}

interface IProps {
    title: string;
    options: Array<IOption>
    label: () => React.ReactElement,
    onClick?: (e: React.MouseEvent) => void;
    onChange?: (option: IOption, e?: React.MouseEvent) => void;
    onToggle?: (value: boolean) => void;
    position?: Position
}

const PopoverSelect: React.FC<IProps> = (
    {
        title,
        options,
        label,
        onClick,
        onChange,
        onToggle,
        position
    }
) => {
    const [ isOpen, setOpen ] = useState(false);
    const close = useCallback(() => toggle(false), [ setOpen ]);

    const toggle = (val?: boolean) => {
        const newVal = val === undefined ? !isOpen : val;
        setOpen(newVal);
        onToggle && onToggle(newVal);
    };

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('click', close);
        }

        return () => {
            window.removeEventListener('click', close);
        }
    }, [ isOpen, close ]);

    const handlerClickItem = (option: IOption, e: React.MouseEvent) => {
        onChange && onChange(option, e);
    };

    const handlerClickPopup = (e: React.MouseEvent) => {
        e.stopPropagation();
    };

    const handlerClick = (e: React.MouseEvent) => {
        onClick && onClick(e);
    };

    return (
        <div tabIndex={0} role="menu" className={styles.content} onClick={handlerClick}>
            <div className={styles.button} tabIndex={0} role="button" onClick={() => toggle()}>
                {label()}
            </div>
            {isOpen && (
                <div
                    className={classNames(styles.popup, position === Position.Right ? styles.popupRight : {})}
                    onClick={handlerClickPopup}
                >
                    <h2 className={styles.title}>{title}</h2>

                    <ul className={styles.options}>
                        {options.map(option => (
                            <li
                                key={option.value}
                                className={classNames(styles.option, option.selected ? styles.optionSelected : null)}
                                onClick={(e) => handlerClickItem(option, e)}
                            >
                                {option.label}
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    );
};

export default PopoverSelect;
