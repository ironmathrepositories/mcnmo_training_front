import React from 'react';
import { getPercentColorGradient } from '@utils/getPercentColor';

interface IProps {
    percent: number;
}

const CircleFill: React.FC<IProps> = ({ percent: p }) => {
    const percent = Math.round(p);
    const dasharray = 2*Math.PI*16;
    const offset = percent * dasharray / 100;
    const strokeDasharray = `${offset} ${dasharray}`;
    const color = getPercentColorGradient(percent);

    return (
        <div style={{ transform: 'rotate(-90deg)' }}>
            <svg width="100%" height="100%" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="32" cy="32" r="32" fill="#F1F1F1" stroke="transparent" />
                <circle
                    cx="32"
                    cy="32"
                    r="16"
                    stroke={color}
                    strokeWidth="32"
                    // stroke-dashoffset="25"
                    strokeDasharray={strokeDasharray}
                />
                {/*<circle cx="32" cy="32" r="30" stroke="white" stroke-width="4" />*/}
            </svg>
        </div>
    );
};

export default CircleFill;
