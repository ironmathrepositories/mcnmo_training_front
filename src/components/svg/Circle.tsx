import React from 'react';
import { getPercentColorGradient } from '@utils/getPercentColor';

interface IProps {
    percent: number;
}

const Circle: React.FC<IProps> = ({ percent: p }) => {
    const percent = Math.round(p);
    const dasharray = 2*Math.PI*18;
    const offset = percent * dasharray / 100;
    const strokeDasharray = `${offset} ${dasharray}`;
    const color = getPercentColorGradient(percent);

    return (
        <div style={{ transform: 'rotate(-90deg)', width: '40px', height: '40px' }}>
            <svg id="svg" width="40" height="40">
                <circle r="20" cx="20" cy="20" fill="#FFFFFF" strokeDasharray="100" strokeDashoffset="0" />
                <circle
                    className="circle"
                    r="18"
                    cx="20"
                    cy="20"
                    fill="#FFFFFF"
                    stroke={color}
                    strokeWidth="4"
                    strokeDasharray={strokeDasharray}
                />
            </svg>
        </div>
    );
};

export default Circle;
