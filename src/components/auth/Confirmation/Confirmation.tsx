import React, { useEffect, useState } from 'react';
import { Form, Field } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';

import Input from '@/components/form/Input/Input';
import Errors from '@/components/form/Errors/Errors';
import { required } from '@/validation';
import { RootState } from '@store';
import * as actions from '@actions';
import * as IAuth from '@redux/auth/interfaces';
import * as authSliceActions from '@redux/auth/slice';

import styles from './Confirmation.module.scss';
import {getFieldErrors} from "@utils/getFieldErrors";

const confirmCodeValidate = required();

const mapState = (state: RootState) => ({
    errors: state.auth.errors,
    resendDateTime: state.auth.resendDateTime
});

const mapDispatch = {
    fetchConfirm: actions.fetchConfirm,
    fetchResendConfirm: actions.fetchResendConfirm,
    clearErrors: authSliceActions.clearErrors
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    confirmCode: string;
}

const TIME_RESEND = 60 * 1000;
const getCount = (time = 0) => {
    if (!time) return 0;
    return Math.round((Number(time) + TIME_RESEND - new Date().getTime()) / 1000)
};

const Confirmation: React.FC<IProps> = (
    {
        resendDateTime,
        confirmCode,
        fetchConfirm,
        fetchResendConfirm,
        errors,
        clearErrors
    }
) => {
    const [ timeResend, setTimeResend ] = useState(0);

    useEffect(() => {
        clearErrors();

        return () => {
            clearErrors();
        }
    }, []);

    useEffect(() => {
        let time;
        if (!resendDateTime) {
            return;
        }

        const nextTime = () => {
            time = setTimeout(() => {
                const count = getCount(resendDateTime);
                if (count > 0) {
                    setTimeResend(count < 0 ? 0 : count);
                    nextTime();
                } else {
                    setTimeResend(0);
                }
            }, 1000);
        };
        nextTime();

        return () => {
            clearTimeout(time);
        };
    }, [ resendDateTime, setTimeResend ]);

    useEffect(() => {
        if (confirmCode) {
            fetchConfirm({ code: confirmCode });
        }
    }, [ confirmCode ]);

    const send = (values) => {
        return new Promise((resolve, reject) => {
            fetchConfirm(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAuth.IConfirmPayload) => {
        return await send(values);
    };

    if (confirmCode && errors?.length === 0) {
        return <div>Запрос на подтверждение...</div>;
    }

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, valid }) => (
                <form onSubmit={handleSubmit}>
                    <div className={styles.content}>
                        <h1 className={styles.title}>Подтверждение аккаунта</h1>

                        <Field
                            name="code"
                            label="Код:"
                            validate={confirmCodeValidate}
                            component={Input}
                            initialValue={confirmCode}
                        />

                        <div className={styles.buttonWrapper}>
                            <button typeof="submit" className={styles.button}>Подтвердить</button>
                        </div>

                        <div className={styles.resendCodeBlock}>
                            {timeResend <= 0 ? (
                                <span className={styles.resendCode} onClick={() => fetchResendConfirm()}>Выслать код повторно</span>
                            ) : (
                                <span>Выслать повторно через {timeResend} секунд</span>
                            )}
                        </div>

                        <Errors errors={errors} />
                    </div>
                </form>
            )}
        />
    );
};

export default connector(Confirmation);
