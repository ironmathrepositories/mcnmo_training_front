import React, { useEffect } from 'react';
import { Form, Field } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';

import { Dialog } from '@redux/auth/constants';
import Input from '@/components/form/Input/Input';
import Errors from '@/components/form/Errors/Errors';
import { composeValidators, required, email } from '@/validation';
import { RootState } from '@store';
import * as actions from '@actions';
import * as IAuth from '@redux/auth/interfaces';
import * as authSliceActions from '@redux/auth/slice';

import styles from './Restoration.module.scss';
import {getFieldErrors} from "@utils/getFieldErrors";

const emailValidate = composeValidators(required(), email());

const mapState = (state: RootState) => ({
    errors: state.auth.errors,
    isRestorationPassword: state.auth.isRestorationPassword
});

const mapDispatch = {
    fetchRestoration: actions.fetchRestoration,
    clearErrors: authSliceActions.clearErrors,
    clearRestorationPasswordSuccess: authSliceActions.clearRestorationPasswordSuccess,
    singDialog: authSliceActions.singDialog
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;

const Restoration: React.FC<IProps> = (
    {
        singDialog,
        fetchRestoration,
        errors,
        isRestorationPassword,
        clearErrors,
        clearRestorationPasswordSuccess
    }
) => {
    useEffect(() => {
        clearErrors();
        clearRestorationPasswordSuccess();
    }, []);

    const send = (values) => {
        return new Promise((resolve, reject) => {
            fetchRestoration(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAuth.IRestorationPayload) => {
        return await send(values);
    };

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, valid }) => (
                <form onSubmit={handleSubmit}>
                    <div className={styles.content}>
                        {isRestorationPassword && (
                            <div className={styles.success}>Запрос на восстановление пароля успешно отправлен</div>
                        )}
                        {!isRestorationPassword && (
                            <>
                                <h1 className={styles.title}>Востановление пароля</h1>

                                <Field
                                    name="email"
                                    label="E-mail:"
                                    validate={emailValidate}
                                    component={Input}
                                />

                                <div className={styles.buttonWrapper}>
                                    <button typeof="submit" className={styles.button}>Востановить</button>
                                </div>

                                <Errors errors={errors} />
                            </>
                        )}

                        <div className={styles.footer}>
                            <span role="link" tabIndex={0} className={styles.link} onClick={() => singDialog(Dialog.SingIn)}>Войти</span>
                            <br />
                            <span role="link" tabIndex={0} className={styles.link} onClick={() => singDialog(Dialog.SingUp)}>Регистрация нового пользователя</span>
                        </div>
                    </div>
                </form>
            )}
        />
    );
};

export default connector(Restoration);
