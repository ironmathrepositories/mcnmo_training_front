import React, { useEffect } from 'react';
import { Form, Field } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';

import { history } from '@redux/history';
import { Dialog } from '@redux/auth/constants';
import Input from '@/components/form/Input/Input';
import Errors from '@/components/form/Errors/Errors';
import { required } from '@/validation';
import { RootState } from '@store';
import * as actions from '@actions';
import * as IAuth from '@redux/auth/interfaces';
import * as authSliceActions from '@redux/auth/slice';
import { getFieldErrors } from '@utils/getFieldErrors';

import styles from './Restore.module.scss';

const passwordValidate = required();

const mapState = (state: RootState) => ({
    errors: state.auth.errors,
    isRestorePassword: state.auth.isRestorePassword
});

const mapDispatch = {
    fetchRestore: actions.fetchRestore,
    clearErrors: authSliceActions.clearErrors,
    clearRestorePasswordSuccess: authSliceActions.clearRestorePasswordSuccess,
    singDialog: authSliceActions.singDialog
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    singleToken: string;
}

const Restore: React.FC<IProps> = (
    {
        singleToken,
        errors,
        isRestorePassword,
        fetchRestore,
        clearErrors,
        clearRestorePasswordSuccess,
        singDialog
    }
) => {
    useEffect(() => {
        clearErrors();
        clearRestorePasswordSuccess();
    }, []);

    useEffect(() => {
        if (!isRestorePassword) return;

        const time = setTimeout(() => {
            history.push('/');
            singDialog(Dialog.SingIn);
        }, 2000);

        return () => {
            clearTimeout(time);
        }
    }, [ isRestorePassword ]);

    const send = (values) => {
        return new Promise((resolve, reject) => {
            const data = !singleToken ? values : { ...values, singleToken };
            fetchRestore(data, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAuth.IRestorePayload) => {
        return await send(values);
    };

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, valid }) => (
                <form onSubmit={handleSubmit}>
                    <div className={styles.content}>
                        <h1 className={styles.title}>Восстановление пароля</h1>

                        {isRestorePassword && (
                            <div className={styles.success}>Пароль изменён!!!</div>
                        )}
                        {!isRestorePassword && (
                            <>
                                {!singleToken && (
                                    <Field
                                        name="oldPassword"
                                        label="Старый пароль:"
                                        validate={passwordValidate}
                                        component={Input}
                                        type="password"
                                    />
                                )}
                                <Field
                                    name="newPassword"
                                    label={!singleToken ? 'Новый пароль:' : 'Пароль'}
                                    validate={passwordValidate}
                                    component={Input}
                                    type="password"
                                />

                                <div className={styles.buttonWrapper}>
                                    <button typeof="submit" className={styles.button}>Изменить</button>
                                </div>

                                <Errors errors={errors} />
                            </>
                        )}

                    </div>
                </form>
            )}
        />
    );
};

export default connector(Restore);
