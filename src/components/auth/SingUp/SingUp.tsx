import React, { useEffect } from 'react';
// import { FORM_ERROR } from 'final-form';
import { Form } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';

import { RootState } from '@store';
import * as IAuth from '@redux/auth/interfaces';
import * as actions from '@redux/actions';
import * as authSliceActions from '@redux/auth/slice';
import FormSingUp from './FormSingUp';
import { Dialog } from '@redux/auth/constants';
import { getFieldErrors } from '@utils/getFieldErrors';

const mapState = (state: RootState) => ({
    errors: state.auth.errors
});

const mapDispatch = {
    fetchSingUp: actions.fetchSingUp,
    clearErrors: authSliceActions.clearErrors,
    singDialog: authSliceActions.singDialog
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;

const SingUp: React.FC<IProps> = ({ singDialog, fetchSingUp, errors, clearErrors }) => {
    useEffect(() => {
        clearErrors();

        return () => {
            clearErrors();
        }
    }, []);

    const send = (values) => {
        return new Promise((resolve, reject) => {
            fetchSingUp(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAuth.ISingUpPayload) => {
        return await send(values);
    };

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, valid, values }) => <FormSingUp
                valid={valid}
                handleSubmit={handleSubmit}
                values={values}
                showEnter={() => singDialog(Dialog.SingIn)}
                errors={errors}
            />}
        />
    );
};

export default connector(SingUp);
