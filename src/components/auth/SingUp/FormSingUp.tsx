import React, { FormEventHandler } from 'react';
import { Field } from 'react-final-form';
import classNames from 'classnames';
import ArrowLeftOutlined from '@ant-design/icons/ArrowLeftOutlined';

import { UserTypes } from '@/interfaces/login';
import Input from '@/components/form/Input/Input';
import Errors from '@/components/form/Errors/Errors';
import SelectInput from '@/components/form/SelectInput/SelectInput';
import { composeValidators, email, required } from '@/validation';
import { ISingUpPayload } from '@redux/auth/interfaces';
import styles from './SingUp.module.scss';

const validate = {
    name: required('Имя обязательно для заполнения'),
    surname: required('Фамилия обязательна для заполнения'),
    patronymic: required('Очество обязательно для заполнения'),
    email: composeValidators(required(), email()),
    class: required(),
    password: required(),
    checked: required(),
};

interface IProps {
    valid: boolean;
    handleSubmit: FormEventHandler<HTMLFormElement>,
    values: ISingUpPayload,
    showEnter?: () => void,
    errors?: Array<string>
}

const FormSingUp: React.FC<IProps> = (
    {
        valid,
        handleSubmit,
        values,
        showEnter,
        errors
    }
) => {
    return (
        <form onSubmit={handleSubmit}>
            <div className={styles.content}>
                {showEnter && (
                    <div role="link" tabIndex={0} className={styles.back} onClick={showEnter}>
                        <ArrowLeftOutlined className={styles.backImg} />
                        <span className={styles.backText}>Назад ко <b>входу</b></span>
                    </div>
                )}
                <h1 className={styles.title}>РЕГИСТРАЦИЯ</h1>

                <Field
                    name="type"
                    defaultValue={UserTypes.STUDENT}
                    component={({ input }) => (
                        <div className={styles.switchType}>
                            <button
                                type="button"
                                className={
                                    classNames(
                                        styles.switchTypeOption,
                                        input.value === UserTypes.STUDENT ? styles.switchTypeOptionActive : {}
                                    )
                                }
                                onClick={() => input.onChange(UserTypes.STUDENT)}
                            >
                                Я учусь
                            </button>
                            <button
                                type="button"
                                className={
                                    classNames(
                                        styles.switchTypeOption,
                                        input.value === UserTypes.TEACHER ? styles.switchTypeOptionActive : {}
                                    )
                                }
                                onClick={() => input.onChange(UserTypes.TEACHER)}
                            >
                                Я учу
                            </button>
                        </div>
                    )}
                />


                {/*<Field*/}
                {/*    name="surname"*/}
                {/*    label="Фамилия:"*/}
                {/*    validate={validate.name}*/}
                {/*    component={Input}*/}
                {/*/>*/}

                {/*<Field*/}
                {/*    name="name"*/}
                {/*    label="Имя:"*/}
                {/*    validate={validate.surname}*/}
                {/*    component={Input}*/}
                {/*/>*/}

                {/*<Field*/}
                {/*    name="patronymic"*/}
                {/*    label="Отчество:"*/}
                {/*    validate={validate.patronymic}*/}
                {/*    component={Input}*/}
                {/*/>*/}

                {/*{values.type === UserTypes.STUDENT && (*/}
                {/*    <Field*/}
                {/*        name="class"*/}
                {/*        label="Класс:"*/}
                {/*        validate={validate.class}*/}
                {/*        component={SelectInput}*/}
                {/*    />*/}
                {/*)}*/}

                <div className={styles.enter}>
                    <div className={styles.desc}>
                        Указанный вами <b>e-mail</b> будет использоваться в качестве <b>логина</b> при входе в систему.
                    </div>

                    <Field
                        name="email"
                        label="E-mail:"
                        validate={validate.email}
                        component={Input}
                    />

                    <Field
                        name="password"
                        label="Пароль:"
                        type="password"
                        validate={validate.password}
                        component={Input}
                    />

                    <label htmlFor="agreeRules" className={styles.checkboxWrap}>
                        <Field<string>
                            id="agreeRules"
                            name="agreeRules"
                            validate={validate.checked}
                            component="input"
                            type="checkbox"
                        />
                        <span className={styles.checkboxText}>
                            Я согласен с <a href="/terms-of-use" target="_blank">правилами пользования сервисом.</a>
                        </span>
                    </label>

                    {values.type === UserTypes.TEACHER && (
                        <label htmlFor="confirmTeacher" className={styles.checkboxWrap}>
                            <Field<string>
                                id="confirmTeacher"
                                name="confirmTeacher"
                                validate={validate.checked}
                                component="input"
                                type="checkbox"
                            />
                            <span className={styles.checkboxText}>
                                Я осознаю, что сервис вправе в любой момент потребовать подтверждения того, что я учитель и может удалить мой аккаунт, если это не подтвердится.
                            </span>
                        </label>
                    )}

                    {/*<label htmlFor="agreePersonalData" className={styles.checkboxWrap}>*/}
                    {/*    <Field<string>*/}
                    {/*        id="agreePersonalData"*/}
                    {/*        name="agreePersonalData"*/}
                    {/*        validate={validate.checked}*/}
                    {/*        component="input"*/}
                    {/*        type="checkbox"*/}
                    {/*    />*/}
                    {/*    <span className={styles.checkboxText}>*/}
                    {/*        Я ознакомился и согласен с <a href="/personal-data-processing" target="_blank">условиями обработки персональных данных.</a>*/}
                    {/*    </span>*/}
                    {/*</label>*/}

                    <div className={styles.buttonWrapper}>
                        <button type="submit" className={styles.button}>Зарегистрироваться</button>
                    </div>

                    <Errors errors={errors} />
                </div>
            </div>
        </form>
    );
};

export default React.memo(FormSingUp);
