import React, { useEffect } from 'react';
import { Form, Field } from 'react-final-form';
import { connect, ConnectedProps } from 'react-redux';

import Input from '@/components/form/Input/Input';
import Errors from '@/components/form/Errors/Errors';
import { composeValidators, required, email } from '@/validation';
import { RootState } from '@store';
import { Dialog } from '@redux/auth/constants';
import { getFieldErrors } from '@utils/getFieldErrors';
import * as actions from '@actions';
import * as IAuth from '@redux/auth/interfaces';
import * as authSliceActions from '@redux/auth/slice';

import styles from './SingIn.module.scss';

const emailValidate = composeValidators(required(), email());
const passwordValidate = required();

const mapState = (state: RootState) => ({
    errors: state.auth.errors
});

const mapDispatch = {
    fetchSingIn: actions.fetchSingIn,
    clearErrors: authSliceActions.clearErrors,
    singDialog: authSliceActions.singDialog
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;

const SingIn: React.FC<IProps> = ({ singDialog, fetchSingIn, errors, clearErrors }) => {
    useEffect(() => {
        clearErrors();

        return () => {
            clearErrors();
        }
    }, []);

    const send = (values) => {
        return new Promise((resolve, reject) => {
            fetchSingIn(values, {
                promiseActions: {
                    resolve: () => resolve(false),
                    reject: (errors) => resolve(getFieldErrors(errors?.response?.data))
                }
            });
        });
    };

    const onSubmit = async (values: IAuth.ISingInPayload) => {
        return await send(values);
    };

    return (
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit, valid }) => (
                <form onSubmit={handleSubmit}>
                    <div className={styles.content}>
                        <h1 className={styles.title}>Вход</h1>

                        <Field
                            name="email"
                            label="E-mail:"
                            validate={emailValidate}
                            component={Input}
                        />

                        <Field
                            name="password"
                            label="Пароль:"
                            type="password"
                            validate={passwordValidate}
                            component={Input}
                        />

                        <div className={styles.buttonWrapper}>
                            <button typeof="submit" className={styles.button}>Войти</button>
                        </div>

                        <Errors errors={errors} />

                        <div className={styles.footer}>
                            <span role="link" tabIndex={0} className={styles.link} onClick={() => singDialog(Dialog.SingUp)}>Регистрация нового пользователя</span>
                            <br />
                            <span role="link" tabIndex={0} className={styles.link} onClick={() => singDialog(Dialog.Restoration)}>Забыли пароль?</span>
                        </div>
                    </div>
                </form>
            )}
        />
    );
};

export default connector(SingIn);
