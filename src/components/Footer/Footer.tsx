import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import logo from '../../img/logo.svg';
import styles from './Footer.module.scss';

export const Footer: React.FC = () => {
    return (
        <div className={styles.footer}>
            <div className="containerEx">
                <div className={styles.footerContent}>
                    <div className={styles.logoBlock}>
                        <Link to="/">
                            <img className={styles.logo} src={logo} alt="logo" />
                        </Link>
                        <div className={styles.copyright}>© МЦНМО, 2021</div>
                    </div>
                    <div className={styles.rightBlock}>
                        <a className={styles.link} href="">Контакты</a>
                        <a href="mailto:support@mathtraining.ru">support@mathtraining.ru</a>
                    </div>
                </div>
            </div>
        </div>
    )
};
