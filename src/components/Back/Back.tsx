import React from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import ArrowLeftOutlined from '@ant-design/icons/ArrowLeftOutlined';

import styles from './Back.module.scss';

interface IProps {
    url?: string;
}

const Back: React.FC<IProps> = ({ url }) => {
    const history = useHistory();

    const handlerClick = (e: React.MouseEvent<HTMLElement>) => {
        if (url) {
            history.push(url);
        } else {
            history.goBack();
        }

        e.preventDefault();
    };

    return (
        <div className={styles.back}>
            <a href="#" onClick={handlerClick} className={styles.backLink}>
                <ArrowLeftOutlined className={styles.arrow} /> Назад
            </a>
        </div>
    );
};

export default Back;
