import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import * as trainingI from '@redux/training/interfaces';
import PopoverSelect, { IOption, Position } from '@/components/PopoverSelect/PopoverSelect';
import { RootState } from '@store';
import * as actions from '@actions';

interface IPropsOwn {
    data: trainingI.ITrainingBlock;
    children: React.ReactElement;
    position?: Position;
}

const mapState = (state: RootState, ownProps: IPropsOwn) => ({
    user: state.auth.token?.dataAccess.user,
    classData: state.accountTeacher.classData,
    recommended: state.accountTeacher.recommended[ownProps.data.id]
});

const mapDispatch = {
    fetchRecommendedClasses: actions.fetchRecommendedClasses,
    setTrainingRecommendedClass: actions.setTrainingRecommendedClass,
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux, IPropsOwn {}

const TeacherSelectClasses: React.FC<IProps> = (
    {
        data,
        classData,
        position,
        recommended,
        fetchRecommendedClasses,
        setTrainingRecommendedClass,
        children
    }
) => {
    const [ options, setOptions ] = useState<Array<IOption>>([]);
    const title = 'Выберите классы';
    const trainingId = data.id;

    useEffect(() => {
        if (!classData) {
            return;
        }

        setOptions(classData.map(item => ({
            label: item.name,
            value: item.id,
            selected: recommended && recommended[item.id]
        })))
    }, [ classData, recommended ]);

    const changeSelect = (option: IOption) => {
        setTrainingRecommendedClass({ trainingId, classId: option.value });
    };

    const clickSelect = (e: React.MouseEvent) => {
        // e.stopPropagation();
    };

    const toggleSelect = (val: boolean) => {
        if (val && !recommended) {
            fetchRecommendedClasses(trainingId);
        }
    };

    return (
        <PopoverSelect
            label={() => children}
            title={title}
            options={options}
            onChange={changeSelect}
            onClick={clickSelect}
            onToggle={toggleSelect}
            position={position}
        />
    );
};

export default connector(TeacherSelectClasses);

