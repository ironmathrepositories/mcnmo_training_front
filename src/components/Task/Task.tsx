/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { ModalComponent } from 'mcnmo_components-collection';
import TexWebPreview from '../WebPreview';
import type { WebPreviewRootData } from '../WebPreview/WebPreview';
import ExclamationOutlined from '@ant-design/icons/ExclamationOutlined';
import QuestionOutlined from '@ant-design/icons/QuestionOutlined';
import RightOutlined from '@ant-design/icons/RightOutlined';

import { RootState } from '@store'
import * as actions from '@actions';
import * as trainingActions from '@redux/training/slice';
import { UserTypes } from '@/interfaces/login';
import modalStyles from './modal.module.scss';
import styles from './Task.module.scss';

const mapState = (state: RootState) => ({
    task: state.training.task,
    user: state.auth.token?.dataAccess.user
});

const mapDispatch = {
    fetchStat: actions.fetchStat,
    fetchTrainingTask: actions.fetchTrainingTask,
    fetchCheckAnswerTraining: actions.fetchCheckAnswerTraining,
    fetchCheckAnswer: actions.fetchCheckAnswer,
    fetchPessimize: actions.fetchPessimize,
    clearTask: trainingActions.clearTask,
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface IProps extends PropsFromRedux {
    trainingId: string;
}

export enum Dialog {
    Disabled = 0,
    Hint = 1,
    Solution = 2
}

const Task: React.FC<IProps> = (
    {
        user,
        task,
        fetchStat,
        fetchTrainingTask,
        fetchCheckAnswerTraining,
        fetchCheckAnswer,
        fetchPessimize,
        clearTask,
        trainingId
    }
) => {
    const [ dialog, setDialog ] = useState<Dialog>(Dialog.Disabled);
    const close = () => setDialog(Dialog.Disabled);

    const openDialog = (type: Dialog) => {
        if (user && user.type === UserTypes.STUDENT) {
            fetchPessimize({ trainingId });
        }
        setDialog(type);
    };

    const nextTask = () => {
        fetchTrainingTask({ trainingId, forceGenerate: true });

        if (user && user.type === UserTypes.STUDENT) {
            fetchStat();
        }

    };

    const checkTaskAnswerHandler = (data) => {
        return new Promise((resolve) => {
            fetchCheckAnswerTraining({ data, trainingId }, {
                promiseActions: {
                    resolve: (d) => resolve(d.data.payload),
                    reject: () => resolve(false)
                }
            })
        });
    };

    const checkAnswerHandler = (data) => {
        return new Promise((resolve) => {
            fetchCheckAnswer({ data }, {
                promiseActions: {
                    resolve: (d) => resolve(d.data.payload),
                    reject: () => resolve(false)
                }
            })
        });
    };

    useEffect(() => {
        if (trainingId) {
            fetchTrainingTask({ trainingId })
        }

        return () => {
            clearTask();
        }
    }, [ trainingId, fetchTrainingTask, clearTask ]);

    return (
        <div className={styles.content}>
            <div className={styles.contentBody}>
                {task && <TexWebPreview key={task.id} data={task.task as WebPreviewRootData} checkAnswerHandler={checkTaskAnswerHandler} />}
            </div>
            <footer className={styles.contentFooter}>
                <div className={styles.buttons}>
                    <button className={styles.button} type="button" onClick={() => openDialog(Dialog.Hint)}>
                        <span className={styles.buttonIcon}>
                            <QuestionOutlined />
                        </span>
                        Подсказка
                    </button>
                    <button className={styles.button} type="button" onClick={() => openDialog(Dialog.Solution)}>
                        <span className={styles.buttonIcon}>
                            <ExclamationOutlined />
                        </span>
                        Решение
                    </button>
                </div>
                <span onClick={() => nextTask()} className={styles.nextContent}>
                    <span>Следующая задача</span>
                    <RightOutlined className={styles.nextArr} />
                </span>
            </footer>

            {task && dialog === Dialog.Hint && (
                <ModalComponent close={close} title="Подсказка" customStyles={modalStyles}>
                    <TexWebPreview data={task.hint as WebPreviewRootData} checkAnswerHandler={checkAnswerHandler} />
                </ModalComponent>
            )}

            {task && dialog === Dialog.Solution && (
                <ModalComponent close={close} title="Решение аналогичной задачи" customStyles={modalStyles}>
                    <TexWebPreview data={task.solution as WebPreviewRootData} checkAnswerHandler={checkAnswerHandler} />
                </ModalComponent>
            )}
        </div>
    );
};

export default connector(Task);
