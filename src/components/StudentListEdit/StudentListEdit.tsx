import React from 'react';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import { connect, ConnectedProps } from 'react-redux';
import className from 'classnames';

import { IClassData, IStudentData } from '@redux/accountTeacher/interfaces';
import { RootState } from '@store';
import * as actions from '@redux/actions';
import styles from './StudentListEdit.module.scss';

const mapState = (state: RootState) => ({});

const mapDispatch = {
    fetchEditStudent: actions.fetchEditStudent,
    fetchDeleteStudent: actions.fetchDeleteStudent
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    data?: Array<IStudentData>,
    classId: string
}

enum StatusType {
    WAITING = 'waiting',
    ACCESSED = 'accessed',
    ACCESS_DENIED = 'access_denied',
}

interface IStatus {
    title: string;
    type: StatusType;
}

const getStatus = (accessResolution?: boolean | null): IStatus => {
    if (accessResolution === undefined || accessResolution === null) {
        return {
            title: 'Ожидание ответа',
            type: StatusType.WAITING
        };
    }

    if (accessResolution) {
        return {
            title: 'Доступ дан',
            type: StatusType.ACCESSED
        }
    }

    return {
        title: 'Отказ в доступе',
        type: StatusType.ACCESS_DENIED
    }
};

const StudentListEdit: React.FC<IProps> = ({ data, classId, fetchEditStudent, fetchDeleteStudent }) => {
    if (!data || data.length === 0) {
        return null;
    }

    const onBlur = (e: React.FocusEvent<HTMLInputElement>, id: string) => {
        const { value } = e.target;
        fetchEditStudent({ studentId: id, data: { comment: value } });
    };

    const remove = (student: IStudentData) => {
        if (confirm(`Вы уверены что хотите удалить ученика ${student.comment} из класса`)) {
            fetchDeleteStudent({ classId: classId, studentId: student.id })
        }
    };

    return (
        <div className={styles.tableWrap}>
            <table className={styles.table}>
                <thead>
                    <tr>
                        <th className={styles.nameTd}>Отображаемое в таблице имя ученика </th>
                        <th className={styles.idTd}>ID</th>
                        <th className={styles.statusTd}>Статус</th>
                        <th className={styles.closeTd} />
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(item => {
                            const status = getStatus(item.accessResolution);
                            return (
                                <tr key={item.id}>
                                    <td className={styles.nameTd}>
                                        <input
                                            className={styles.input}
                                            type="text"
                                            defaultValue={item.comment || ''}
                                            onBlur={(e) => onBlur(e, item.id)}
                                        />
                                    </td>
                                    <td className={styles.idTd}>
                                        <div className={styles.id}>{item.id}</div>
                                    </td>
                                    <td className={className(styles.statusTd, status.type)}>
                                        {status.title}
                                    </td>
                                    <td className={styles.closeTd}>
                                        <CloseOutlined className={styles.closeIcon} onClick={() => remove(item)} />
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    )
};

export default connector(StudentListEdit);
