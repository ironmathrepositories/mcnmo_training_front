import React, { useContext } from 'react';
import { FieldRenderProps } from 'react-final-form';
import classNames from 'classnames';
import FormStyleContext from '@/context/FormStyleContext';
import styles from '../Input/Input.module.scss';

type IProps = FieldRenderProps<string, any>;

const SelectInput: React.FC<IProps> = ({ options, input, meta, ...rest }) => {
    const formStyleContext = useContext(FormStyleContext);
    const { wrapStyle } = formStyleContext;
    const isError = meta.error && meta.touched;

    return (
        <div className={classNames(styles.wrap, wrapStyle)}>
            <label className={styles.label}>{rest.label}</label>
            <div className={styles.inputWrap}>
                <select className={classNames(styles.input, isError ? styles.inputError : {})} {...input}>
                    {options.map(option => (
                        <option value={option.value} key={option.value}>{option.label}</option>
                    ))}
                </select>
            </div>
        </div>
    )
};

export default SelectInput;

