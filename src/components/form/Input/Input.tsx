import React, { useContext } from 'react';
import { FieldRenderProps } from 'react-final-form';
import classNames from 'classnames';

import FormStyleContext from '@/context/FormStyleContext';
import styles from './Input.module.scss';

type IProps = FieldRenderProps<string, any>;

const Input: React.FC<IProps> = ({ input, meta, ...rest }) => {
    const formStyleContext = useContext(FormStyleContext);
    const { wrapStyle } = formStyleContext;
    const error = meta.error || meta.submitError;
    const isError = error && meta.touched;

    return (
        <div className={classNames(styles.wrap, wrapStyle)}>
            <label className={styles.label}>{rest.label}</label>
            <div className={styles.inputWrap}>
                <input
                    className={classNames(styles.input, isError ? styles.inputError : {})}
                    type="text"
                    placeholder={rest.placeholder}
                    {...input}
                />
                {isError && <div>{error}</div>}
            </div>
        </div>
    );
};

export default Input;
