import React from 'react';
import styles from './Errors.module.scss';

interface IProps {
    errors?: Array<string>
}

const Errors: React.FC<IProps> = ({ errors }) => {
    if (!errors) {
        return null;
    }

    return (
        <ul className={styles.container}>
            {errors.map(error => (<li className={styles.error} key={error}>{error}</li>))}
        </ul>
    );
};

export default Errors;
