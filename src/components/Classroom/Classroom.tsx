import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { RootState } from '@store';
import * as actions from '@actions';
import * as accountTeacherActions from '@redux/accountTeacher/slice';
import EditOutlined from '@ant-design/icons/EditOutlined';
import { IClassData } from '@redux/accountTeacher/interfaces';
import ClassroomEdit from '@/components/ClassroomEdit/ClassroomEdit';
import StudentList from '../StudentList/StudentList';
import styles from './Classroom.module.scss';

interface IPropsOwn {
    data: IClassData
}

const mapState = (state: RootState, ownProps) => ({
    scores: state.accountTeacher.scores[ownProps.data.id]
});

const mapDispatch = {
    fetchScores: actions.fetchScores,
    setEditableClass: accountTeacherActions.setEditableClass
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux, IPropsOwn {}

const Classroom: React.FC<IProps> = ({ data, fetchScores, setEditableClass, scores }) => {
    useEffect(() => {
        fetchScores(data.id);
    }, [ data.id ]);

    const edit = () => {
        setEditableClass(data.id);
    };

    if (data.isEdit) {
        return <ClassroomEdit data={data} />
    }

    return (
        <div className={styles.wrap}>
            <div className={styles.head}>
                <h2 className={styles.title}>
                    {data.name}
                </h2>
                <div className={styles.hButton} onClick={edit}>
                    <EditOutlined />
                    <span className={styles.hButtonText}>Редактировать класс</span>
                </div>
            </div>

            <StudentList data={data.students} classId={data.id} scores={scores} />
        </div>
    );
};

export default connector(Classroom);
