import React from 'react';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import CheckOutlined from '@ant-design/icons/CheckOutlined';
import styles from '../style.scss';

interface IOkCancelFooter {
    okText?: string;
    cancelText?: string;
    okAction: () => void;
    cancelAction: () => void;
}

const OkCancelFooter = ({ okText, cancelText, okAction, cancelAction }: IOkCancelFooter): JSX.Element => {
    const curOkText = okText || 'Готово';
    const curCancelText = cancelText || 'Отмена';

    return (
        <div className={styles['modal-footer']}>
            <div className={styles['footer-buttons']}>
                <button onClick={cancelAction} className={styles.buttonClose}>
                    <CloseOutlined /> {curCancelText}
                </button>
                <button type="submit" onClick={okAction} className={styles.buttonOk}>
                    <CheckOutlined /> {curOkText}
                </button>
            </div>
        </div>
    );
}

export default OkCancelFooter
