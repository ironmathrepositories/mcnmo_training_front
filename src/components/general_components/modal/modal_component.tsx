/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import styles from './style.scss';
import CloseOutlined from '@ant-design/icons/CloseOutlined';

interface IModalComponent {
    close: () => void;
    title: string;
    children: React.ReactNode;
    rootDiv?: string;
    footer?: React.ReactNode; // если footer={null} не будет кнопок 'Отмена'/'Готово'
}

const ModalComponent = (props: IModalComponent): JSX.Element | null => {
    const { close, title, children, rootDiv, footer = null } = props;

    const closeOnEscapeKeyDown = e => {
        if ((e.charCode || e.keyCode) === 27) {
            close();
        }
    };

    useEffect(() => {
        document.body.addEventListener('keydown', closeOnEscapeKeyDown);
        return () => {
            document.body.removeEventListener('keydown', closeOnEscapeKeyDown);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const curRootDiv = rootDiv || 'app';
    const portalDiv = document.getElementById(curRootDiv);

    if (!portalDiv) {
        return null;
    }

    return ReactDOM.createPortal(
        <div className={styles.modal} onClick={close}>
            <div className={styles['modal-content']} onClick={e => e.stopPropagation()}>
                <div className={styles['modal-header']}>
                    <h4 className={styles['modal-title']}>{title}</h4>
                    <div onClick={close} className={styles.headerClose}>
                        <CloseOutlined />
                    </div>
                </div>
                <div className={styles['modal-body']}>
                    {children}
                </div>
                {footer}
            </div>
        </div>,
        portalDiv
    );
};

export default ModalComponent;
