import React, { useState } from 'react';
import ModalComponent from '../../modal_component';
import { useDispatch } from 'react-redux';
import styles from './styles.scss';
// import { fetchCreateDirectory } from '@redux/actions';
import { toastError } from '@utils/toast_error';
import OkCancelFooter from '../../footers/ok_cancel';

// todo: сохранено для примера того, как создавать модалки - взято из Теории.
interface IType {
    close: () => void;
    title: string;
}

const CreateRootFolderModal = ({ close, title }: IType): JSX.Element => {
    const [ name, setName ] = useState('');
    const [ required, showRequired ] = useState(false);
    const dispatch = useDispatch();

    const changeName = (e) => {
        const name = e.target.value;
        if (name && required) {
            showRequired(false);
        }
        setName(name);
    }

    const submit = () => {
        if (!name) {
            showRequired(true)
            toastError('Введите имя папки');
            return null;
        }
        showRequired(false)
        // dispatch(fetchCreateDirectory(`/${name}`));
        close()
    }

    const curTitle = title || 'Создать папку';
    const Footer = <OkCancelFooter okAction={submit} cancelAction={close} />

    return <ModalComponent close={close} title={curTitle} footer={Footer}>
        <div className={styles.createFolder}>
            <div className={styles.inputTexts}>
                <span>Название папки:</span>
                {required && <span className={styles.required}>Обязательное поле</span>}
            </div>
            <input className={styles.folderNameInput} type="text" name="name" value={name} placeholder="Название" onChange={changeName} />
        </div>
    </ModalComponent>;
};

export default CreateRootFolderModal;
