import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getModalState } from '@redux/modal/selectors';
import { closeModal } from '@redux/actions';
import modals from './modals';

const ModalWrapper = (): JSX.Element | null => {
    const { modalName, isOpen, title } = useSelector(getModalState);
    const dispatch = useDispatch();
    const Modal = modals[modalName];

    const close = () => {
        dispatch(closeModal());
    }

    if (!isOpen || !Modal) {
        if (isOpen && !Modal) {
            console.error(`Нет такого модального окна: ${modalName} `);
        }
        return null;
    }

    return <Modal close={close} title={title} />;

};

export default ModalWrapper;
