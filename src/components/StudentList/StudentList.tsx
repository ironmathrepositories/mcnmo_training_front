import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import CloseOutlined from '@ant-design/icons/CloseOutlined';

import { RootState } from '@store';
import * as actions from '@actions';
import { IScoresRows, IStudentData } from '@redux/accountTeacher/interfaces';
import { getPercentColorGradient } from '@utils/getPercentColor';
import styles from './StudentList.module.scss';

const mapState = (state: RootState) => ({});

const mapDispatch = {
    fetchDeleteRecommendClass: actions.fetchDeleteRecommendClass,
};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

interface IProps extends PropsFromRedux {
    classId: string;
    data?: Array<IStudentData>,
    scores?: IScoresRows,
}


const StudentList: React.FC<IProps> = (
    {
        classId,
        data,
        scores,
        fetchDeleteRecommendClass
    }
) => {
    if (!scores?.length) {
        return null;
    }

    const deleteRecommend = (trainingId: string, classId: string) => {
        if (confirm('Вы уверены что хотите удалить класс из рекоммендаций?')) {
            fetchDeleteRecommendClass({ trainingId, classId });
        }
    };

    return (
        <div className={styles.tableWrap}>
            <table className={styles.table}>
                <thead>
                    <tr>
                        <th className={styles.tdTitle}>
                            <div className={styles.tdTitleHead}>
                                <div className={styles.tdTitleHeadTop}>
                                    Ученики
                                </div>
                                <div className={styles.tdTitleHeadBottom}>
                                    Название тренинга
                                </div>
                            </div>
                        </th>
                        {data && data.map((student) => (
                            <th className={styles.colFio} key={student.id}>
                                <div className={styles.fio}>
                                    <b>{student.comment}</b>
                                    {/*<div>{student.name}</div>*/}
                                </div>
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {scores.map(item => (
                        <tr key={item.id}>
                            <td className={styles.tdTitle}>
                                <span className={styles.closeIconWrap} onClick={() => deleteRecommend(item.id, classId)}>
                                    <CloseOutlined />
                                </span>
                                <Link to={`/training/${item.examDirectionId}/${item.trainingCollectionId}/${item.id}`}>{item.name}</Link>
                            </td>
                            {data && data.map((student) => {
                                const score = item.scores[student.id];
                                const num = score ? String(score).split('.') : [ 0 ];

                                return (
                                    <React.Fragment key={student.id}>
                                        <td className={styles.gradeTd} style={{ backgroundColor: getPercentColorGradient(Number(score)) }}>
                                            {score ? (
                                                <>
                                                    {num[0]}
                                                    {num[1] && (
                                                        <span className={styles.tenths}>,{num[1]}</span>
                                                    )}
                                                </>
                                            ) : <span className={styles.tenths}>—</span>}
                                        </td>
                                    </React.Fragment>
                                );
                            })}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
};

export default connector(StudentList);
