import React, { useState, useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import CaretDownOutlined from '@ant-design/icons/CaretDownOutlined';

import * as trainingI from '@redux/training/interfaces';
import styles from './Select.module.scss';

interface IProps {
    list: Array<trainingI.ITrainingDirection>
}

const SelectTitle: React.FC<IProps> = ({ list }) => {
    const [ isOpen, setOpen ] = useState(false);
    const close = useCallback(() => setOpen(false), [ setOpen ]);

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('click', close);
        }

        return () => {
            window.removeEventListener('click', close);
        }
    }, [ isOpen, close ]);

    return (
        <div className={classNames(styles.selectWrap, isOpen ? styles.selectWrapOpen : {})}>
            <div className={styles.select} onClick={() => setOpen(!isOpen)}>
                К другим тренингам
                <div className={styles.selectChk}>
                    <CaretDownOutlined />
                </div>
            </div>
            {isOpen && (
                <ul className={styles.selectList}>
                    {list.map(item => (
                        <li className={styles.selectItem} key={item.id}>
                            <Link to={`/topics/${item.id}`}>{item.name}</Link>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default SelectTitle;
