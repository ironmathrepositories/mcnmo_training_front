import React, { useState, useEffect, useCallback } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import * as I from './interfaces';
import CaretDownOutlined from '@ant-design/icons/CaretDownOutlined';

import styles from './Select.module.scss';

interface IProps {
    list: Array<I.IOption>;
    defaultValue?: string;
    placeholder?: string;
    disabled?: boolean;
}

const Select: React.FC<IProps> = ({ list, defaultValue, placeholder, disabled }) => {
    const [ isOpen, setOpen ] = useState(false);
    const [ value, setValue ] = useState(defaultValue);
    const close = useCallback(() => setOpen(false), [ setOpen ]);

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('click', close);
        }

        return () => {
            window.removeEventListener('click', close);
        }
    }, [ isOpen, close ]);

    const activeItem = list.find(item => item.value === value);

    return (
        <div className={classNames(styles.selectWrap, isOpen ? styles.selectWrapOpen : {})}>
            <div className={styles.select} onClick={() => !disabled && setOpen(!isOpen)}>
                {activeItem?.label || placeholder}
                <div className={classNames(styles.selectChk, disabled ? styles.selectChkDisabled : undefined)}>
                    <CaretDownOutlined />
                </div>
            </div>
            {isOpen && (
                <ul className={styles.selectList}>
                    {list.map(item => (
                        <li className={styles.selectItem} key={item.value}>
                            <Link to={`/${item.value}`}>{item.label}</Link>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default Select;
