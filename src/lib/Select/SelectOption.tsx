import React from 'react';
import classNames from 'classnames';
import { MODE } from './constants';
import { ISelectOption } from './interfaces';
import styles from './module.select.scss';

const SelectOption: React.FC<ISelectOption> = (
    {
        option,
        selectedOption,
        keyUpOptionHandler,
        setOption,
        setParent,
        mode,
        search
    }
) => {
    const handlerClick = (e: React.MouseEvent<HTMLDivElement>) => {
        setParent(option.value);
        e.stopPropagation();
    };

    const getLabel = () => {
        if (option.component) {
            const Component = option.component;
            return Component();
        }

        if (search && mode === MODE.SEARCH) {
            const reg = new RegExp(search, 'ig');
            const html = option.label.replace(reg, s => `<b>${s}</b>`);
            return <span dangerouslySetInnerHTML={{ __html: html }} />;
        }

        return option.label;
    };

    return (
        <div
            role="button"
            tabIndex={0}
            className={classNames(
                styles.option,
                selectedOption && option.value === selectedOption.value && styles.selected,
                option.disabled && styles.disabled
            )}
            onClick={() => setOption(option)}
            onKeyUp={keyUpOptionHandler}
        >
            {getLabel()}
            {mode !== MODE.SEARCH && option.isParent && (
                <div
                    className={styles.optionNext}
                    role="button"
                    tabIndex={-1}
                    onClick={handlerClick}
                    onKeyUp={() => {}}
                >
                    &gt;
                </div>
            )}
        </div>
    );
};

export default SelectOption;
