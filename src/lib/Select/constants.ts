export const SEARCH_TEXT = 'search';
export const ANIMATE_TIME = 1000; // Берём из transition

export enum ANIMATE {
    NONE = '',
    LEFT = 'Left',
    RIGHT = 'Right'
}

export enum MODE {
    DEFAULT,
    SEARCH
}
