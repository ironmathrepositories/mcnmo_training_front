import React, { useCallback, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import DownOutlined from '@ant-design/icons/DownOutlined';
import { setArrayUniqueId } from './utils/setUniqueId';
import SelectOptions from './SelectOptions';
import { IOption, ISelect } from './interfaces';
import { ANIMATE, MODE } from './constants';
import styles from './module.select.scss';

// Возвращаем все элементы у которых есть дети
const getParents = (options: IOption[]) => {
    return options.reduce((result: string[], option) => {
        return option.parent
            ? [ ...result, option.parent ]
            : result;
    }, []);
};

const isParent = (elem: HTMLElement, parentElem: HTMLElement): boolean => {
    let node: HTMLElement | null = elem;

    while (node) {
        if (node === parentElem) {
            return true;
        }
        node = node.parentElement;
    }

    return false;
};

const NOT_SELECTED = 'Не выбранно';
const MAX_ITEM = 10;

export const Select: React.FC<ISelect> = (
    {
        options: defaultOptions,
        defaultValue,
        fullPathValue,
        isSearch,
        isOutlined,
        placeholder= '',
        onChange,
        open = false,
        onChangeOpen,
        extendStyles
    }
) => {
    let time: ReturnType<typeof setTimeout>;
    const ref = useRef<HTMLDivElement>(null);
    const [ selectedOption, setSelectedOption ] = useState<IOption>();
    const [ isOpen, setOpen ] = useState<boolean>(open);
    const [ options, setOptions ] = useState<IOption[]>();
    const [ mode, setMode ] = useState<MODE>(MODE.DEFAULT);
    const [ search, setSearch ] = useState<string>('');
    const [ parent, setParent ] = useState<string>('');
    const [ animate, setAnimate ] = useState(ANIMATE.NONE);

    useEffect(() => {
        const parentList = getParents(defaultOptions);
        //
        const newOptions = defaultOptions.map(option => ({
            ...option,
            isParent: parentList.includes(option.value)
        }));

        setOptions(setArrayUniqueId(newOptions));

        // Если после смены опций её не оказалось в списке её надо убрать, если поменялся язык, надо вывести опцию с новым языком
        if (selectedOption) {
            const newSelectedOption = newOptions.find(item => item.value === selectedOption.value);
            setSelectedOption(newSelectedOption || undefined);
        }
    }, [ defaultOptions ]);

    useEffect(() => {
        let option = defaultValue !== undefined && defaultOptions.find(item => item.value === defaultValue);

        if (!option) {
            option = defaultOptions.find(item => item.selected);
        }

        if (option) {
            if (!selectedOption || option.value !== selectedOption.value) {
                setSelectedOption(option);
                if (onChange) {
                    onChange(option.value, option);
                }
            }
        }
    }, []);

    // Управяем открытием или закрытием опций из внешнего компонента
    useEffect(() => {
        if (open !== isOpen) {
            setOpen(open);
            setAnimate(ANIMATE.NONE);
            updateParent();
        }
    }, [ open ]);

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('click', hideOptionsByClick);
        }

        return () => {
            window.removeEventListener('click', hideOptionsByClick);
            clearTimeout(time);
        };
    }, [ isOpen ]);

    const updateSearch = (val: string) => {
        setSearch(val);
        setMode(MODE.SEARCH);
    };

    const hideOptionsByClick = (e: MouseEvent) => {
        const elem = e.target as HTMLElement;

        if (!ref.current) {
            return;
        }

        if (!isParent(elem, ref.current)) {
            toggleOpen(false);
        }
    };

    const toggleOpen = (val?: boolean) => {
        updateParent();
        const newOpen = val !== undefined ? val : !isOpen;
        setOpen(newOpen);
        if (onChangeOpen) {
            onChangeOpen(newOpen);
        }
        setAnimate(ANIMATE.NONE);
    };

    // Делаем активным родителя выбранной опции
    const updateParent = () => {
        if (!selectedOption) {
            changeParent('');
            return;
        }

        changeParent(selectedOption.parent || '');
    };

    const changeParent = (val: string) => {
        setParent(val);
        setAnimate(ANIMATE.RIGHT);
    };

    const getFullPath = (path: string[], option: IOption): string[] => {
        const newPath = [ option.label, ...path ];

        if (option.parent) {
            const parentOption = options?.find(item => item.value === option.parent);
            if (!parentOption) {
                return newPath;
            }
            return getFullPath(newPath, parentOption);
        }

        return newPath;
    };

    const getLabelText = ():string => {
        if (!selectedOption && isSearch) {
            return '';
        }

        if (!selectedOption) {
            return NOT_SELECTED;
        }

        if (!fullPathValue || !selectedOption.parent) {
            return selectedOption.label;
        }

        const path = getFullPath([], selectedOption);
        return path.join( ' / ');
    };

    const getTitle = (): string | React.FC => {
        if (selectedOption && selectedOption.component) {
            const Component = selectedOption.component;
            return Component();
        }

        return getLabelText();
    };

    const getValue = () => {
        return mode === MODE.SEARCH ? search : getLabelText();
    };

    const keyUpLabelHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
        switch (e.key) {
            case 'Enter' : toggleOpen();
        }
    };

    const clickInputHandler = (e : React.MouseEvent) => {

    };

    const focusInputHandler = () => {
        if (!isOpen) {
            toggleOpen(true);
        }
    };

    const changeInputHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        const elem = e.target as HTMLInputElement;
        updateSearch(elem.value);
        e.stopPropagation();
    };

    const keyUpOptionHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
    // console.log(e.key);
        switch (e.key) {
            case 'ArrowUp' : {
                // console.log('ArrowUp()');
                break;
            }
            case 'ArrowDown' : {
                // console.log('ArrowDown()');
                break;
            }
            case 'Enter' : {
                // console.log('Enter()');
            }
        }
    };

    const back = () => {
        const option = options?.find(item => item.value === parent);
        const val = option && option.parent ? option.parent : '';

        time = setTimeout(() => {
            setParent(val);
            setAnimate(ANIMATE.LEFT);
        }, 0);
    };

    const setOption = useCallback((option: IOption) => {
        if (option.disabled) {
            return;
        }

        setSelectedOption(option);
        if (onChange) {
            onChange(option.value, option);
        }
        setSearch(option.label);
        toggleOpen(false);
        setMode(MODE.DEFAULT);
    }, []);

    if (!options) {
        return null;
    }

    const resultOptions =
      mode === MODE.SEARCH
          ? options
              .filter(option => !search || option.label.toLowerCase().indexOf(search.toLowerCase()) !== -1)
              .slice(0, MAX_ITEM)
          : options.filter(option => parent ? option.parent === parent : !option.parent);

    const iconClose = <DownOutlined onClick={() => toggleOpen()} />;

    return (
        <div className={styles.select} ref={ref}>
            <div className={styles.label}>
                {isSearch ? (
                    <input
                        className={styles.input}
                        onClick={clickInputHandler}
                        onFocus={focusInputHandler}
                        onChange={changeInputHandler}
                        placeholder={placeholder}
                        value={getValue()}
                        // value={search}
                    />
                ) : (
                    <div
                        className={styles.title}
                        onClick={() => toggleOpen()}
                        role="button"
                        tabIndex={-1}
                        onKeyUp={keyUpLabelHandler}
                    >
                        {getLabelText()}
                    </div>
                )}

                {isOutlined && (
                    <div className={classNames({
                        select__chk: true,
                        select__chk_open: isOpen
                    })}
                    >
                        {iconClose}
                    </div>
                )}
            </div>

            {isOpen && (
                <SelectOptions
                    options={resultOptions}
                    selectedOption={selectedOption}
                    keyUpOptionHandler={keyUpOptionHandler}
                    setOption={setOption}
                    setParent={changeParent}
                    parent={parent}
                    back={back}
                    animate={animate}
                    mode={search ? mode : MODE.DEFAULT}
                    search={search}
                />
            )}
        </div>
    );
};
