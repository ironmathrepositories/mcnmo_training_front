export function genId() {
    return Math.random().toString();
}

export function setUniqueId<T>(item: T): T {
    return { ...item, id: genId() };
}

// Для каждого элемента массива создаём id
export function setArrayUniqueId<T>(arr: Array<T>): Array<T> {
    // @ts-ignore
    return arr.map((item, i) => ({ ...item, id: item.id ? item.id : `${i}_${genId()}` }));
}
