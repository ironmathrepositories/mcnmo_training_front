import React from 'react';
import { MODE } from './constants';

export interface IStyles {
    [className: string]: string;
}

export interface IOption {
    label: string;
    value: string;
    // component?: React.FC,
    component?: () => React.FC,
    id?: string;
    parent?: string;
    isParent?: boolean;
    selected?: boolean;
    disabled?: boolean;
}

export interface ISelectOption {
    option: IOption;
    selectedOption?: IOption;
    keyUpOptionHandler: (e: React.KeyboardEvent<HTMLDivElement>) => void;
    setOption: (option: IOption) => void;
    setParent: (value: string) => void;
    mode: MODE;
    search: string;
}

export interface ISelect {
    options: IOption[];
    defaultValue?: string;
    fullPathValue?: boolean;
    isSearch?: boolean;
    isOutlined?: boolean;
    placeholder?: string;
    onChange?: (value: string, option?: IOption) => void;
    open?: boolean;
    onChangeOpen?: (open: boolean) => void;
    extendStyles?: IStyles;
}
