import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { ANIMATE, ANIMATE_TIME, MODE } from './constants';
import SelectOption from './SelectOption';
import { IOption } from './interfaces';
import styles from './module.select.scss';

interface ISelectOptions {
    parent: string;
    options: IOption[];
    selectedOption?: IOption;
    keyUpOptionHandler: (e: React.KeyboardEvent<HTMLDivElement>) => void;
    setOption: (option: IOption) => void;
    setParent: (option: string) => void;
    back: () => void;
    animate: ANIMATE;
    mode: MODE;
    search: string;
}

const SelectOptions: React.FC<ISelectOptions> = (
    {
        parent,
        options,
        selectedOption,
        keyUpOptionHandler,
        setOption,
        setParent,
        back,
        animate,
        mode,
        search
    }
) => {
    const ref = useRef<HTMLDivElement>(null);
    const [ animateStart, setAnimateStart ] = useState(false);
    const [ animateLeave, setAnimateLeave ] = useState(false);
    let time: ReturnType<typeof setTimeout>;
    let animateId: ReturnType<typeof requestAnimationFrame>;

    useEffect(() => {

        function endAnimate() {
            setAnimateStart(false);
            setAnimateLeave(false);
            ref.current?.removeEventListener('transitionend', endAnimate);
            clearTimeout(time);
        }

        function beginAnimate() {
            setAnimateLeave(true);
            ref.current?.addEventListener('transitionend', endAnimate);
            // Если бразер не поддерживает transitionend, а поддерживает его разновидности типа ontransitionend, webkitTransitionEnd
            time = setTimeout(endAnimate, ANIMATE_TIME);
        }

        if (animate !== ANIMATE.NONE) {
            setAnimateStart(true);
            animateId = window.requestAnimationFrame(beginAnimate);
        }

        return function cleanup() {
            clearTimeout(time);
            window.cancelAnimationFrame(animateId);
            if (ref.current) {
                ref.current.removeEventListener('transitionend', endAnimate);
            }
        };
    }, [ parent ]);

    return (
        <div
            className={classNames(
                styles.options,
                animateStart && styles[`options${animate}Start`],
                animateLeave && styles[`options${animate}Leave`]
            )}
        >
            <div className={styles.optionsContent} ref={ref}>
                {mode !== MODE.SEARCH && parent && (
                    <div
                        role="button"
                        tabIndex={0}
                        className={classNames(styles.option, styles.optionBack)}
                        onClick={back}
                        onKeyUp={() => {}}
                    >
                        &lt; Назад
                    </div>
                )}
                {options.map(option => (
                    <SelectOption
                        key={`${option.id}`}
                        option={option}
                        selectedOption={selectedOption}
                        keyUpOptionHandler={keyUpOptionHandler}
                        setOption={setOption}
                        setParent={setParent}
                        mode={mode}
                        search={search}
                    />
                ))}
            </div>
        </div>
    );
};

export default SelectOptions;
