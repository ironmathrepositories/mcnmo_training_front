import { IUser } from '@redux/auth/interfaces';

export const getUserName = (user: IUser): string => {
    const result: Array<string | undefined> = [ user.surname, user.name, user.patronymic ].filter(i => i);

    return result.join(' ');
};
