import { isArray } from '@utils/is_array';

export const isBaseObject = (possibleObject: unknown): possibleObject is Record<PropertyKey, unknown> => {
    return Object.prototype.toString.call(possibleObject) === '[object Object]';
}

// eslint-disable-next-line @typescript-eslint/ban-types
export const isObject = (possibleObject: unknown): possibleObject is Object => {
    return typeof possibleObject === 'object' && possibleObject !== null && !isArray(possibleObject);
}