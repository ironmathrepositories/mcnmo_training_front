import { toast } from 'react-toastify';
import { hasOwnProperty } from './has_own_property';
import { isAxiosError } from './is_axios_error';
import { isBaseError } from './is_base_error';

const getErrorMessage = (error: unknown, defaultMessage?: string): string | undefined => {
    if (typeof error === 'string') return error;
    if (isAxiosError(error)) return error.message; // todo: нужно переделать на ErrorResponse интерфейс (@types/error_response.d.ts) - необходимо этот интерфейс предложить бэку.
    if (isBaseError(error) || (hasOwnProperty<string>(error, 'message') && typeof error.message === 'string')) {
        // @ts-ignore
        return error.message;
    }
    return defaultMessage;
}

export const toastError = (error: Error | string, defaultMessage?: string): void => {
    if (error || defaultMessage) {
        const message = getErrorMessage(error, defaultMessage);

        if (message) {
            toast.error(message);
            return;
        }

        console.error('не удалось сформировать сообщение ошибки в toastError: ', { error, defaultMessage });
    } else console.error('нет сообщения об ошибке в toastError');
    toast.error('Операция завершилась с ошибкой!');
};

export const toastSuccess = (message: string): void => {
    if (!message || typeof message !== 'string') {
        console.error('нет сообщения в toastSuccess ', message);
        toast.warning('Операция завершилась успешно, но не передано сообщение или оно имеет неверный формат.');
    } else toast.success(message);
};
