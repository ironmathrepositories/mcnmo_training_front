export const isNumber = (possibleNumber: unknown): possibleNumber is number => {
    return typeof possibleNumber === 'number' && !Number.isNaN(possibleNumber);
}