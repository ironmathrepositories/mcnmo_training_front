import { isObject } from '@utils/is_object';

export const hasOwnProperty = <T = unknown>(object: unknown, key: PropertyKey): object is Record<PropertyKey, unknown> & { key: T } => {
    if (!isObject(object)) return false;
    return Object.prototype.hasOwnProperty.call(object, key);
}
