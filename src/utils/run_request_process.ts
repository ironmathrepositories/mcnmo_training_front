import { call, put, takeEvery, select } from 'redux-saga/effects';
import { AxiosError, AxiosResponse  } from 'axios';
import { toast } from 'react-toastify';
import { Action, PromisifiedAction } from '@/types/action';
import { isFunction } from '@utils/is_function';
import * as api from '@/api';
import * as actions from '@/redux/actions';
import * as authSliceActions from '@redux/auth/slice';
import { isAxiosError } from './is_axios_error';
import { ResponsePreprocessorReturn, ResponseProcessGenerator } from '@/types/response_process_generator';
import { ErrorResponse } from '@/types/error_response';
import { isBaseObject } from './is_object';
import { isGenerator } from './is_generator';
import { Codes } from '@/constants';
import { RootState } from '@store';
import { IMessages } from '@/interfaces';

export class BreakRequestProcess extends Error {}

const processingMessages = (data: IMessages) => {
    data.map(item => {
        if (item.field) return;
        switch (item.type?.toLowerCase()) {
            case 'info':
                toast.info(item.message);
                break;
            case 'success':
                toast.success(item.message);
                break;
            default:
                toast.error(item.message);
        }
    });
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function runRequestProcess<R, P>(type: string, responseProcessor?: ResponseProcessGenerator<R, P>) {
    if (typeof api[type] !== 'function')
        throw new Error(`[error][runRequestProcess] Api action is not defined: "${type}".`);

    const success = `${type}Success`;
    const failure = `${type}Failure`;

    const requestProcessor = function* (action: Action<P> | PromisifiedAction<P, AxiosResponse<R>, AxiosError<ErrorResponse>>) {
        try {
            let response: AxiosResponse<R> = yield call(api[type], action.payload);

            if (isFunction(responseProcessor)) {
                const responseProcessorGenerator = responseProcessor(response, null, action);
                if (!isGenerator<unknown, ResponsePreprocessorReturn<R>, unknown>(responseProcessorGenerator))
                    throw new Error('[error][runRequestProcess] responseProcessor must be a generator.')
                const { data: processedResponse } = yield* responseProcessorGenerator;
                if (processedResponse) response = processedResponse;
            }

            if (action.meta?.promiseActions && isBaseObject(action.meta.promiseActions) && isFunction(action.meta.promiseActions.resolve)) {
                action.meta.promiseActions.resolve(response);
            }

            // @ts-ignore
            const displayMessages = response.data.displayMessages || [];
            processingMessages(displayMessages);

            if (!isFunction(actions[success]))
                throw new Error(`[error][runRequestProcess] Redux action is not defined: "${success}".`);
            yield put(actions[success](response, action.payload));

        } catch (error) {
            if (error instanceof BreakRequestProcess) return;

            if (isAxiosError(error)) {
                if (error.response
                    && error.response.status === 401
                    && error.response.data.code === Codes.EXPIRED
                    && type !== 'fetchRefresh'
                ) {
                    const refreshActions = yield select((state: RootState) => state.auth.refreshActions);
                    if (!refreshActions) {
                        yield put(actions.fetchRefresh());
                    }
                    yield put(authSliceActions.addRefreshAction({ type, action }));
                    return;
                }

                const displayMessages = error.response?.data?.displayMessages || [];
                processingMessages(displayMessages);

                if (isFunction(responseProcessor)) {
                    const responseProcessorGenerator = responseProcessor(null, error, action);
                    if (!isGenerator<unknown, ResponsePreprocessorReturn<R>, unknown>(responseProcessorGenerator)) {
                        throw new Error('[error][runRequestProcess] responseProcessor must be a generator.');
                    }
                    const { error: processedError } = yield* responseProcessorGenerator;
                    if (processedError) error = processedError;
                }

                if (action.meta?.promiseActions && isBaseObject(action.meta.promiseActions) && isFunction(action.meta.promiseActions.reject)) {
                    action.meta.promiseActions.reject(error);
                }

                if (!isFunction(actions[failure])) {
                    throw new Error(`[error][runRequestProcess] Redux action is not defined: "${failure}".`);
                }

                yield put(actions[failure](error, action.payload));
            } else {
                throw error;
            }
        }
    };

    return function* () {
        yield takeEvery(type, requestProcessor);
    }
}
