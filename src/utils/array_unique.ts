export const arrayUnique = <T = unknown>(array: T[]): T[] => {
    if (!Array.isArray(array)) throw new Error('[error] arrayUnique: value must be an array!');
    return [ ...new Set(array) ];
}
