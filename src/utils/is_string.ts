export const isString = (possibleString: unknown): possibleString is string => {
    return typeof possibleString === 'string';
}