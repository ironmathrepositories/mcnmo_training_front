import { ErrorResponse } from '@/types/error_response';
import { AxiosError } from 'axios';
import { hasOwnProperty } from '@utils/has_own_property';
import { isBaseError } from '@utils/is_base_error';

export const isAxiosError = (possibleAxiosError: unknown): possibleAxiosError is AxiosError<ErrorResponse> => {
    if (!isBaseError(possibleAxiosError)) return false;
    return hasOwnProperty(possibleAxiosError, 'isAxiosError') && !!possibleAxiosError.isAxiosError;
}