export const isArray = (possibleArray: unknown): possibleArray is Array<unknown> => {
    return Array.isArray(possibleArray);
}