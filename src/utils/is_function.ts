// eslint-disable-next-line @typescript-eslint/ban-types
export const isFunction = (possibleFunction: unknown): possibleFunction is Function => {
    return typeof possibleFunction === 'function';
}