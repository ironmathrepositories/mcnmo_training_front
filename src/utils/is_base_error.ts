export const isBaseError = (possibleError: unknown): possibleError is Error => {
    return Object.prototype.toString.call(possibleError) === '[object Error]';
}