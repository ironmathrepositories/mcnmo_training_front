export const isGenerator = <Y, R, N>(possibleGenerator: unknown): possibleGenerator is Generator<Y, R, N> => {
    return Object.prototype.toString.call(possibleGenerator) === '[object Generator]'; // it works correctly only for ES6+
}