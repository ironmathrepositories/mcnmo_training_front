import { Action, PromisifiedAction } from '@/types/action';
import { EnhancedStore } from '@reduxjs/toolkit';

export const getPromisifiedDispatch = (store: EnhancedStore) => {
    return <P, R, E>(action: Action<P>): Promise<R | E> => {
        return new Promise((resolve, reject) => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            store.dispatch({
                ...action,
                meta: {
                    promiseActions: {
                        resolve,
                        reject
                    }
                }
            } as PromisifiedAction<P, R, E>);
        });
    }
}