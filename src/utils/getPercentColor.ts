export enum PERCENT_COLOR {
    TRY_AGAIN = '#FF2559',
    BAD = '#FF2559',
    GOOD = '#FFB525',
    EXCELLENT = '#04CF00'
}

export const getPercentColor = (percent: number): PERCENT_COLOR => {
    const per = Number(percent);
    if (per < 40) {
        return PERCENT_COLOR.TRY_AGAIN;
    } else if (per < 55) {
        return PERCENT_COLOR.BAD;
    } else if (per < 70) {
        return PERCENT_COLOR.GOOD;
    }

    return PERCENT_COLOR.EXCELLENT;
};


const hex = function (x) {
    x = x.toString(16);
    return (x.length == 1) ? '0' + x : x;
};

const getGradient = (color1: string, color2: string, ratio: number): string => {
    const r = Math.ceil(parseInt(color1.substring(0, 2), 16) * (1 - ratio) + parseInt(color2.substring(0, 2), 16) * ratio);
    const g = Math.ceil(parseInt(color1.substring(2, 4), 16) * (1 - ratio) + parseInt(color2.substring(2, 4), 16) * ratio);
    const b = Math.ceil(parseInt(color1.substring(4, 6), 16) * (1 - ratio) + parseInt(color2.substring(4, 6), 16) * ratio);

    const color = hex(r) + hex(g) + hex(b);
    return `#${color}`;
};

/*
D85555 - 0%
E36F4D - 17%
EF8A45 - 33%
F8B755 - 50%
FFD961 - 67%
B1CC3C - 83%
6FC11C - 100%
 */
export const getPercentColorGradient = (percent: number): string => {
    const per = Number(percent);
    if (!percent) {
        return 'transparent';
    }
    if (per <= 0) return '#D85555';
    if (per < 17) {
        return getGradient('D85555', 'E36F4D', per/17);
    }
    if (per === 17) return '#E36F4D';
    if (per < 33) {
        return getGradient('E36F4D', 'EF8A45', (per - 17)/16);
    }
    if (per === 33) return '#EF8A45';
    if (per < 50) {
        return getGradient('EF8A45', 'F8B755', (per - 33)/17);
    }
    if (per === 50) return '#F8B755';
    if (per < 67) {
        return getGradient('F8B755', 'FFD961', (per - 50)/17);
    }
    if (per === 67) return '#FFD961';
    if (per < 83) {
        return getGradient('FFD961', 'B1CC3C', (per - 67)/16);
    }
    if (per === 83) return '#B1CC3C';
    if (per < 100) {
        return getGradient('B1CC3C', '6FC11C', (per - 83)/17);
    }

    return '#6FC11C';
};
