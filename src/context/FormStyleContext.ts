import React from 'react';

export const defaultValue = {
    wrapStyle: 'default'
};
export const brForm = {
    wrapStyle: 'break'
};
export const authForm = {
    wrapStyle: 'auth'
};
const FormStyleContext = React.createContext(defaultValue);

export default FormStyleContext;
