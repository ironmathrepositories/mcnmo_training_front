export interface IError {
    type?: string;
    field?: string;
    message: string;
}

export type IErrors = Array<Error>;

