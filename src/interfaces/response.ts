import { ErrorResponse } from '@/types/error_response';
import { Codes } from '@/constants';

export type IResponseSuccess<P> = {
    type: Codes,
    payload: P
}

export type IResponse<P> = ErrorResponse | IResponseSuccess<P>;

export interface IPagination<T> {
    meta: {
        total: number; // общее количество страниц
        page: number; // номер текущей страницы
        chunk: number; // количество на странице
    },
    list: Array<T>
}
