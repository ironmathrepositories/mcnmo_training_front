export interface Message {
    type?: string;
    message: string;
    field?: string;
}

export type IMessages = Array<Message>;
