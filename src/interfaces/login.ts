export enum UserTypes {
    TEACHER = 'Teacher',
    STUDENT = 'Student'
}
