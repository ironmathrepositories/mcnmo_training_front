import agent from './agent';
import { AxiosResponse } from 'axios';
import * as IAccountTeacher from '@redux/accountTeacher/interfaces';

// Список рекомендованных классов
export const fetchClassData = (): Promise<AxiosResponse<IAccountTeacher.IClassDataResponse>> => {
    return agent.GET('/account/teacher/classes', { full: true });
};

// Информация по классу
export const fetchClassDataItem = (classId: string): Promise<AxiosResponse<IAccountTeacher.IClassDataItemResponse>> => {
    return agent.GET(`/account/teacher/class/${classId}`, { full: true });
};

// Добавление класса
export const fetchAddClass = (data: IAccountTeacher.IAddClassPayload): Promise<AxiosResponse<IAccountTeacher.IAddClassResponse>> => {
    return agent.POST('/account/teacher/class', data);
};

// Редактирование класса
export const fetchEditClass = (payload: IAccountTeacher.IEditClassPayload): Promise<AxiosResponse<IAccountTeacher.IEditClassResponse>> => {
    const { id, data } = payload;
    return agent.PUT(`/account/teacher/class/${id}`, data);
};

// Список рекомендованных классов
export const fetchRecommendedClasses = (trainingId: string): Promise<AxiosResponse<IAccountTeacher.IRecommendedClassesResponse>> => {
    return agent.GET(`/trainings/${trainingId}/recommended-classes`);
};

// Удалить рекоммендацию к классу
export const fetchDeleteRecommendClass = (payload: IAccountTeacher.ITrainingRecommendedClassPayload): Promise<AxiosResponse<IAccountTeacher.IRecommendedClassesResponse>> => {
    const { trainingId, classId } = payload;
    return agent.PUT(`/trainings/${trainingId}/recommend`, { [classId]: false });
};

// Получение данных по оценкам учеников
export const fetchScores = (classId: string): Promise<AxiosResponse<IAccountTeacher.IScoresResponse>> => {
    return agent.GET(`/account/teacher/scores/${classId}`);
};

// Добавление ученика в класс
export const fetchAddStudent = (data: IAccountTeacher.IAddStudentPayload): Promise<AxiosResponse<IAccountTeacher.IAddStudentResponse>> => {
    const { classId, studentId } = data;
    return agent.PUT(`/account/teacher/add-student/${classId}`, { id: studentId });
};

// Изменение ФИО ученика
export const fetchEditStudent = (payload: IAccountTeacher.IEditStudentPayload): Promise<AxiosResponse<IAccountTeacher.IEditStudentResponse>> => {
    const { studentId, data } = payload;
    return agent.PUT(`/account/teacher/override-student-name/${studentId}`, data);
};

// Удаление ученика из класса
export const fetchDeleteStudent = (data: IAccountTeacher.IAddStudentPayload): Promise<AxiosResponse<IAccountTeacher.IAddStudentResponse>> => {
    const { classId, studentId } = data;
    return agent.DELETE(`/account/teacher/delete-student/${classId}`, { id: studentId });
};

// Изменение своего ФИО
export const fetchDeleteTeacherClass = (data: IAccountTeacher.IDeleteTeacherClassPayload): Promise<AxiosResponse<IAccountTeacher.IDeleteTeacherClassResponse>> => {
    return agent.DELETE(`/account/teacher/class/${data.classId}`);
};
