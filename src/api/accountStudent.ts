import agent from './agent';
import { AxiosResponse } from 'axios';
import * as IAccountStudent from '@redux/accountStudent/interfaces';

// Получение списка учителей
export const fetchStudentTeachers = (): Promise<AxiosResponse<IAccountStudent.ITeachersListResponse>> => {
    return agent.GET('/account/student/teachers');
};

// Изменение разрешения на доступ к данным ученика
export const fetchStudentSetAccessResolution = (data: IAccountStudent.ISetAccessResolutionPayload): Promise<AxiosResponse<IAccountStudent.ISetAccessResolutionResponse>> => {
    // const { teacherId, accessResolution } = data;
    return agent.PUT('/account/student/set-access-resolution', data);
};

// Список тренингов
export const fetchStudentTrainings = (data: IAccountStudent.IStudentTrainingsPayload): Promise<AxiosResponse<IAccountStudent.IStudentTrainingsResponse>> => {
    return agent.GET('/account/student/trainings', data);
};

// Статистика ученика
export const fetchStat = (): Promise<AxiosResponse<IAccountStudent.IStatResponse>> => {
    return agent.GET('/account/student/stat');
};
