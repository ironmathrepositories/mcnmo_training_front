import { AxiosResponse } from 'axios';
import * as IAccount from '@redux/account/interfaces';
import agent from '@/api/agent';

// Получение данных по своему аккаунту
export const fetchAccount = (): Promise<AxiosResponse<IAccount.IAccountResponse>> => {
    return agent.GET('/account');
};

// Изменение своего ФИО
export const fetchAccountSetFio = (data: IAccount.IFetchAccountSetFioPayload): Promise<AxiosResponse<IAccount.IAccountSetFioResponse>> => {
    return agent.PUT('/account/set-fio', data);
};

