import agent from './agent';
import { AxiosResponse } from 'axios';
import * as IAuth from '@redux/auth/interfaces';

// Отправляем запрос на регистрацию
export const fetchSingUp = (data: IAuth.ISingUpPayload): Promise<AxiosResponse<IAuth.ISingUpResponse>> => {
    return agent.POST('/auth/register', data);
};

// Отправляем запрос на авторизацию
export const fetchSingIn = (data: IAuth.ISingInPayload): Promise<AxiosResponse<IAuth.ISingInResponse>> => {
    return agent.POST('/auth/login', data);
};

// Обновление токена доступа
export const fetchRefresh = (): Promise<AxiosResponse<IAuth.IRefreshResponse>> => {
    return agent.GET('/auth/refresh');
};

// Код подтверждения
export const fetchConfirm = (data: IAuth.IConfirmPayload): Promise<AxiosResponse<IAuth.IConfirmResponse>> => {
    return agent.PUT('/auth/confirm', data);
};

// Повторная высылка кода подтверждения
export const fetchResendConfirm = (): Promise<AxiosResponse<IAuth.IConfirmResponse>> => {
    return agent.POST('/auth/resend-confirmation');
};

// Запрос ссылки на восстановление пароля
export const fetchRestoration = (data: IAuth.IRestorationPayload): Promise<AxiosResponse<IAuth.IConfirmResponse>> => {
    return agent.POST('/auth/send-restoration', data);
};

// Восстановление пароля
export const fetchRestore = (payload: IAuth.IRestorePayload): Promise<AxiosResponse<IAuth.IConfirmResponse>> => {
    const data = { ...payload };
    const { singleToken } = data;

    const headers = singleToken ? {
        Authorization: `Bearer ${singleToken}`
    }: undefined;
    delete data.singleToken;

    return agent.PUT('/auth/restore', data, headers);
};

// Выход
export const fetchLogout = (): Promise<AxiosResponse<IAuth.ILogoutResponse>> => {
    return agent.PUT('/auth/logout');
};
