/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosResponse } from 'axios';

import axios from './axios';

const timeout = 30000;

export default {
    POST<T = { [key: string]: any }, R = any>(path: string, data?: T, params?: Record<string, string>, headers?: Record<string, string>): Promise<AxiosResponse<R>> {
        return axios.post(`${path}`, data, { timeout, params, headers }) as Promise<AxiosResponse<R>>;
    },

    PUT<T = { [key: string]: any }, R = any>(path: string, data?: T, headers?: Record<string, string>): Promise<AxiosResponse<R>> {
        return axios.put(`${path}`, data, { timeout, headers }) as Promise<AxiosResponse<R>>;
    },

    PATCH<T = { [key: string]: any }, R = any>(path: string, data?: T): Promise<AxiosResponse<R>> {
        return axios.patch(`${path}`, data, { timeout }) as Promise<AxiosResponse<R>>;
    },

    GET<T = { [key: string]: any }, R = any>(path: string, params?: T): Promise<AxiosResponse<R>> {
        return axios.get(`${path}`, {
            params,
            timeout
        }) as Promise<AxiosResponse<R>>;
    },

    DELETE<T = { [key: string]: any }, R = any>(path: string, params?: T): Promise<AxiosResponse<R>> {
        return axios({
            method: 'DELETE',
            url: path,
            data: params,
            timeout
        }) as Promise<AxiosResponse<R>>;
        // return axios.delete(`${path}`, {
        //     params,
        //     timeout
        // }) as Promise<AxiosResponse<R>>;
    }
};
