import axios from 'axios';
import { history } from '@history';

const HTTP = axios.create({
    baseURL: '/api/v1',
    withCredentials: true
});

HTTP.interceptors.request.use(
    (config) => {
        const { accessToken, refreshToken } = localStorage;

        if (!accessToken && !refreshToken) {
            return config;
        }

        const token = config.url === '/auth/refresh' ? refreshToken : accessToken;

        return {
            ...config,
            headers: {
                Authorization: `Bearer ${token}`,
                common: {
                    ...config.headers.common,
                    'Content-Type': 'application/json; charset=utf-8'
                },
                post: {
                    ...config.headers.post,
                    'Content-Type': 'application/json; charset=utf-8'
                }
            }
        };
    },
    (error) => Promise.reject(error)
);

HTTP.interceptors.response.use(
    (response) => response,
    (error) => {
        const status = error.response ? error.response.status : 408;
        // if (status === 401) {
        //     history.push('/login');
        // }
        return Promise.reject(error);
    }
);

export default HTTP;
