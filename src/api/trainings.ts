import agent from './agent';
import { AxiosResponse } from 'axios';
import * as ITraining from '@redux/training/interfaces';
import * as IAccountTeacher from '@redux/accountTeacher/interfaces';

// Метод возвращает список направлений тренингов, отображаемых на главной странице системы сразу после входа.
export const fetchTrainingsDirections = (): Promise<AxiosResponse<ITraining.ITrainingDirectionResponse>> => {
    return agent.GET('/trainings/directions');
};

// Метод возвращает список коллекций тренингов
export const fetchTrainingsCollections = (id: string): Promise<AxiosResponse<ITraining.ITrainingCollectionsResponse>> => {
    return agent.GET(`/trainings/${id}/collections`);
};

// Метод возвращает коллекцию тренингов
export const fetchTrainingsCollection = (id: string): Promise<AxiosResponse<ITraining.ITrainingCollectionResponse>> => {
    return agent.GET(`/trainings/collection/${id}`);
};

// Получение задачи тренинга
export const fetchTrainingTask = (payload: ITraining.ITrainingTaskPayload): Promise<AxiosResponse<ITraining.ITrainingTaskResponse>> => {
    return agent.GET(`/trainings/${payload.trainingId}/task`, { forceGenerate: payload.forceGenerate });
};

// Список рекомендованных классов
export const fetchTrainingsRecommendedClasses = (trainingId: string): Promise<AxiosResponse<ITraining.ITrainingsRecommendedClassesResponse>> => {
    return agent.GET(`/trainings/${trainingId}>/recommended-classes`);
};

// Рекомендация тренинга учителем
export const fetchTrainingsRecommend = (payload: IAccountTeacher.ITrainingRecommendPayload): Promise<AxiosResponse<IAccountTeacher.ITrainingRecommendResponse>> => {
    const { trainingId, data } = payload;
    return agent.PUT(`/trainings/${trainingId}/recommend`, data);
};

// Проверка ответа
export const fetchCheckAnswerTraining = (payload: ITraining.ICheckAnswerTrainingPayload): Promise<AxiosResponse<ITraining.ICheckAnswerTrainingResponse>> => {
    const { trainingId, data } = payload;
    return agent.PUT(`/trainings/${trainingId}/check`, data);
};

export const fetchCheckAnswer = (payload: ITraining.ICheckAnswerPayload): Promise<AxiosResponse<ITraining.ICheckAnswerResponse>> => {
    const { data } = payload;
    return agent.PUT('/trainings/check', data);
};

// Уведомление об открытии подсказки/решения
export const fetchPessimize = (payload: ITraining.IPessimizePayload): Promise<AxiosResponse<ITraining.IPessimizeResponse>> => {
    const { trainingId } = payload;
    return agent.POST(`/trainings/${trainingId}/pessimize`);
};

// Варианты ОГЭ
export const fetchVariants = (payload: ITraining.IVariantsPayload): Promise<AxiosResponse<ITraining.IVariantsResponse>> => {
    return agent.GET('/trainings/variants', { type: payload });
};
