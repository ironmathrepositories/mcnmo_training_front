import { createSlice } from '@reduxjs/toolkit';
import * as I from './interfaces';

interface IState {
    directionList?: I.ITrainingDirectionValues,
    collectionList?: I.ITrainingsCollectionValues,
    collection?: I.ITrainingsCollection,
    task?: I.ITrainingTask,
    answer?: I.ICheckAnswerData,
    variants?: I.IVariants
}

const initialState: IState = {};

const slice = createSlice({
    name: 'training',
    initialState,
    reducers: {
        setDirections(state, action) {
            state.directionList = action.payload;
        },
        setCollections(state, action) {
            state.collectionList = action.payload;
        },
        clearCollections(state) {
            delete state.collectionList;
        },
        setCollection(state, action) {
            state.collection = action.payload;
        },
        clearCollection(state) {
            delete state.collection;
        },
        setTask(state, action) {
            state.task = {
                id: Math.random().toString(),
                ...action.payload
            };
        },
        clearTask(state) {
            delete state.task
        },
        updateScoreInTraining(state, action) {
            const { trainingId, score } = action.payload;
            if (!state.collection) {
                return state;
            }
            state.collection.trainings = state.collection.trainings.map(training => {
                if (String(training.id) === String(trainingId)) {
                    training.score = score;
                }

                return training;
            })
        },
        setAnswer(state, action) {
            state.answer = action.payload;
        },
        setVariants(state, action) {
            state.variants = action.payload
        }
    }
});

export const {
    setDirections,
    setCollections,
    clearCollections,
    setCollection,
    clearCollection,
    setTask,
    clearTask,
    updateScoreInTraining,
    setAnswer,
    setVariants
} = slice.actions;

export default slice.reducer;
