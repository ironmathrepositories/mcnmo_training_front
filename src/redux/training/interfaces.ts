import { Codes } from '@/constants';
import { IErrors, IMessages } from '@/interfaces';
import { ErrorResponse } from '@/types/error_response';
import { IResponse } from '@/interfaces/response';
import { Action } from '@/types/action';

export enum TrainingDirectionTypes {
    GOS_EXAM = 'GosExam',
    SCHOOL = 'School'
}
export interface ITrainingDirection {
    id: string;
    type: TrainingDirectionTypes;
    name: string;
    icon?: string;
    style: {
        backgroundColor: string;
        color?: string;
    }
}
export type ITrainingDirectionValues = Array<ITrainingDirection>;
export type ITrainingDirectionAction = Action<unknown>;
export type ITrainingDirectionResponse = IResponse<ITrainingDirectionValues>;


// список коллекций тренингов
export interface ITrainingBlock {
    id: string;
    name: string;
    icon: string;
    recommendedBy?: Array<string>; // Список учителей рекомендовавших этот тренинг
    score?: number | null;
}
export interface ITrainingsCollection {
    id: string;
    examDirectionId: string;
    name: string;
    trainings: Array<ITrainingBlock>
}
export type ITrainingsCollectionValues = Array<ITrainingsCollection>;
export type ITrainingsCollectionsAction = Action<unknown>;
export type ITrainingCollectionsResponse = IResponse<ITrainingsCollectionValues>;


// Колекция тренинга
export interface ITrainingsCollectionPayload {
    id: string
}
export type ITrainingsCollectionAction = Action<ITrainingsCollectionPayload>;
export type ITrainingCollectionResponse = IResponse<ITrainingsCollection>;


// Получение задачи тренинга
export interface IWebPreview {
    tag: string;
    elem?: string | string[] | IWebPreview | IWebPreview[];
    options?: Record<string, unknown>;
}
export interface ITrainingTask {
    task: IWebPreview;
    solution?: IWebPreview;
    hint?: IWebPreview;
    score?: number | null;
    id: string;
}
export interface ITrainingTaskPayload {
    trainingId: string;
    forceGenerate?: boolean;
}
export type ITrainingTaskAction = Action<ITrainingTaskPayload>;
export type ITrainingTaskResponse = IResponse<ITrainingTask>;


// Список рекомендованных классов
export type ITrainingsRecommendedClassesAction = Action<unknown>;
export type ITrainingsRecommendedClassesResponse = IResponse<Array<string>>;


// Рекомендация тренинга учителем
export interface ITrainingsRecommendPayload {
    [classId: string]: boolean;
}
export type ITrainingsRecommendAction = Action<ITrainingsRecommendPayload>;
export type ITrainingsRecommendResponse = unknown | ErrorResponse;


// Проверка ответа
export interface ICheckAnswerRequest {
    actionId: string;
    data: {
        [inputId: string]: IAnswer;
    }
}

export type IAnswer = boolean | string | string[] | IMatchingAnswer;
export type IMatchingAnswer = Array<[ string | string[], string | string[] ]>;

export interface ICheckAnswerData {
    actionId: string;
    score?: number;
    checkResult: {
        [inputId: string]: boolean;
    },
    replace?: {
        [elementId: string]: IWebPreview;
    }
}

export interface ICheckAnswerPayload {
    data: ICheckAnswerRequest;
    variantId: string;
}
export type ICheckAnswerAction = Action<ICheckAnswerPayload>;
export type ICheckAnswerResponse = IResponse<ICheckAnswerData>;

// ----------------
export interface ICheckAnswerTrainingPayload extends ISimpleCheckAnswer {
    trainingId: string;
}

export interface ISimpleCheckAnswer {
    data: ICheckAnswerRequest
}

export type ICheckAnswerTrainingAction = Action<ICheckAnswerTrainingPayload>;
export type ICheckAnswerTrainingResponse = IResponse<ICheckAnswerData>;

// export interface ICheckAnswerTrainingPayload {
//     data: ICheckAnswerRequest;
//     trainingId: string;
// }
//
// export type ICheckAnswerAction = Action<ICheckAnswerTrainingPayload>;
// export type ICheckAnswerResponse = IResponse<ICheckAnswerData>;


// Уведомление об открытии подсказки/решения
export interface IPessimizePayload {
    trainingId: string;
}

export type IPessimizeAction = Action<IPessimizePayload>;
export type IPessimizeResponse = IResponse<unknown>;

// Варианты ОГЭ
export interface IOGEVariant {
    id: string;
    title: string;
    tasksCount: number;
    tasks: IWebPreview
}

export type IVariants = Array<IOGEVariant>;

export type IVariantsPayload = string;
export type IVariantsAction = Action<IVariantsPayload>;
export type IVariantsResponse = IResponse<IVariants>;
