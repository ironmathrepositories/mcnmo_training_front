import { AxiosError, AxiosResponse } from 'axios';
import { put } from 'redux-saga/effects';

import * as actions from '@redux/actions';
import { ErrorResponse } from '@/types/error_response';
import { toastError } from '@utils/toast_error';
import { runRequestProcess } from '@utils/run_request_process';
import * as I from './interfaces';
import * as trainingActions from './slice';
import { IResponseSuccess } from '@/interfaces/response';

const trainingsDirections = function* (data: AxiosResponse<I.ITrainingDirectionResponse>, error: AxiosError<ErrorResponse>) {
    if (data && data.status === 200) {
        yield put(trainingActions.setDirections((data.data as IResponseSuccess<I.ITrainingDirectionValues>).payload));
    }
    if (error) {
        toastError(error, 'Ошибка загрузки списка направлений тренингов');
    }

    return { data, error };
};

const trainingsCollections = function* (data: AxiosResponse<I.ITrainingCollectionsResponse>, error: AxiosError<ErrorResponse>) {
    if (data && data.status === 200) {
        yield put(trainingActions.setCollections((data.data as IResponseSuccess<I.ITrainingsCollectionValues>).payload));
    }
    if (error) {
        toastError(error, 'Ошибка загрузки списка коллекций тренингов');
    }

    return { data, error };
};

const trainingsCollection = function* (data: AxiosResponse<I.ITrainingCollectionResponse>, error: AxiosError<ErrorResponse>) {
    if (data && data.status === 200) {
        yield put(trainingActions.setCollection((data.data as IResponseSuccess<I.ITrainingsCollection>).payload));
    }
    if (error) {
        toastError(error, 'Ошибка загрузки коллекции тренинга');
    }
    return { data, error };
};

const trainingTask = function* (data: AxiosResponse<I.ITrainingTaskResponse>, error: AxiosError<ErrorResponse>, action: I.ITrainingTaskAction) {
    if (data && data.status === 200) {
        const { trainingId } = action.payload;
        const task = (data.data as IResponseSuccess<I.ITrainingTask>).payload;

        yield put(trainingActions.setTask(task));
        yield put(trainingActions.updateScoreInTraining({
            trainingId,
            score: task.score
        }));
    }
    if (error) {
        toastError(error, 'Ошибка загрузки задачи');
    }
    return { data, error };
};

const checkAnswerTraining = function* (data: AxiosResponse<I.ICheckAnswerTrainingResponse>, error: AxiosError<ErrorResponse>, action: I.ICheckAnswerTrainingAction) {
    if (data && data.status === 200) {
        const { trainingId } = action.payload;
        const answer = (data.data as IResponseSuccess<I.ICheckAnswerData>).payload;

        yield put(trainingActions.setAnswer(answer));
        yield put(trainingActions.updateScoreInTraining({
            trainingId,
            score: answer.score
        }));
    }

    return { data, error };
};

// const checkAnswer = function* (data: AxiosResponse<I.ICheckAnswerResponse>, error: AxiosError<ErrorResponse>, action: I.ICheckAnswerAction) {
//     if (data && data.status === 200) {
//         console.log({ data });
//         // const { trainingId } = action.payload;
//         // const answer = (data.data as IResponseSuccess<I.ICheckAnswerData>).payload;
//         //
//         // yield put(trainingActions.setAnswer(answer));
//         // yield put(trainingActions.updateScoreInTraining({
//         //     trainingId,
//         //     score: answer.score
//         // }));
//     }
//
//     return { data, error };
// };

const variantsResponse = function* (data: AxiosResponse<I.IVariantsResponse>, error: AxiosError<ErrorResponse>) {
    if (data && data.status === 200) {
        const variants = (data.data as IResponseSuccess<I.IVariants>).payload;
        // const variants = variantsP.payload;
        yield put(trainingActions.setVariants(variants));
    }

    return { data, error };
};

export const watchTrainingsDirections = runRequestProcess<I.ITrainingDirectionResponse, unknown>(actions.fetchTrainingsDirections.type, trainingsDirections);
export const watchTrainingsCollections = runRequestProcess<I.ITrainingCollectionsResponse, unknown>(actions.fetchTrainingsCollections.type, trainingsCollections);
export const watchTrainingsCollection = runRequestProcess<I.ITrainingCollectionResponse, I.ITrainingsCollectionPayload>(actions.fetchTrainingsCollection.type, trainingsCollection);
export const watchTrainingTask = runRequestProcess<I.ITrainingTaskResponse, I.ITrainingTaskPayload>(actions.fetchTrainingTask.type, trainingTask);
export const watchCheckAnswerTraining = runRequestProcess<I.ICheckAnswerTrainingResponse, I.ICheckAnswerTrainingPayload>(actions.fetchCheckAnswerTraining.type, checkAnswerTraining);
export const watchCheckAnswer = runRequestProcess<I.ICheckAnswerResponse, I.ICheckAnswerPayload>(actions.fetchCheckAnswer.type);
export const watchPessimize = runRequestProcess<I.IPessimizeResponse, I.IPessimizePayload>(actions.fetchPessimize.type);
export const watchVariants = runRequestProcess<I.IVariantsResponse, I.IVariantsPayload>(actions.fetchVariants.type, variantsResponse);

const init = function* () {
    yield put(actions.fetchTrainingsDirections());
};

export default [
    init,
    watchTrainingsDirections,
    watchTrainingsCollections,
    watchTrainingsCollection,
    watchTrainingTask,
    watchCheckAnswerTraining,
    watchCheckAnswer,
    watchPessimize,
    watchVariants
];
