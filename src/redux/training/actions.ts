import { createAction } from '@reduxjs/toolkit';
import * as I from './interfaces';

// TrainingsDirections
export const fetchTrainingsDirections = createAction('fetchTrainingsDirections');
export const fetchTrainingsDirectionsSuccess = createAction('fetchTrainingsDirectionsSuccess');
export const fetchTrainingsDirectionsFailure = createAction('fetchTrainingsDirectionsFailure');

// TrainingsCollections
export const fetchTrainingsCollections = createAction('fetchTrainingsCollections', (payload: string) => ({ payload }));
export const fetchTrainingsCollectionsSuccess = createAction('fetchTrainingsCollectionsSuccess');
export const fetchTrainingsCollectionsFailure = createAction('fetchTrainingsCollectionsFailure');

// TrainingsCollection
export const fetchTrainingsCollection = createAction('fetchTrainingsCollection', (payload: string) => ({ payload }));
export const fetchTrainingsCollectionSuccess = createAction('fetchTrainingsCollectionSuccess');
export const fetchTrainingsCollectionFailure = createAction('fetchTrainingsCollectionFailure');

// TrainingTask
export const fetchTrainingTask = createAction('fetchTrainingTask', (payload: I.ITrainingTaskPayload) => ({ payload }));
export const fetchTrainingTaskSuccess = createAction('fetchTrainingTaskSuccess');
export const fetchTrainingTaskFailure = createAction('fetchTrainingTaskFailure');

// fetchCheckAnswer
export const fetchCheckAnswer = createAction('fetchCheckAnswer', (payload: I.ISimpleCheckAnswer, meta: any) => ({ payload, meta }));
export const fetchCheckAnswerSuccess = createAction('fetchCheckAnswerSuccess');
export const fetchCheckAnswerFailure = createAction('fetchCheckAnswerFailure');

// fetchCheckAnswerTraining
export const fetchCheckAnswerTraining = createAction('fetchCheckAnswerTraining', (payload: I.ICheckAnswerTrainingPayload, meta: any) => ({ payload, meta }));
export const fetchCheckAnswerTrainingSuccess = createAction('fetchCheckAnswerTrainingSuccess');
export const fetchCheckAnswerTrainingFailure = createAction('fetchCheckAnswerTrainingFailure');

// fetchPessimize
export const fetchPessimize = createAction('fetchPessimize', (payload: I.IPessimizePayload) => ({ payload }));
export const fetchPessimizeSuccess = createAction('fetchPessimizeSuccess');
export const fetchPessimizeFailure = createAction('fetchPessimizeFailure');

// Варианты ОГЭ
export const fetchVariants = createAction('fetchVariants', (payload: I.IVariantsPayload) => ({ payload }));
export const fetchVariantsSuccess = createAction('fetchVariantsSuccess');
export const fetchVariantsFailure = createAction('fetchVariantsFailure');
