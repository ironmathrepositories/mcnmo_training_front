import { all, fork } from 'redux-saga/effects'
// import { watchFetchSingUp } from '@redux/auth/sagas';
import accountSagas from '@redux/account/sagas';
import accountStudentSagas from '@redux/accountStudent/sagas';
import accountTeacherSagas from '@redux/accountTeacher/sagas';
import authSagas from '@redux/auth/sagas';
import trainingSagas from '@redux/training/sagas';
// import { watchFetchLogin } from '@redux/login/sagas';

const registeredSagas = [
    ...accountSagas,
    ...accountStudentSagas,
    ...accountTeacherSagas,
    ...authSagas,
    ...trainingSagas
];

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function* rootSata () {
    const forkedSagas = registeredSagas.map((saga) => fork(saga));
    yield all(forkedSagas);
}
