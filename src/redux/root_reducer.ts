import { combineReducers } from '@reduxjs/toolkit';
import account from '@redux/account/slice';
import accountStudent from '@redux/accountStudent/slice';
import accountTeacher from '@redux/accountTeacher/slice';
import auth from '@redux/auth/slice';
import modal from '@redux/modal/reducer';
import training from '@redux/training/slice';

export default combineReducers({
    account,
    accountStudent,
    accountTeacher,
    auth,
    modal,
    training
});
