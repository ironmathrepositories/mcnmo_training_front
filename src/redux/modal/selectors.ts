import { RootState } from '@redux/store';
import { IModalState } from '@/types/modal';
import { createSelector } from '@reduxjs/toolkit';

const modalState = (state: RootState): IModalState => state.modal;

export const getModalState = createSelector(modalState, (modal) => modal);
