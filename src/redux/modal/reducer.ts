import { createReducer } from '@reduxjs/toolkit';
import { Action } from '@/types/action';
import { openModal, closeModal } from '@redux/actions';
import { IModalState } from '@/types/modal';

const modalInitialState: IModalState = {
    isOpen: false,
    modalName: '',
    title: ''
};

export default createReducer(modalInitialState, {
    [openModal.type]: (state, action: Action<IModalState>) => {
        const { modalName, title } = action.payload;
        state.isOpen = true;
        state.modalName = modalName;
        state.title = title;
    },
    [closeModal.type]: (state) => {
        state.isOpen = false;
        state.modalName = '';
        state.title = '';
    },
});
