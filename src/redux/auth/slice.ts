import { createSlice } from '@reduxjs/toolkit';
import { Dialog } from './constants';
import { ITokenPayload } from '@/types/login';

interface refreshAction {
    action: any;
    type: string;
}

interface IState {
    token?: {
        access: string;
        refresh: string;
        dataAccess: ITokenPayload
    },
    errors?: Array<string>,
    refreshActions?: Array<refreshAction>
    authDialog: Dialog,
    resendDateTime?: number,
    isRestorationPassword?: boolean,
    isRestorePassword?: boolean
}

const initialState: IState = {
    authDialog: Dialog.Close
};

const slice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setToken(state, action) {
            const { access, refresh, dataAccess } = action.payload;
            state.token = {
                access,
                refresh,
                dataAccess
            };
        },
        clearToken(state) {
            delete localStorage.accessToken;
            delete localStorage.refreshToken;
            delete state.token;
        },
        singDialog(state, action) {
            const dialog: Dialog = action.payload;
            state.authDialog = dialog;
        },
        setErrors(state, action) {
            const errors: Array<string> = action.payload;
            state.errors = errors
        },
        clearErrors(state) {
            delete state.errors;
        },
        addRefreshAction(state, action) {
            if (!state.refreshActions) {
                state.refreshActions = [];
            }

            const { payload } = action;
            state.refreshActions.push({
                action: payload.action,
                type: payload.type
            });
        },
        clearRefreshActions(state) {
            delete state.refreshActions;
        },
        setResendDateTime(state) {
            state.resendDateTime = new Date().getTime();
            localStorage.resendDateTime = state.resendDateTime;
        },
        recoverResendDateTime(state) {
            if (localStorage.resendDateTime) {
                state.resendDateTime = localStorage.resendDateTime;
            }
        },
        successRestorationPassword(state) {
            state.isRestorationPassword = true;
        },
        clearRestorationPasswordSuccess(state) {
            delete state.isRestorationPassword;
        },
        successRestorePassword(state) {
            state.isRestorePassword = true;
        },
        clearRestorePasswordSuccess(state) {
            delete state.isRestorePassword;
        }
    }
});

export const {
    setToken,
    clearToken,
    singDialog,
    setErrors,
    clearErrors,
    addRefreshAction,
    clearRefreshActions,
    setResendDateTime,
    recoverResendDateTime,
    successRestorationPassword,
    clearRestorationPasswordSuccess,
    successRestorePassword,
    clearRestorePasswordSuccess
} = slice.actions;
export default slice.reducer;
