import { put, select, takeLatest } from 'redux-saga/effects';
import { AxiosError, AxiosResponse } from 'axios';

import { runRequestProcess } from '@utils/run_request_process';
import * as actions from '@redux/actions';
import { ITokenPayload } from '@/types/login';
import { ErrorResponse } from '@/types/error_response';
import jwt_decode from 'jwt-decode';
import { toastError } from '@utils/toast_error';
import * as I from './interfaces';
import * as authSliceActions from '@redux/auth/slice';
import * as accountStudentActions from '@redux/accountStudent/slice';
import { Dialog } from '@redux/auth/constants';
import { RootState } from '@store';
import { history } from '@redux/history';
import { toast } from 'react-toastify';
import { IResponseSuccess } from '@/interfaces/response';
import { UserTypes } from '@/interfaces/login';

function* updateToken(payload: I.ISingUpPayloadSuccess) {
    const access = payload.access;
    const refresh = payload.refresh;

    // Cookies.set('JWT-Cookie', access);
    // Cookies.set('refresh', refresh);
    localStorage.accessToken = access;
    localStorage.refreshToken = refresh;
    const dataAccess: ITokenPayload = jwt_decode(access);

    yield put(authSliceActions.singDialog(Dialog.Close));
    yield put(authSliceActions.setToken({
        access,
        refresh,
        dataAccess
    }));

    if (dataAccess.user.type === UserTypes.STUDENT) {
        yield put(actions.fetchStat());
    }
}

// Регистрация
const singUpSuccess = function* (data: AxiosResponse<I.ISingUpResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 201)) {
        const payload = (data.data as IResponseSuccess<I.ISingUpPayloadSuccess>).payload;
        // const displayMessages = data.data.displayMessages as IMessages;

        yield updateToken(payload);
    }
    if (error) {
        toastError(error, 'Ошибка регистрации.');
    }

    return { data, error };
};

// Авторизация
const singInSuccess = function* (data: AxiosResponse<I.ISingInResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        const payload = (data.data as IResponseSuccess<I.ISingInPayloadSuccess>).payload;

        yield updateToken(payload);
    }
    if (error) {
        toastError(error, 'Ошибка аутентификации.');
    }

    return { data, error };
};

// Обновление токена авторизации
const refreshSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const payload = (data.data as IResponseSuccess<I.ISingInPayloadSuccess>).payload;

        yield updateToken(payload);

        const refreshActions = yield select((state: RootState) => state.auth.refreshActions);
        if (refreshActions) {
            for (const item of refreshActions) {
                yield put(actions[item.type](item.action.payload, item.action.meta));
            }
        }
    }

    // В случае, если токен обновления "протух"
    if (error) {
        if (error.response && error.response.status === 401) {
            yield put(authSliceActions.clearToken());
        } else {
            toastError(error, 'Что-то пошло не так.');
        }
    }

    yield put(authSliceActions.clearRefreshActions());

    return { data, error };
};

// Код подтверждения
const confirmSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        toast.success('Подтверждение получено');
        history.push('/');
    }

    return { data, error };
};

// Повторный код подтверждения
const confirmResendSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(authSliceActions.setResendDateTime());
    }

    return { data, error };
};

// Запрос ссылки на восстановление пароля
const restorationSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(authSliceActions.successRestorationPassword());
    }

    return { data, error };
};

// Восстановление пароля
const restoreSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(authSliceActions.successRestorePassword());
    }

    return { data, error };
};

// Выход
const logoutSuccess = function* (data: AxiosResponse<I.IRefreshResponse>, error: AxiosError<ErrorResponse>) {
    yield put(authSliceActions.clearToken());
    yield put(accountStudentActions.clearStatistic());

    return { data, error };
};


const callbackFailure = function*(action) {
    try {
        const errors = action.payload.response.data?.displayMessages;
        const errorsText = errors.map(error => error.message);
        yield put(authSliceActions.setErrors(errorsText));
    } catch (e) {}
};

// Инициализируем redux, записываем туда токен
const initToken = function* () {
    // const access = Cookies.get('JWT-Cookie');
    // const refresh = Cookies.get('refresh');
    const access = localStorage.accessToken;
    const refresh = localStorage.refreshToken;

    if (access) {
        try {
            const dataAccess: ITokenPayload = jwt_decode(access);

            yield put(authSliceActions.setToken({
                access,
                refresh,
                dataAccess
            }));

            if (dataAccess.user.type === UserTypes.STUDENT) {
                yield put(actions.fetchStat());
            }
        } catch (e) {
            // некорректный токен
        }
    }
};

const init = function* () {
    yield initToken();
    yield put(authSliceActions.recoverResendDateTime());

    yield takeLatest(`${actions.fetchSingUpFailure.type}`, callbackFailure);
    yield takeLatest(`${actions.fetchSingInFailure.type}`, callbackFailure);
};


export const watchSingUp = runRequestProcess<I.ISingUpResponse, I.ISingUpPayload>(actions.fetchSingUp.type, singUpSuccess);
export const watchSingIn = runRequestProcess<I.ISingInResponse, I.ISingInPayload>(actions.fetchSingIn.type, singInSuccess);
export const watchRefresh = runRequestProcess<I.IRefreshResponse, unknown>(actions.fetchRefresh.type, refreshSuccess);
export const watchConfirm = runRequestProcess<I.IConfirmResponse, I.IConfirmPayload>(actions.fetchConfirm.type, confirmSuccess);
export const watchResendConfirm = runRequestProcess<I.IConfirmResendResponse, I.IConfirmResendPayload>(actions.fetchResendConfirm.type, confirmResendSuccess);
export const watchRestoration = runRequestProcess<I.IRestorationResponse, I.IRestorationPayload>(actions.fetchRestoration.type, restorationSuccess);
export const watchRestore = runRequestProcess<I.IRestoreResponse, I.IRestorePayload>(actions.fetchRestore.type, restoreSuccess);
export const watchLogout = runRequestProcess<I.ILogoutResponse, unknown>(actions.fetchLogout.type, logoutSuccess);

export default [
    watchSingUp,
    watchSingIn,
    watchRefresh,
    watchConfirm,
    watchResendConfirm,
    watchRestoration,
    watchRestore,
    watchLogout,
    init
];
