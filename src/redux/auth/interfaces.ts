import { IErrors, IMessages } from '@/interfaces';
import { UserTypes } from '@/interfaces/login';
import { ErrorResponse } from '@/types/error_response';
import { IResponse } from '@/interfaces/response';
import { Action } from '@/types/action';

export interface IUser {
    id: string;
    name?: string;
    surname?: string;
    patronymic?: string;
    email: string;
    comment?: string;
    type: UserTypes;
    class?: number;
}

// SingUp
export interface ISingUpPayload extends IUser {
    password: string;
    agreeRules: boolean;
    confirmTeacher: boolean;
    agreePersonalData: boolean;
}

export type IFetchSingUpAction = Action<ISingUpPayload>;

export interface ISingUpPayloadSuccess {
    access: string;
    refresh: string;
}

// export interface IResponse extends ErrorResponse {
//     payload: ISingUpPayloadSuccess | IErrors
// }

export type ISingUpResponse = IResponse<ISingUpPayloadSuccess>;

// SingIn
export interface ISingInPayload extends IUser {
    email: string;
    password: string;
}
export type IFetchSingInAction = Action<ISingInPayload>;
export type ISingInPayloadSuccess = ISingUpPayloadSuccess;
export type ISingInResponse = ISingUpResponse;

// Refresh
export type IFetchRefreshAction = Action<unknown>;
export type IRefreshResponse = ISingUpResponse;

// Confirm
export interface IConfirmPayload {
    code: string;
}
export type IFetchConfirmAction = Action<IConfirmPayload>;
export type IConfirmResponse = ISingUpResponse | undefined;

// Confirm Resend
export type IConfirmResendPayload = unknown;
export type IFetchConfirmResendAction = Action<IConfirmResendPayload>;
export type IConfirmResendResponse = ErrorResponse | undefined;

// Restoration
export interface IRestorationPayload {
    email: string;
}
export type IFetchRestorationAction = Action<IRestorationPayload>;
export type IRestorationResponse = ErrorResponse | undefined;

// Restore
export interface IRestorePayload {
    singleToken?: string;
    oldPassword?: string;
    newPassword: string;
}
export type IFetchRestoreAction = Action<IRestorePayload>;
export type IRestoreResponse = ErrorResponse | undefined;

// Logout
export type IFetchLogoutAction = Action<unknown>;
export type ILogoutResponse = ErrorResponse | undefined;
