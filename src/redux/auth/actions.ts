import { createAction } from '@reduxjs/toolkit';
import * as I from './interfaces';

// SingUp
export const fetchSingUp = createAction('fetchSingUp', (payload: I.ISingUpPayload, meta: any) => ({ payload, meta }));
export const fetchSingUpSuccess = createAction('fetchSingUpSuccess');
export const fetchSingUpFailure = createAction('fetchSingUpFailure');

// SingIn
export const fetchSingIn = createAction('fetchSingIn', (payload: I.ISingInPayload, meta: any) => ({ payload, meta }));
export const fetchSingInSuccess = createAction('fetchSingInSuccess');
export const fetchSingInFailure = createAction('fetchSingInFailure');

// Refresh
export const fetchRefresh = createAction('fetchRefresh');
export const fetchRefreshSuccess = createAction('fetchRefreshSuccess');
export const fetchRefreshFailure = createAction('fetchRefreshFailure');

// Confirm
export const fetchConfirm = createAction('fetchConfirm', (payload: I.IConfirmPayload, meta?: any) => ({ payload, meta }));
export const fetchConfirmSuccess = createAction('fetchConfirmSuccess');
export const fetchConfirmFailure = createAction('fetchConfirmFailure');

// Confirm Resend
export const fetchResendConfirm = createAction('fetchResendConfirm');
export const fetchResendConfirmSuccess = createAction('fetchResendConfirmSuccess');
export const fetchResendConfirmFailure = createAction('fetchResendConfirmFailure');

// Restoration
export const fetchRestoration = createAction('fetchRestoration', (payload: I.IRestorationPayload, meta: any) => ({ payload, meta }));
export const fetchRestorationSuccess = createAction('fetchRestorationSuccess');
export const fetchRestorationFailure = createAction('fetchRestorationFailure');

// Restore
export const fetchRestore = createAction('fetchRestore', (payload: I.IRestorePayload, meta: any) => ({ payload, meta }));
export const fetchRestoreSuccess = createAction('fetchRestoreSuccess');
export const fetchRestoreFailure = createAction('fetchRestoreFailure');

// Logout
export const fetchLogout = createAction('fetchLogout');
export const fetchLogoutSuccess = createAction('fetchLogouteSuccess');
export const fetchLogoutFailure = createAction('fetchLogoutFailure');
