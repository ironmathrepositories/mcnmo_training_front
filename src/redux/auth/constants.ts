export enum Dialog {
    Close = 0,
    SingIn = 1,
    SingUp = 2,
    Restoration = 3
}
