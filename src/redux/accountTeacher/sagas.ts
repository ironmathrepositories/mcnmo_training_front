import { takeLatest, put, select, all } from 'redux-saga/effects';
import { AxiosError, AxiosResponse } from 'axios';

import { RootState } from '@store';
import { runRequestProcess } from '@utils/run_request_process';
import { IResponseSuccess } from '@/interfaces/response';
import * as actions from '@redux/actions';
import { ErrorResponse } from '@/types/error_response';
import * as I from './interfaces';
import * as accountTeacherActions from './slice';

const classDataSuccess = function* (data: AxiosResponse<I.IClassDataResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const payload = (data.data as IResponseSuccess<I.IClassDataSuccess>).payload;
        yield put(accountTeacherActions.setClassData(payload));
    }

    return { data, error };
};

const classDataItemSuccess = function* (data: AxiosResponse<I.IClassDataItemResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        // yield put(accountTeacherActions.successRestorePassword());
    }

    return { data, error };
};

const addClassSuccess = function* (data: AxiosResponse<I.IAddClassResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        const payload = (data.data as IResponseSuccess<I.IClassData>).payload;
        yield put(accountTeacherActions.addClass(payload));
    }

    return { data, error };
};

const editClassSuccess = function* (data: AxiosResponse<I.IEditClassResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        const payload = (data.data as IResponseSuccess<I.IClassData>).payload;
        yield put(accountTeacherActions.editClass(payload));
    }

    return { data, error };
};

const scoresSuccess = function* (data: AxiosResponse<I.IScoresResponse>, error: AxiosError<ErrorResponse>, action: I.IScoresAction) {
    if (data && (data.status === 200)) {
        const classId = action.payload;
        const payload = (data.data as IResponseSuccess<I.IScoresRows>).payload;
        yield put(accountTeacherActions.setScores({ classId, scores: payload }));
    }

    return { data, error };
};

const recommendedClassesSuccess = function* (data: AxiosResponse<I.IRecommendedClassesResponse>, error: AxiosError<ErrorResponse>, action: I.IRecommendedClassesAction) {
    if (data && (data.status === 200)) {
        const payload = (data.data as IResponseSuccess<I.IRecommendedClassesData>).payload;
        yield put(accountTeacherActions.setRecommendedClass({ trainingId: action.payload, data: payload }));
    }

    return { data, error };
};

const addStudentSuccess = function* (data: AxiosResponse<I.IAddStudentResponse>, error: AxiosError<ErrorResponse>, action: I.IAddStudentAction) {
    if (data && (data.status === 200 || data.status === 204)) {
        const classId = action.payload.classId;
        const student = (data.data as IResponseSuccess<I.IStudentData>).payload;
        yield put(accountTeacherActions.addStudent({ classId, student }));
        yield put(actions.fetchScores(classId));
        yield put(actions.fetchClassData());
    }

    return { data, error };
};

const editStudentSuccess = function* (data: AxiosResponse<I.IEditStudentResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200 || data.status === 204)) {
        const student = (data.data as IResponseSuccess<I.IStudentData>).payload;
        yield put(accountTeacherActions.editStudent(student));
    }

    return { data, error };
};

const deleteStudentSuccess = function* (data: AxiosResponse<I.IEditStudentResponse>, error: AxiosError<ErrorResponse>, action: I.IDeleteStudentAction) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(accountTeacherActions.deleteStudent(action.payload));
    }

    return { data, error };
};

const setTrainingRecommended = function* (action) {
    const { trainingId } = action.payload;
    yield put(accountTeacherActions.setTrainingRecommended(action.payload));

    const recommended = yield select((state: RootState) => state.accountTeacher.recommended[trainingId]);
    yield put(actions.fetchTrainingsRecommend({ trainingId, data: recommended }));
};

const trainingsRecommend = function* (data: AxiosResponse<I.ITrainingRecommendResponse>, error: AxiosError<ErrorResponse>) {
    return { data, error };
};

const deleteRecommendClass = function* (data: AxiosResponse<I.ITrainingRecommendResponse>, error: AxiosError<ErrorResponse>, action: I.IDeleteRecommendClassAction) {
    if (data && (data.status === 200 || data.status === 204)) {
        const { classId } = action.payload;
        yield put(actions.fetchScores(classId));
    }

    return { data, error };
};


const deleteTeacherClassSuccess = function* (data: AxiosResponse<I.IDeleteTeacherClassResponse>, error: AxiosError<ErrorResponse>, action: I.IDeleteTeacherClassAction) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(accountTeacherActions.deleteClass(action.payload.classId));
    }

    return { data, error };
};

const init = function* () {
    yield takeLatest(`${actions.setTrainingRecommendedClass.type}`, setTrainingRecommended);
};


export const watchFetchClassData = runRequestProcess<I.IClassDataResponse, unknown>(actions.fetchClassData.type, classDataSuccess);
export const watchFetchClassDataItem = runRequestProcess<I.IClassDataItemResponse, I.IFetchClassDataItemPayload>(actions.fetchClassDataItem.type, classDataItemSuccess);
export const watchFetchAddClass = runRequestProcess<I.IAddClassResponse, I.IAddClassPayload>(actions.fetchAddClass.type, addClassSuccess);
export const watchFetchEditClass = runRequestProcess<I.IEditClassResponse, I.IEditClassPayload>(actions.fetchEditClass.type, editClassSuccess);
export const watchFetchScores = runRequestProcess<I.IScoresResponse, I.IScoresPayload>(actions.fetchScores.type, scoresSuccess);
export const watchFetchRecommendedClasses = runRequestProcess<I.IRecommendedClassesResponse, I.IRecommendedClassesPayload>(actions.fetchRecommendedClasses.type, recommendedClassesSuccess);
export const watchFetchTrainingsRecommend = runRequestProcess<I.ITrainingRecommendResponse, I.ITrainingRecommendPayload>(actions.fetchTrainingsRecommend.type, trainingsRecommend);
export const watchFetchDeleteRecommendClass = runRequestProcess<I.IDeleteRecommendClassResponse, I.ITrainingRecommendedClassPayload>(actions.fetchDeleteRecommendClass.type, deleteRecommendClass);
export const watchFetchAddStudent = runRequestProcess<I.IAddStudentResponse, I.IAddStudentPayload>(actions.fetchAddStudent.type, addStudentSuccess);
export const watchFetchEditStudent = runRequestProcess<I.IEditStudentResponse, I.IEditStudentPayload>(actions.fetchEditStudent.type, editStudentSuccess);
export const watchFetchDeleteStudent = runRequestProcess<I.IDeleteStudentResponse, I.IDeleteStudentPayload>(actions.fetchDeleteStudent.type, deleteStudentSuccess);
export const watchDeleteTeacherClass = runRequestProcess<I.IDeleteTeacherClassResponse, I.IDeleteTeacherClassPayload>(actions.fetchDeleteTeacherClass.type, deleteTeacherClassSuccess);

export default [
    watchFetchClassData,
    watchFetchClassDataItem,
    watchFetchAddClass,
    watchFetchEditClass,
    watchFetchScores,
    watchFetchRecommendedClasses,
    watchFetchTrainingsRecommend,
    watchFetchDeleteRecommendClass,
    watchFetchAddStudent,
    watchFetchEditStudent,
    watchFetchDeleteStudent,
    watchDeleteTeacherClass,
    init
];
