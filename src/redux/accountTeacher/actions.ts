import { createAction } from '@reduxjs/toolkit';
import * as I from './interfaces';

// Список классов учителя
export const fetchClassData = createAction('fetchClassData');
export const fetchClassDataSuccess = createAction('fetchClassDataSuccess');
export const fetchClassDataFailure = createAction('fetchClassDataFailure');

// Информация по классу
export const fetchClassDataItem = createAction('fetchClassDataItem', (payload: I.IFetchClassDataItemPayload) => ({ payload }));
export const fetchClassDataItemSuccess = createAction('fetchClassDataItemSuccess');
export const fetchClassDataItemFailure = createAction('fetchClassDataItemFailure');

// Информация по классу
export const fetchAddClass = createAction('fetchAddClass', (payload: I.IAddClassPayload, meta: any) => ({ payload, meta }));
export const fetchAddClassSuccess = createAction('fetchAddClassSuccess');
export const fetchAddClassFailure = createAction('fetchAddClassFailure');

// Редактирование класса
export const fetchEditClass = createAction('fetchEditClass', (payload: I.IEditClassPayload) => ({ payload }));
export const fetchEditClassSuccess = createAction('fetchEditClassSuccess');
export const fetchEditClassFailure = createAction('fetchEditClassFailure');

// Получение данных по оценкам учеников
export const fetchScores = createAction('fetchScores', (payload: I.IScoresPayload) => ({ payload }));
export const fetchScoresSuccess = createAction('fetchScoresSuccess');
export const fetchScoresFailure = createAction('fetchScoresFailure');

// Список рекомендованных классов
export const fetchRecommendedClasses = createAction('fetchRecommendedClasses', (payload: I.IRecommendedClassesPayload) => ({ payload }));
export const fetchRecommendedClassesSuccess = createAction('fetchRecommendedClassesSuccess');
export const fetchRecommendedClassesFailure = createAction('fetchRecommendedClassesFailure');

// Устанавливаем класс рекомендованным
export const setTrainingRecommendedClass = createAction('setTrainingRecommendedClass', (payload: I.ITrainingRecommendedClassPayload) => ({ payload }));

// Удаляем класс рекомендованным
export const fetchDeleteRecommendClass = createAction('fetchDeleteRecommendClass', (payload: I.ITrainingRecommendedClassPayload) => ({ payload }));
export const fetchDeleteRecommendClassSuccess = createAction('fetchDeleteRecommendClassSuccess');
export const fetchDeleteRecommendClassFailure = createAction('fetchDeleteRecommendClassFailure');

// Рекомендация тренинга учителем
export const fetchTrainingsRecommend = createAction('fetchTrainingsRecommend', (payload: I.ITrainingRecommendPayload) => ({ payload }));
export const fetchTrainingsRecommendSuccess = createAction('fetchTrainingsRecommendSuccess');
export const fetchTrainingsRecommendFailure = createAction('fetchTrainingsRecommendFailure');

// Получение данных по оценкам учеников
export const fetchAddStudent = createAction('fetchAddStudent', (payload: I.IAddStudentPayload, meta: any) => ({ payload, meta }));
export const fetchAddStudentSuccess = createAction('fetchAddStudentSuccess');
export const fetchAddStudentFailure = createAction('fetchAddStudentFailure');

// Изменение ФИО ученика
export const fetchEditStudent = createAction('fetchEditStudent', (payload: I.IEditStudentPayload) => ({ payload }));
export const fetchEditStudentSuccess = createAction('fetchEditStudentSuccess');
export const fetchEditStudentFailure = createAction('fetchEditStudentFailure');

// Удаление ученика из класса
export const fetchDeleteStudent = createAction('fetchDeleteStudent', (payload: I.IAddStudentPayload) => ({ payload }));
export const fetchDeleteStudentSuccess = createAction('fetchDeleteStudentSuccess');
export const fetchDeleteStudentFailure = createAction('fetchDeleteStudentFailure');

// Удаление класса
export const fetchDeleteTeacherClass = createAction('fetchDeleteTeacherClass', (payload: I.IDeleteTeacherClassPayload) => ({ payload }));
export const fetchDeleteTeacherClassSuccess = createAction('fetchDeleteTeacherClassSuccess');
export const fetchDeleteTeacherClassFailure = createAction('fetchDeleteTeacherClassFailure');
