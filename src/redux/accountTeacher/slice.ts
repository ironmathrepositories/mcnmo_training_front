import { createSlice } from '@reduxjs/toolkit';
import * as I from './interfaces';

interface IState {
    classData?: I.IClassDataSuccess;
    scores: I.IScoresData,
    recommended: {
        [id: string]: I.IRecommendedClassesDataObject
    }
}

const initialState: IState = {
    recommended: {},
    scores: {}
};

const setEditable = (state, classId, isEdit) => {
    if (state.classData) {
        state.classData = state.classData.map(classItem => {
            if (classItem.id === classId) {
                return {
                    ...classItem,
                    isEdit
                }
            }

            return classItem;
        });
    }
};

const slice = createSlice({
    name: 'accountTeacher',
    initialState,
    reducers: {
        setClassData(state, action) {
            state.classData = action.payload;
        },
        addClass(state, action) {
            const classItem = { ...action.payload, isEdit: true };
            state.classData = [ classItem, ...(state.classData || []) ];
        },
        editClass(state, action) {
            const { payload } = action;
            if (!state.classData) {
                return;
            }

            state.classData = state.classData.map(itemClass => {
                if (itemClass.id !== payload.id) {
                    return itemClass;
                }

                return {
                    ...itemClass,
                    ...payload
                }
            });
        },
        deleteClass(state, action) {
            const { payload } = action;
            if (!state.classData) {
                return;
            }

            state.classData = state.classData.filter(itemClass => itemClass.id !== payload);
        },
        setScores(state, action) {
            const { classId, scores } = action.payload as I.ISetScorePayload;
            state.scores[classId] = scores;
        },
        setEditableClass(state, action) {
            const classId = action.payload;
            return setEditable(state, classId, true);
        },
        setReadableClass(state, action) {
            const classId = action.payload;
            return setEditable(state, classId, false);
        },
        setRecommendedClass(state, action) {
            const { trainingId, data } = action.payload;
            state.recommended[trainingId] = data && data.reduce((result, item) => {
                return {
                    ...result,
                    [item]: true
                }
            }, {});
            // state.recommended[trainingId] = data;
        },
        setTrainingRecommended(state, action) {
            const { trainingId, classId } = action.payload;
            if (!state.recommended[trainingId]) {
                return;
            }

            state.recommended[trainingId][classId] = !state.recommended[trainingId][classId];
        },
        addStudent(state, action) {
            const { classId, student } = action.payload;

            if (!state.classData) return;
            state.classData = state.classData.map(classItem => {
                if (classItem.id !== classId) {
                    return classItem;
                }

                return {
                    ...classItem,
                    students: [ student, ...(classItem.students || []) ]
                }
            })
        },
        editStudent(state, action) {
            const student = action.payload;

            if (!state.classData) return;
            state.classData.forEach(classItem => {
                if (!classItem.students) {
                    return;
                }

                classItem.students = classItem.students.map(item => {
                    if (item.id !== student.id) {
                        return item;
                    }

                    return student;
                })
            })
        },
        deleteStudent(state, action) {
            const { classId, studentId } = action.payload;

            if (!state.classData) return;
            state.classData = state.classData.map(classItem => {
                if (classItem.id !== classId) {
                    return classItem
                }

                const students = classItem.students?.filter(student => student.id !== studentId);

                return {
                    ...classItem,
                    students
                };
            }, []);
        }
    }
});

export const {
    setClassData,
    addClass,
    editClass,
    deleteClass,
    setScores,
    setEditableClass,
    setReadableClass,
    setRecommendedClass,
    setTrainingRecommended,
    addStudent,
    editStudent,
    deleteStudent
} = slice.actions;
export default slice.reducer;
