import { Codes } from '@/constants';
import { IResponse } from '@/interfaces/response';
import { Action } from '@/types/action';

export interface IClassData {
    id: string;
    name: string;
    class: number;
    students?: Array<IStudentData>,
    isEdit?: boolean; // Редактируется ли блок в данный момент
}

export interface IStudentData {
    id: string;
    name?: string | null;
    patronymic?: string | null;
    surname?: string | null;
    comment: string | null;
    accessResolution?: boolean;
    classes?: Array<IClassData>
}

// Список рекомендованных классов
export type IClassDataSuccess = Array<IClassData>;
export type IFetchClassDataAction = Action<unknown>;
export type IClassDataResponse = IResponse<IClassDataSuccess>;

// Информация по классу
export interface IFetchClassDataItemPayload {
    classId: string;
}
export type IFetchClassDataItemAction = Action<IFetchClassDataItemPayload>;
export type IClassDataItemResponse = IResponse<IClassData>;

// Добавление класса
export interface IAddClassPayload {
    name: string;
    class: number;
}
export type IAddClassAction = Action<IAddClassPayload>;
export type IAddClassResponse = IResponse<IClassData>;

// Редактирование класса
export interface IEditClassPayload {
    id: string;
    data: {
        name: string;
        class: number;
    }
}
export type IEditClassAction = Action<IEditClassPayload>;
export type IEditClassResponse = IResponse<IClassData>;

// Получение данных по оценкам учеников
export type IScoresPayload = string;
export type IScoresAction = Action<IScoresPayload>;
export interface IScoresRow {
    id: string;
    examDirectionId: string;
    trainingCollectionId: string;
    name: string;
    scores: {
        [studentId: string]: number | null;
    }
}
export type IScoresRows = Array<IScoresRow>;
export interface IScoresData {
    [classId: string]: IScoresRows
}
export interface ISetScorePayload {
    classId: string;
    scores: IScoresRows;
}
export type IScoresResponse = IResponse<IScoresRows>;

// Список рекомендованных классов
export type IRecommendedClassesPayload = string;
export type IRecommendedClassesAction = Action<IRecommendedClassesPayload>;
export type IRecommendedClassesData = Array<string>;
export interface IRecommendedClassesDataObject {
    [id: string]: boolean;
}
export type IRecommendedClassesResponse = IResponse<IRecommendedClassesData>;

// Устанавливаем класс рекомендованным
export interface ITrainingRecommendedClassPayload {
    trainingId: string;
    classId: string;
}

export type ITrainingRecommendedClassAction = Action<ITrainingRecommendedClassPayload>;

// Рекомендация тренинга учителем
export interface ITrainingRecommendPayload {
    trainingId: string;
    data: IRecommendedClassesDataObject;
}

export type ITrainingRecommendResponse = Action<unknown>;

// Удалить рекоммендацию к классу
export type IDeleteRecommendClassAction = Action<ITrainingRecommendedClassPayload>;
export type IDeleteRecommendClassResponse = Action<unknown>;

// Добавление ученика в класс
export interface IAddStudentPayload {
    studentId: string;
    classId: string;
}
export interface IEditStudentDataPayload {
    comment: string | null;
}
export type IAddStudentAction = Action<IAddStudentPayload>;
export type IAddStudentResponse = IResponse<IEditStudentDataPayload>;

// Изменение ФИО ученика
export interface IEditStudentPayload {
    studentId: string;
    data: IEditStudentDataPayload;
}
export type IEditStudentAction = Action<IEditStudentPayload>;
export type IEditStudentResponse = IResponse<IStudentData>;

// Удаление ученика из класса
export interface IDeleteStudentPayload {
    studentId: string;
    classId: string;
}
export type IDeleteStudentAction = Action<IDeleteStudentPayload>;
export type IDeleteStudentResponse = IResponse<unknown>;

// Удаление класса
export interface IDeleteTeacherClassPayload {
    classId: string;
}
export type IDeleteTeacherClassAction = Action<IDeleteTeacherClassPayload>;
export type IDeleteTeacherClassResponse = IResponse<unknown>;
