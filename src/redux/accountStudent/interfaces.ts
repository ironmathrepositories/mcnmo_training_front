import { IPagination, IResponse } from '@/interfaces/response';
import { Action } from '@/types/action';

// Получение списка учителей
export interface ITeacherData {
    id: string;
    name: string | null;
    patronymic: string | null;
    surname: string | null;
    comment: string | null;
    accessResolution: boolean; // разрашен ли данному учителю доступ к данным текущего ученика
}

export type ITeacherDataList = Array<ITeacherData>;
export type ITeacherDataAction = Action<unknown>;
export type ITeachersListResponse = IResponse<ITeacherDataList>;

// Изменение разрешения на доступ к данным ученика
export interface ISetAccessResolutionPayload {
    teacherId: string;
    accessResolution: boolean;
}
export type ISetAccessResolutionAction = Action<ISetAccessResolutionPayload>;
export type ISetAccessResolutionResponse = IResponse<unknown>;

// Список тренингов
export interface IStudentTrainingsPayload {
    page: number;
    chunk: number;
}
export type IStudentTrainingsAction = Action<IStudentTrainingsPayload>;

export interface IStudentTrainingBlock {
    id: string;
    examDirectionId: string;
    trainingCollectionId: string;
    name: string;
    icon: string;
    recommendedBy?: Array<string>; // Список учителей рекомендовавших этот тренинг
    score?: number | null;
}
export type IStudentTrainingsData = IPagination<IStudentTrainingBlock>;
export type IStudentTrainingsResponse = IResponse<IStudentTrainingsData>;

// Статистика ученика
export interface IStat {
    total: number;
    lastWeek: number;
}
export type IStatAction = Action<unknown>;
export type IStatResponse = IResponse<IStat>
