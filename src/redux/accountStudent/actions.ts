import { createAction } from '@reduxjs/toolkit';
import * as I from './interfaces';

// Получение списка учителей
export const fetchStudentTeachers = createAction('fetchStudentTeachers');
export const fetchStudentTeachersSuccess = createAction('fetchStudentTeachersSuccess');
export const fetchStudentTeachersFailure = createAction('fetchStudentTeachersFailure');

// Получение списка учителей
export const fetchStudentSetAccessResolution = createAction('fetchStudentSetAccessResolution', (payload: I.ISetAccessResolutionPayload) => ({ payload }));
export const fetchStudentSetAccessResolutionSuccess = createAction('fetchStudentSetAccessResolutionSuccess');
export const fetchStudentSetAccessResolutionFailure = createAction('fetchStudentSetAccessResolutionFailure');

// Список тренингов
export const fetchStudentTrainings = createAction('fetchStudentTrainings', (payload: I.IStudentTrainingsPayload) => ({ payload }));
export const fetchStudentTrainingsSuccess = createAction('fetchStudentTrainingsSuccess');
export const fetchStudentTrainingsFailure = createAction('fetchStudentTrainingsFailure');

// Статистика ученика
export const fetchStat = createAction('fetchStat');
export const fetchStatSuccess = createAction('fetchStatSuccess');
export const fetchStatFailure = createAction('fetchStatFailure');
