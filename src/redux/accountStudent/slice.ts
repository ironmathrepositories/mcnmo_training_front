import { createSlice } from '@reduxjs/toolkit';
import * as I from './interfaces';

interface IState {
    teachers?: I.ITeacherDataList,
    studentTrainings?: I.IStudentTrainingsData,
    stat?: I.IStat
}

const initialState: IState = {};

const slice = createSlice({
    name: 'accountStudent',
    initialState,
    reducers: {
        setTeachers(state, action) {
            state.teachers = action.payload;
        },
        setAccessResolution(state, action) {
            const { teacherId, accessResolution } = action.payload;

            if (!state.teachers) {
                return;
            }

            state.teachers = state.teachers.map(teacher => {
                if (teacher.id === teacherId) {
                    return {
                        ...teacher,
                        accessResolution
                    }
                }

                return teacher;
            });
        },
        setStudentTrainings(state, action) {
            state.studentTrainings = action.payload;
        },
        setStatistic(state, action) {
            state.stat = action.payload;
        },
        clearStatistic(state) {
            delete state.stat;
        }
    }
});

export const {
    setTeachers,
    setAccessResolution,
    setStudentTrainings,
    setStatistic,
    clearStatistic
} = slice.actions;

export default slice.reducer;
