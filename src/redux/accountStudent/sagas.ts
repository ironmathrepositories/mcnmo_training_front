import { takeLatest, put, select, all } from 'redux-saga/effects';
import { AxiosError, AxiosResponse } from 'axios';

import { RootState } from '@store';
import { runRequestProcess } from '@utils/run_request_process';
import { IResponseSuccess } from '@/interfaces/response';
import * as actions from '@redux/actions';
import { ErrorResponse } from '@/types/error_response';
import * as I from './interfaces';
import * as accountStudentActions from './slice';

// Получение списка учителей
const studentTeachersSuccess = function* (data: AxiosResponse<I.ITeachersListResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const payload = (data.data as IResponseSuccess<I.ITeacherDataList>).payload;
        yield put(accountStudentActions.setTeachers(payload));
    }

    return { data, error };
};

// Изменение разрешения на доступ к данным ученика
const studentSetAccessResolution = function* (data: AxiosResponse<I.ISetAccessResolutionResponse>, error: AxiosError<ErrorResponse>, action: I.ISetAccessResolutionAction) {
    if (data && (data.status === 200 || data.status === 204)) {
        yield put(accountStudentActions.setAccessResolution(action.payload));
    }

    return { data, error };
};

// Список тренингов
const studentTrainings = function* (data: AxiosResponse<I.IStudentTrainingsResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const trainings = (data.data as IResponseSuccess<I.IStudentTrainingsData>).payload;
        yield put(accountStudentActions.setStudentTrainings(trainings));
    }

    return { data, error };
};

// Статистика ученика
const studentStat = function* (data: AxiosResponse<I.IStatResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const stat = (data.data as IResponseSuccess<I.IStat>).payload;
        yield put(accountStudentActions.setStatistic(stat));
    }

    return { data, error };
};



const init = function* () {
    // yield takeLatest(`${actions.setTrainingRecommendedClass.type}`, setTrainingRecommended);
};


export const watchFetchStudentTeachers = runRequestProcess<I.ITeachersListResponse, unknown>(actions.fetchStudentTeachers.type, studentTeachersSuccess);
export const watchFetchSetAccessResolution = runRequestProcess<I.ISetAccessResolutionResponse, I.ISetAccessResolutionPayload>(actions.fetchStudentSetAccessResolution.type, studentSetAccessResolution);
export const watchFetchStudentTrainings = runRequestProcess<I.IStudentTrainingsResponse, I.IStudentTrainingsPayload>(actions.fetchStudentTrainings.type, studentTrainings);
export const watchFetchStat = runRequestProcess<I.IStatResponse, unknown>(actions.fetchStat.type, studentStat);

export default [
    watchFetchStudentTeachers,
    watchFetchSetAccessResolution,
    watchFetchStudentTrainings,
    watchFetchStat,
    init
];
