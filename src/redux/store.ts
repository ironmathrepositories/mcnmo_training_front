import { getPromisifiedDispatch } from '@utils/promisified_dispatch';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '@redux/root_reducer';
import rootSaga from '@redux/root_saga';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer,
    devTools: !!DEVELOPMENT,
    middleware: getDefaultMiddleware({
        thunk: false,
        serializableCheck: false
    }).concat(sagaMiddleware)
});

if (DEVELOPMENT && module.hot) {
    module.hot.accept('./root_reducer', () => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const newRootReducer = require('./root_reducer').default
        store.replaceReducer(newRootReducer)
    })
}

sagaMiddleware.run(rootSaga);

export const promisifiedDispatch = getPromisifiedDispatch(store);
export type RootState = ReturnType<typeof store.getState>;

export default store;
