import { createSlice } from '@reduxjs/toolkit';
import * as I from './interfaces';

interface IState {
    accountData?: I.IAccountData
}

const initialState: IState = {};

const slice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        setAccountData(state, action) {
            state.accountData = action.payload;
        }
    }
});

export const {
    setAccountData
} = slice.actions;

export default slice.reducer;
