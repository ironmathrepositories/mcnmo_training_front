import { Codes } from '@/constants';
import { IResponse } from '@/interfaces/response';
import { Action } from '@/types/action';
import { IUser } from '@redux/auth/interfaces';

export interface IAccountData {
    id: string;
    name: string | null;
    surname: string | null;
    patronymic: string | null;
    comment?: string;
    email: string;
    confirmed: boolean;
}

// Получение данных по своему аккаунту
export type IFetchAccountAction = unknown;
export type IAccountResponse = IResponse<IAccountData>;

// Изменение своего ФИО
export interface IFetchAccountSetFioPayload {
    comment: string;
    name?: string;
    surname?: string;
    patronymic?: string;
}
export type IFetchAccountSetFioAction = Action<IFetchAccountSetFioPayload>;
export type IAccountSetFioResponse = IResponse<IAccountData>;
