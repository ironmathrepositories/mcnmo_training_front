import { takeLatest, put, select, all } from 'redux-saga/effects';
import { AxiosError, AxiosResponse } from 'axios';

import { BreakRequestProcess, runRequestProcess } from '@utils/run_request_process';
import * as actions from '@redux/actions';
import { ErrorResponse } from '@/types/error_response';
import * as I from './interfaces';
import * as accountActions from './slice';
import { IResponseSuccess } from '@/interfaces/response';

const accountSuccess = function* (data: AxiosResponse<I.IAccountResponse>, error: AxiosError<ErrorResponse>) {
    if (data && (data.status === 200)) {
        const payload = (data.data as IResponseSuccess<I.IAccountData>).payload;
        yield put(accountActions.setAccountData(payload));
    }

    return { data, error };
};

export const watchFetchAccount = runRequestProcess<I.IAccountResponse, unknown>(actions.fetchAccount.type, accountSuccess);
export const watchFetchAccountSetFio = runRequestProcess<I.IAccountSetFioResponse, I.IFetchAccountSetFioPayload>(actions.fetchAccountSetFio.type, accountSuccess);

export default [
    watchFetchAccount,
    watchFetchAccountSetFio
];
