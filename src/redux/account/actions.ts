import { createAction } from '@reduxjs/toolkit';
import * as I from './interfaces';

// Получение данных по своему аккаунту
export const fetchAccount = createAction('fetchAccount');
export const fetchAccountSuccess = createAction('fetchAccountSuccess');
export const fetchAccountFailure = createAction('fetchAccountFailure');

// Изменение своего ФИО
export const fetchAccountSetFio = createAction('fetchAccountSetFio', (payload: I.IFetchAccountSetFioPayload) => ({ payload }));
export const fetchAccountSetFioSuccess = createAction('fetchAccountSetFioSuccess');
export const fetchAccountSetFioFailure = createAction('fetchAccountSetFioFailure');
