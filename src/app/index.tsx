import React from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '@redux/store';
import FormStyleContext, { defaultValue } from '@/context/FormStyleContext';
import AppRouter from './router';
import { Modal, ErrorBoundary } from '@components';
import { history } from '@redux/history';
import { ToastContainer } from 'react-toastify';
import Header from '../components/Header/Header';
import { Footer } from '../components/Footer/Footer';
import styles from './index.module.scss';
import '!style-loader!css-loader!react-toastify/dist/ReactToastify.css';

const App = (): JSX.Element => {
    return (
        <ErrorBoundary>
            <FormStyleContext.Provider value={defaultValue}>
                <Provider store={store}>
                    <Modal />
                    <ToastContainer
                        position="top-right"
                        autoClose={5000}
                        hideProgressBar={true}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover
                    />
                    <Router history={history}>
                        <AppRouter />
                        <Footer />
                    </Router>
                </Provider>
            </FormStyleContext.Provider>

        </ErrorBoundary>
    )
};

export default App;
