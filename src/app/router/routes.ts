import { Routes, RouteItem } from '@/types/route_item';
import * as screens from '@screens';

export const routes: Routes = [
    {
        path: '/',
        exact: true,
        component: screens.EnterScreen
    },
    {
        path: '/about',
        exact: true,
        component: screens.AboutScreen
    },
    {
        path: '/classroom',
        exact: true,
        component: screens.ClassRoomScreen,
        isLogin: true
    },
    {
        path: '/teacherInfo',
        exact: true,
        component: screens.TeacherInfoScreen
    },
    {
        path: '/terms-of-use',
        exact: true,
        component: screens.TermsOfUseScreen
    },
    {
        path: '/personal-data-processing',
        exact: true,
        component: screens.PersonalDataProcessingScreen
    },
    {
        path: '/training/:directionId/:collectionId/:trainingId',
        exact: true,
        component: screens.CollectionScreen,
        isLogin: true
    },
    {
        path: '/topics/:trainingId',
        exact: true,
        component: screens.TopicsScreen
    },
    {
        path: '/confirmation',
        exact: true,
        component: screens.ConfirmationScreen
    },
    {
        path: '/confirmation/:confirmCode',
        exact: true,
        component: screens.ConfirmationScreen
    },
    {
        path: '/new-password',
        exact: true,
        component: screens.RestoreScreen
    },
    {
        path: '/new-password/:singleToken',
        exact: true,
        component: screens.RestoreScreen
    },
    {
        path: '/error',
        exact: true,
        component: screens.ErrorScreen
    }
];

interface TestVersionScreenProp {
    type: string;
}

[ 'oge', 'ege' ].forEach((item) => {
    const route = {
        path: `/${item}`,
        exact: true,
        component: screens.TestVersionScreen,
        props: {
            type: item
        }
    };
    routes.push(route as unknown as RouteItem<TestVersionScreenProp>);
});
