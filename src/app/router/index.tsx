import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import classNames from 'classnames';

import { RootState } from '@store';
import * as screens from '@screens';
import { routes } from './routes'
import styles from '@/app/index.module.scss';
import Header from '@/components/Header/Header';

const mapState = (state: RootState) => ({
    token: state.auth.token?.access
});

const mapDispatch = {};

const connector = connect(mapState, mapDispatch);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IProps = PropsFromRedux;

const AppRouter: React.FC<IProps> = ({ token }): JSX.Element => {
    return <Switch>
        {routes.map((params) => {
            const Component = token || !params.isLogin ? params.component : screens.NoAccessScreen;
            const layout = props => {
                const classN = [ styles.appContent ];
                if (props.location.pathname === '/') {
                    classN.push(styles.appContentMain);
                }

                const propsComponent = params.props
                    ? {
                        ...params.props,
                        ...props
                    } : {
                        ...props
                    };

                return (
                    <div className={classNames(...classN)}>
                        <Header />
                        <Component {...propsComponent} />
                    </div>
                )
            };

            return <Route
                key={params.path}
                exact={params.exact}
                component={layout}
                path={params.path}
            />
        })}
        <Redirect push to='/error' />
    </Switch>
};

export default connector(AppRouter);
