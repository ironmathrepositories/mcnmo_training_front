#!/usr/bin/bash
rm -rf ./node_modules;
rm -rf ./dist;
npm install;
npm run build;
ssh math_editor_demo@31.31.202.61 /bin/bash << EOF
cd ~/web/mcnmo_trainings/training_integration;
docker stop training_integration;
docker rm /training_integration;
rm -rf ./*;
mkdir ./dist;
mkdir ./infra;
exit;
EOF
scp -r ./dist/* math_editor_demo@31.31.202.61:/home/math_editor_demo/web/mcnmo_trainings/training_integration/dist;
scp -r ./infra/* math_editor_demo@31.31.202.61:/home/math_editor_demo/web/mcnmo_trainings/training_integration/infra;
scp ./DockerfileIntegration math_editor_demo@31.31.202.61:/home/math_editor_demo/web/mcnmo_trainings/training_integration/DockerfileIntegration;
ssh math_editor_demo@31.31.202.61 /bin/bash << EOF
cd ~/web/mcnmo_trainings/training_integration;
docker build -f DockerfileIntegration -t training_integration ./;
docker run -d -p 4005:80 --name training_integration training_integration:latest;
exit;
EOF