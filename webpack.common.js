const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const createModuleFederationPlugin = require('./modules.config');

function getModulesBaseURL(modulesTarget) {
  switch (modulesTarget) {
    case 'production':
      return 'https://npm.mathem.ru';
    case 'test':
    default:
      return 'https://altnpm.mathem.ru';
  }
}

module.exports = (env, options) => {
  const { mode } = options;
  console.log('Building with ENV:', env);
  console.log('Build mode:', mode);
  console.log('Webpack options:', options);

  return {
    entry: {
      app: path.join(__dirname, 'src', 'index.tsx'),
    },
    target: 'web',
    resolve: {
      extensions: ['.ts', '.tsx', '.json', '.js', '.jsx', '.scss'],
      alias: {
        react: path.resolve(__dirname, 'node_modules/react'), // https://reactjs.org/warnings/invalid-hook-call-warning.html
        '@': path.resolve(__dirname, 'src'),
        '@redux': path.resolve(__dirname, 'src/redux'),
        '@actions': path.resolve(__dirname, 'src/redux/actions'),
        '@selectors': path.resolve(__dirname, 'src/redux/selectors'),
        '@history': path.resolve(__dirname, 'src/redux/history'),
        '@store': path.resolve(__dirname, 'src/redux/store'),
        '@components': path.resolve(
          __dirname,
          'src/components/general_components'
        ),
        '@utils': path.resolve(__dirname, 'src/utils'),
        '@screens': path.resolve(__dirname, 'src/components/screens'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc)ss$/i,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                modules: {
                  localIdentName: '[path]__[local]___[hash:base64:5]',
                },
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },
        {
          test: /\.css$/, // for css from dependencies
          use: [MiniCssExtractPlugin.loader, 'css-loader'],
        },
        {
          test: /bootstrap\.tsx$/,
          loader: 'bundle-loader',
          options: {
            lazy: true,
          },
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
        },
        {
          test: /\.(ttf|woff|woff2|eot)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                publicPath: 'fonts',
                outputPath: 'fonts',
              },
            },
          ],
        },
        {
          test: /\.(png|jpe?g|gif|jp2|webp|svg|ico)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
          },
        },
      ],
    },
    plugins: [
      createModuleFederationPlugin({
        target: process.env.MODULES_TARGET,
      }),
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin(),
      new HtmlPlugin({
        template: path.join(__dirname, 'src', 'index.html'),
        favicon: path.join(__dirname, 'src', 'img', 'favicon.ico'),
      }),
      new webpack.DefinePlugin({
        'process.env.MODULES_TARGET': JSON.stringify(
          process.env.MODULES_TARGET
        ),
        'process.env.WYSIWYG_URL': JSON.stringify(process.env.WYSIWYG_URL),
        'process.env.WYSIWYG_BASE_URL': JSON.stringify(
          getModulesBaseURL(process.env.MODULES_TARGET)
        ),
        'process.env.WEBPREVIEW_URL': JSON.stringify(
          process.env.WEBPREVIEW_URL
        ),
        'process.env.WEBPREVIEW_URL_V1': JSON.stringify(
          process.env.WEBPREVIEW_URL_V1
        ),
        'process.env.WEBPREVIEW_URL_V2': JSON.stringify(
          process.env.WEBPREVIEW_URL_V2
        ),
        'process.env.WEBPREVIEW_BASE_URL': JSON.stringify(
          getModulesBaseURL(process.env.MODULES_TARGET)
        ),
      }),
    ],
  };
};
